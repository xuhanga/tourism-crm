<?php
return array(
    'AUTH_CONFIG'=>array(
        'AUTH_ON'           => true, // 认证开关
        'AUTH_TYPE'         => 1, // 认证方式，1为实时认证；2为登录认证。
        'AUTH_GROUP'        => C('DB_PREFIX').'reseller_auth_group', // 用户组数据表名
        'AUTH_GROUP_ACCESS' => C('DB_PREFIX').'reseller_auth_group_access', // 用户-用户组关系表
        'AUTH_RULE'         => C('DB_PREFIX').'reseller_auth_rule', // 权限规则表
        'AUTH_USER'         => C('DB_PREFIX').'reseller', // 用户信息表
    ),
    //不用验证的控制器
    'NOT_VALIDATE'=>array(
        'Reseller/Login/login',
        'Reseller/Index/index',
        'Reseller/OrderCenter/getLine', //获取线路
        'Reseller/OrderCenter/order',   //出团时间选择页面
        'Reseller/OrderCenter/getSeat', //获取当天团的余位
        'Reseller/OrderCenter/checkGroup', //检测该团状态
        'Reseller/OrderCenter/getCity', //获取城市列表
        'Reseller/OrderCenter/tripDetail', //行程详情
        'Reseller/OrderCenter/showCode',  //更新/获取验证码
        'Reseller/OrderCenter/writeInfo', //step 3 写入游客信息
        'Reseller/OrderCenter/submitTouristInfo', //提交订单信息
        'Reseller/OrderCenter/detailView',  //订单预览
        'Reseller/OrderCenter/orderChange', //修改订单实收金额
        'Reseller/Index/orderDetail',       //订单详情
    ),

);