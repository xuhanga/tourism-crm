<?php
namespace Reseller\Model;
/**
 * ModelName
 */
class UsersModel extends BaseModel{

    protected $tableName = 'operator_line_reseller';

    // 自动验证
    protected $_validate=array(
        array('reseller_name','require','用户名必须',0,'',3), // 验证字段必填
        array('reseller_phone','checkPhone','手机号格式不正确!',0,'callback',3), // 验证字段必填
        array('reseller_phone','checkPhoneExists','该手机号码已被注册!',0,'callback',3), // 验证字段必填
        array('reseller_account','require','登录帐号必须填写',1,'',3), // 验证字段必填
        array('reseller_account','checkAccount','帐号已经存在!',1,'callback',3), // 验证字段必填
    );

    /**
     * 判断手机号是否正确
     */
    public  function  checkPhone(){
        $operator_phone=I('reseller_phone');

        if(preg_match("/^1[34578]{1}\d{9}$/",$operator_phone)){
           return true;
        }else{
            return false;
        }
    }

    /**
     * 判断手机号是否重复
     */
    public  function checkPhoneExists(){
        $id=I('id');
        $map['reseller_phone']=I('reseller_phone');
        $map['reseller_flag']=1;
        $info=$this->where($map)->find();
        if($info&&$info['reseller_id']!=$id){
            return false;
        }
        return true;
    }


    //判断用户帐号是否重复
    public function checkAccount(){
        $id=I('id');
        $map['reseller_account']=I('reseller_account');
        $map['reseller_flag']=1;
        $info=$this->where($map)->find();
        if($info&&$info['reseller_id']!=$id){

            return false;
        }
        return true;
    }

    // 自动完成
    protected $_auto=array(
        //array('login_pwd','md5',1,'function') , // 对password字段在新增的时候使md5函数处理
        array('create_time','time',1,'function'), // 对date字段在新增的时候写入当前时间戳
        array('pid','setPid',1,'callback'), //设置父ID
        array('reseller_type','1'), // 默认是国内
        array('reseller_status','1'), // 默认用户状态正常
        array('is_review','1'), // 默认已审核
        array('reseller_sn','newresellerSn',1,'callback'), //分配唯一编号
    );


    //分配唯一编号
    public  function  newresellerSn(){
        $lastSn=M('operator_line_reseller')->order('reseller_sn desc')->getField('reseller_sn');
        if(!$lastSn){
            return "10000".mt_rand(100,999);
        }
        return $lastSn+mt_rand(100,999);
    }


    //设置pid
    public  function  setPid(){
        if(session('IS_ADMIN')){
           return  session('reseller_user.reseller_id');
        }
        return session('reseller_user.pid');
    }


    /**
     * 添加用户
     */
    public function addData($data){
        // 对data数据进行验证
        if(!$data=$this->create($data)){
            // 验证不通过返回错误
            return false;
        }else{
            // 验证通过
            if($data['login_pwd']){
                $data['login_secret']=mt_rand(1000,9999);
                $data['login_pwd']=md5( $data['login_pwd'].$data['login_secret']);
            }
            unset($data['id']);
            $data['operator_id'] = session('reseller_user.operator_id');
            $data['shop_or_reseller'] = session('reseller_user.shop_or_reseller');
            $data['start_time'] = session('reseller_user.start_time');
            $data['end_time'] = session('reseller_user.end_time');
            $result=$this->add($data);
            return $result;
        }
    }

    /**
     * 修改用户
     */
    public function editData($map,$data){
        // 对data数据进行验证
        if(!$data=$this->create($data)){
            // 验证不通过返回错误
            return false;
        }else{
            // 验证通过
            if($data['login_pwd']){
                $data['login_secret']=mt_rand(1000,9999);
                $data['login_pwd']=md5( $data['login_pwd'].$data['login_secret']);
            }
            unset($data['id']);

            // echo M()->_sql();die;
            $result=$this
                ->where(array($map))
                ->save($data);

            return $result;
        }
    }

    /**
     * 删除数据
     * @param   array   $map    where语句数组形式
     * @return  boolean         操作是否成功
     */
    public function deleteData($map){
        die('禁止删除用户');
    }


    /**
     * 更改用户状态
     */
     public  function changeUserStatus($data){
         $uid=intval($data['uid']);
         if(!$uid){
             return ['status'=>-1,'msg'=>'操作失败!'];
         }
         $res=M('operator_line_reseller')->where(['reseller_id'=>$uid])->save(['reseller_status'=>$data['status']]);
         if($res){
             return ['status'=>1,'msg'=>'操作成功!'];
         }
         return ['status'=>0,'msg'=>'操作失败!'];
     }

}
