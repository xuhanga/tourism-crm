<?php
namespace Reseller\Controller;

use Think\Controller;

/**
 * 后台首页控制器
 */
class LoginController extends Controller
{

    /**
     * 登录
     */
    public function login()
    {

        if (I('logout')) {
            session('reseller_user', null);
            $this->success('退出成功!', '/Reseller/Login/login');
            exit();
        }

        if (IS_AJAX) {
            $userName = I('username');
            $password = I('password');
            // $code=I('code');
            // $verify = new \Think\Verify();
            // if(!$verify->check($code)){
            //     $this->ajaxReturn(['status'=>-1,'msg'=>'验证码错误!']);
            // }
            $userInfo = M('operator_line_reseller')
            //['reseller_sn' => $userName, 'reseller_phone' => $userName , '_logic' => 'or']
            ->where(array('reseller_phone' => $userName, 'reseller_status' => 1, 'reseller_flag' => 1))
                ->find();
            $time = time();
            if ($time < $userInfo['start_time'] || $userInfo['end_time'] < $time) {
                return $this->ajaxReturn(['status' => -1, 'msg' => '不在合作时间之内']);
            }

            // dump($userInfo['login_pwd']);
            // echo $userInfo['login_secret'];

            // dump(md5( $userInfo['login_secret'].$password));
            // die;
            if ($userInfo['login_pwd'] == md5( $password.$userInfo['login_secret'])) {
                session('reseller_user', $userInfo);
                //如果是超管,拥有所有权限
                session('IS_ADMIN', $userInfo['pid'] == 0 ? 1 : 0);

                $this->ajaxReturn(['status' => 1, 'msg' => '登录成功!']);
            }

            $this->ajaxReturn(['status' => 0, 'msg' => '登录失败!']);

        }
        $this->display('login');
    }

    /**
     * 显示验证码
     */
    public function showCode()
    {
        ob_clean();
        validateCode();
    }

}
