<?php
namespace Operator\Controller;
use Think\Controller;
/**
 * 后台首页控制器
 */
class IndexController extends BaseController{

    public function __construct(){
        parent::__construct();
    }

	/**
	 * 首页
	 */
	public function index(){

		$this->display('index');
	}
	/**
	 * elements
	 */
	public function elements(){
		$this->display();
	}

	/**
	 * welcome
	 */
	public function welcome(){
		$m = D('Index');
		$this->day = $m->dayData();
        $this->years = $m->years();
		$this->selectYear = I('years') ? I('years') : date('Y', time());
        if( I('years') && I('choose') ){
			$this->right = $m->yearsDate();
		}else{
			$this->right = $m->monthDate();
		}

        //待审核产品列表
        $this->auditList=D('Index')->auditProductList();

        //待审核的单品供应商
        $this->singleSupplier=D('Index')->auditSingleSupplier();

        //待审核线路
        $this->lineList=D('Index')->lineList();

        //待合作线路
        $this->cooperationList=D('Index')->cooperationList();

        //待确认订单
        $this->auditOrderList=D('Index')->auditOrderList();

        //待结算订单
        $this->closingList=D('Index')->closingList();

		$this->list = $list;
	    $this->display('welcome');
	}

	/**
	 * 首页订单详情
	 */
	public function orderDetail(){
		$this->ob = D('Index')->orderDetail();
		$r = D('orderCenter4')->detailView();
		$this->dataToDisplay($r,['service']);
        $this->dataToDisplay($r['service']);
		// dump(end($this->ob['tourism'])['tourists_id']);die;
		$this->display('order_detail');
	}

    /**
     * 待审核的单品供应商列表
     */
	public  function  waitAuditUnitShop(){
	    $admin_id=session('operator_user.pid')==0?session('operator_user.operator_id'):session('operator_user.pid');
	    $nowTime=time();
	    $where['admin_operator_id']=$admin_id;
	    $where['status']=-1;
	    $where['_string']="$nowTime between start_time and end_time";
        $info=D('Index')->waitAuditUnitShop($where);
        $this->list=$info['list'];
        $this->show=$info['show'];
        $this->display();
    }

    /**
     * 更改单品供应商的审核状态
     */
    public  function  changeSupplierStatus(){
	    $data=I('');
	    $res=D('Index')->changeSupplierStatus($data);
	    $this->ajaxReturn($res);
    }

    /**
     * 待审核的单品供应商商品列表
     */
    public function waitAuditUnitGoods(){
        $admin_id=session('operator_user.pid')==0?session('operator_user.operator_id'):session('operator_user.pid');
        $this->auditList=D('Index')->waitAuditUnitGoods($admin_id);
        $this->display();
    }





}