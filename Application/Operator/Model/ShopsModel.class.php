<?php
namespace Operator\Model;
/**
 * 商家管理
 */
class ShopsModel extends BaseModel{

    protected $tableName = 'public_single_supplier';

    /**
     * 运营商新增单品供应商
     */
    public  function  addShops($data){
        $admin_id=session('operator_user.pid')==0?session('operator_user.operator_id'):session('operator_user.pid');
        $data['admin_operator_id']=$admin_id;
        if($this->create($data)){
            return $this->add();
        }
        return false;
    }


    /**
     * 生成新的商家编号
     */
    public  function shopSn(){
       $supplier_sn= $this->order('supplier_sn desc')->getField('supplier_sn');
       if(!$supplier_sn){
           return "10000".mt_rand(100,999);
       }
       return $supplier_sn+mt_rand(100,999);
    }

    /**
     * 生成新的商家编号
     */
    public  function supplierShopSn(){
        $supplier_sn= M('operator_line_supplier')->order('supplier_sn desc')->getField('supplier_sn');
        if(!$supplier_sn){
            return "10000".mt_rand(100,999);
        }
        return $supplier_sn+mt_rand(100,999);
    }


    /**
     * 生成新的商家编号
     */
    public  function resellerShopSn(){
       $supplier_sn= M('operator_line_reseller')->order('reseller_sn desc')->getField('reseller_sn');
       if(!$supplier_sn){
           return "10000".mt_rand(100,999);
       }
       return $supplier_sn+mt_rand(100,999);
    }


    /**
     * 交通商家列表
     */
    public function trafficShops($where){
        $count      = $this->where($where)->count();// 查询满足要求的总记录数
        $Page       = new \Think\Page($count,25);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show       = $Page->show();// 分页显示输出
        // 进行分页数据查询 注意limit方法的参数要使用Page类的属性
        $list=$this->where($where)->order('supplier_id desc')->limit($Page->firstRow.','.$Page->listRows)->select();
        $allAreas=S('allAreas');
        if(!$allAreas){
            $allAreas=M('areas')->where(['isShow'=>1,'areaFlag'=>1])->field('areaId,areaName')->select();
            $newArea=[];
            foreach ($allAreas as $k=>$v){
                $newArea[$v['areaId']]=$v['areaName'];
            }
            S('allAreas',$newArea,3600);
            $allAreas=$newArea;
        }

        foreach ($list as $k=>$v){
            $list[$k]['addr']=$allAreas[$v['country']].' '.$allAreas[$v['province']].' '.$allAreas[$v['city']].' '.$allAreas[$v['area']];
        }

        return ['list'=>$list,'show'=>$show];
    }

    /**
     * 导出交通供应商
     */
    /*public  function  trafficExportExcel($where){

        $list=$this->where($where)->order('supplier_id desc')->select();
        $allAreas=S('allAreas');
        if(!$allAreas){
            $allAreas=M('areas')->where(['isShow'=>1,'areaFlag'=>1])->field('areaId,areaName')->select();
            $newArea=[];
            foreach ($allAreas as $k=>$v){
                $newArea[$v['areaId']]=$v['areaName'];
            }
            S('allAreas',$newArea,3600);
            $allAreas=$newArea;
        }

        foreach ($list as $k=>$v){
            $list[$k]['addr']=$allAreas[$v['country']].' '.$allAreas[$v['province']].' '.$allAreas[$v['city']].' '.$allAreas[$v['area']];
        }

        $expCellName  = array(
            array('supplier_sn','供应商编号'),
            array('supplier_name','供应商名称'),
            array('type','项目'),
            array('Linkman','联系人'),
            array('Linkman_phone','联系电话'),
            array('partner_time','合作时间'),
            array('currency','币种'),
            array('status','状态'),
        );

        $fileName='交通供应商列表';
        switch ($where['type']){
            case 2:$fileName='酒店供应商';break;
            case 3:$fileName='保险供应商';break;
            case 4:$fileName='景区供应商';break;
        }
        parent::exportExcel($fileName,$expCellName,$list);
    }*/




    /**
     * 开通与禁用用户状态
     */
    public function trafficStatusById($data){
        return $this->where(['supplier_id'=>$data['id']])->setField(['status'=>$data['status']]);
    }

    /**
     * 导出单品供应商Excel
     */
    public  function exportExcelGroupList($where){
        $list=$this->where($where)->field('supplier_sn,supplier_name,type,Linkman,Linkman_phone,status,start_time,end_time')->select();
        foreach ($list as $k=>$v){
            $list[$k]['cooperation']=date('Y-m-d',$v['start_time']).'至'.date('Y-m-d',$v['end_time']);
            $list[$k]['type']=self::suppierType($v['type']);
            $list[$k]['status']=$v['status']==1?'开通':'锁定';
        }
        $expCellName  = array(
            array('supplier_sn','编号'),
            array('supplier_name','商家名称'),
            array('type','项目'),
            array('cooperation','合作时间'),
            array('Linkman','联系人'),
            array('Linkman_phone','联系电话'),
            array('status','状态'),
        );

        $fileName='交通供应商列表';
        switch ($where['type']){
            case 2:$fileName='酒店供应商';break;
            case 3:$fileName='保险供应商';break;
            case 4:$fileName='景区供应商';break;
        }
        parent::exportExcel($fileName,$expCellName,$list);
    }


    /**
     * 返回是哪一类型的商家
     */
    private function  suppierType($type){
        $name='';
        switch ($type){
            case 1:$name='交通供应商';break;
            case 2:$name='酒店供应商';break;
            case 3:$name='保险供应商';break;
            case 4:$name='景区供应商';break;
        }
        return $name;
    }


    /**
     * 添加交通单品
     */
    public function addTraffic($data){

        $newData=[];
        $db=M('public_single_traffic_price');
        //判断是否重复添加
        $existsWhere['traffic_id']=$data['traffic_id'];
        $existsWhere['start_city']=$data['start_city'];
        $existsWhere['goal_city']=$data['goal_city'];
        $existsWhere['supplier_id']=$data['supplier_id'];

        $seatArr=$data['room'];


        foreach ($seatArr as $k=> $v){
            $existsWhere['seat_id']=$k;
            $exists=$db->where($existsWhere)->find();
            if($exists){
                continue;
            }
            if($v['adult_actual_cost']&&$v['child_actual_cost']&&$v['room_count']){
                $addData['seat_id']=$k;
                $addData['is_review']=-1;
                $addData['is_shelves']=-1;
                $addData['total_actual_cost']=$v['child_actual_cost']+$v['adult_actual_cost'];
                $addData['adult_actual_cost']=$v['adult_actual_cost'];
                $addData['child_actual_cost']=$v['child_actual_cost'];
                $addData['entry_id']=session('operator_user.operator_id');
                $addData['supplier_id']=$data['supplier_id'];
                $addData['traffic_id']=$data['traffic_id'];
                $addData['start_country']=$data['start_country'];
                $addData['start_province']=$data['start_province'];
                $addData['start_city']=$data['start_city'];
                $addData['start_area']=$data['start_area'];
                $addData['money_type']=$data['money_type'];
                $addData['goal_country']=$data['goal_country'];
                $addData['goal_province']=$data['goal_province'];
                $addData['goal_city']=$data['goal_city'];
                $addData['goal_area']=$data['goal_area'];
                $addData['create_time']=time();
                $addData['product_sn']=self::LineSn();
                $addData['seat_num']=$v['room_count'];
                $addData['routes_name']=$data['routes_name'];
                $newData[]=$addData;
            }
        }
        if(!$newData){
            return ['status'=>0,'msg'=>'数据填写不完整或者重复添加!'];
        }

        $res=  $db->addAll($newData);
        if($res){
            return  ['status'=>1,'msg'=>'添加成功!'];
        }
        return  ['status'=>0,'msg'=>'添加失败!'];

    }



    /**
     * 交通线路编号
     */
    public  function LineSn(){
        $product_sn= M('public_single_traffic_price')->order('product_sn desc')->getField('product_sn');
        if(!$product_sn){
            return "10000".mt_rand(100,999);
        }
        return $product_sn+mt_rand(100,999);
    }














    /**
     * 房间类型
     */
    public  function  hotelRoom(){
        return M('public_single_room')->select();
    }


    /**
     * 添加房间
     */
    public function addHotelRoom($data){


        if(!$data['supplier_id']){
            return ['status'=>0,'msg'=>'添加失败!'];
        }

        $scenicData['room_name']=$data['room_name'];
        $scenicData['supplier_id']=$data['supplier_id'];
        $scenicData['star_level']=$data['star_level'];
        $scenicData['create_time']=time();
        M()->startTrans();
        $scenicRes=M('public_single_room')->add($scenicData);

        $priceData['room_id']=$scenicRes;
        $priceData['money_type']=$data['money_type'];

        $priceData['total_actual_cost']=$data['child_actual_cost']+$data['adult_actual_cost'];
        $priceData['child_actual_cost']=$data['child_actual_cost'];
        $priceData['adult_actual_cost']=$data['adult_actual_cost'];
        $priceData['room_count']=$data['room_count'];
        $priceData['star_level']=$data['star_level'];

        $priceData['create_time']=time();
        $priceData['entry_id']=session('operator_user.operator_id');
        $priceData['supplier_id']=$data['supplier_id'];
        $priceData['user_count']=$data['user_count'];
        $priceData['product_sn']=self::roomSn();

        $priceRes=M('public_single_room_price')->add($priceData);

        if($scenicRes&&$priceRes){
            M()->commit();
            return ['status'=>1,'msg'=>'添加成功!'];
        }
        M()->rollback();
        return ['status'=>0,'msg'=>'添加失败!'];



    }





    /**
     * 房间编号
     */
    public  function roomSn(){
        $product_sn= M('public_single_room_price')->order('product_sn desc')->getField('product_sn');
        if(!$product_sn){
            return "10000".mt_rand(100,999);
        }
        return $product_sn+mt_rand(100,999);
    }

    /**
     * 根据酒店单品供应商ID获取产品列表
     */
    public  function  hotelProductById($where){
        $db_prefix=C('DB_PREFIX');
        $count      = M('public_single_room_price as p')
            ->join("{$db_prefix}public_single_supplier as si on p.supplier_id=si.supplier_id")
            ->join("LEFT JOIN {$db_prefix}operator as sp on p.entry_id=sp.operator_id")
            ->where($where)->count();// 查询满足要求的总记录数
        $Page       = new \Think\Page($count,25);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show       = $Page->show();// 分页显示输出
        // 进行分页数据查询 注意limit方法的参数要使用Page类的属性
        $list=M('public_single_room_price as p')
            ->join("{$db_prefix}public_single_supplier as si on p.supplier_id=si.supplier_id")
            ->join("{$db_prefix}public_single_room as r on p.room_id=r.room_id")
            ->join("LEFT JOIN {$db_prefix}operator as sp on p.entry_id=sp.operator_id")
            ->where($where)
            ->field('p.*,si.supplier_name as si_name,sp.operator_name as sp_name,r.room_name')
            ->order('p.room_priceId desc')
            ->limit($Page->firstRow.','.$Page->listRows)
            ->select();
        return ['list'=>$list,'show'=>$show];

    }



    /**
     * 某个酒店供应商商品导出Excel
     */
    public  function hotelExportExcelBySupplierId($where){
        $db_prefix=C('DB_PREFIX');
        $list=M('public_single_room_price as p')
            ->join("{$db_prefix}public_single_supplier as si on p.supplier_id=si.supplier_id")
            ->join("{$db_prefix}public_single_room as r on p.room_id=r.room_id")
            ->join("LEFT JOIN {$db_prefix}operator as sp on p.entry_id=sp.operator_id")
            ->where($where)
            ->field('p.*,si.supplier_name as si_name,sp.operator_name as sp_name,r.room_name')
            ->order('p.room_priceId desc')
            ->select();
        foreach ($list as $k=>$v){
           $list[$k]['actual_cost']="成人:".$v['adult_actual_cost']."/人 \n\r"."儿童:".$v['child_actual_cost']."/人";
           $list[$k]['is_review']=self::is_review($v['is_review']);
           $list[$k]['is_shelves']=$v['is_shelves']==1?'上架':'下架';
        }

        $currency=A('Operator/Base')->getCurrency();

        $expCellName  = array(
            array('si_name','供应商名称'),
            array('star_level','星级'),
            array('product_sn','编号'),
            array('room_name','房间'),
/*            array('foreign_cost','外币本价'),
            array('exchange_rate','汇率'),*/
            array('actual_cost','成本价('.$currency.")"),
            array('sp_name','操作人'),
            array('is_review','审核状态'),
            array('is_shelves','是否上架'),
        );


        $fileName=$list[0]['si_name']."产品列表";
        parent::exportExcel($fileName,$expCellName,$list);
    }

    /**
     * 某个酒店供应商商品导出Excel
     */
    public  function trafficExportExcelBySupplierId($where){
        $db_prefix=C('DB_PREFIX');
        $list=M('public_single_traffic_price as p')
            ->join("{$db_prefix}public_single_supplier as si on p.supplier_id=si.supplier_id")
            ->join("__PUBLIC_SINGLE_TRAFFIC_SEAT__ as s on s.seat_id=p.seat_id")
            ->join("{$db_prefix}public_single_traffic as r on p.traffic_id=r.traffic_id")
            ->join("LEFT JOIN {$db_prefix}operator as sp on p.entry_id=sp.operator_id")
            ->where($where)
            ->field('p.*,si.supplier_name as si_name,sp.operator_name as sp_name,r.traffic_name,s.seat_name')
            ->order('p.traffic_priceId desc')
            ->select();
        foreach ($list as $k=>$v){
           $list[$k]['actual_cost']="成人:".$v['adult_actual_cost']."/人 \n\r"."儿童:".$v['child_actual_cost']."/人";
           $list[$k]['is_review']=self::is_review($v['is_review']);
           $list[$k]['traffic_name']=$v['traffic_name'].'-'.$v['seat_name'];
           $list[$k]['is_shelves']=$v['is_shelves']==1?'上架':'下架';
        }

        $currency=A('Operator/Base')->getCurrency();
        $expCellName  = array(
            array('si_name','供应商名称'),
            array('routes_name','线路名称'),
            array('product_sn','编号'),
            array('traffic_name','交通方式'),
            array('actual_cost','成本价('.$currency.")"),
            array('sp_name','操作人'),
            array('is_review','审核状态'),
            array('is_shelves','是否上架'),
        );

        $fileName=$list[0]['si_name']."线路列表";
        parent::exportExcel($fileName,$expCellName,$list);
    }

    /**
     * 审核状态
     */
    private function is_review($status){
        $name='';
        switch ($status){
            case 1:$name='已通过';break;
            case -1:$name='未审核';break;
            case -2:$name='已拒绝';break;
        }
        return $name;
    }


    /**
     * 酒店单品上下架
     */
    public  function  hotelRoomShelvesById($data){
        $roomInfo=M('public_single_room_price')->where(array('room_priceId'=>$data['room_priceId']))->find();
        if($roomInfo['is_review']!=1){
            return ['status'=>-1,'msg'=>'操作失败,此房间尚未审核!'];
        }
        $res=M('public_single_room_price')->where(array('room_priceId'=>$data['room_priceId']))->setField(['is_shelves'=>$data['is_shelves']]);
        if($res){
            return ['status'=>1,'msg'=>'操作成功!'];
        }
        return ['status'=>0,'msg'=>'操作失败!'];
    }

    /**
     * 线路单品上下架
     */
    public  function  trafficShelvesById($data){
        $roomInfo=M('public_single_traffic_price')->where(array('traffic_priceId'=>$data['traffic_priceId']))->find();
        if($roomInfo['is_review']!=1){
            return ['status'=>-1,'msg'=>'操作失败,此线路尚未审核!'];
        }
        $res=M('public_single_traffic_price')->where(array('traffic_priceId'=>$data['traffic_priceId']))->setField(['is_shelves'=>$data['is_shelves']]);
        if($res){
            return ['status'=>1,'msg'=>'操作成功!'];
        }
        return ['status'=>0,'msg'=>'操作失败!'];
    }









    /**
     * 酒店单品审核
     */
    public  function  editHotelRoom($data){

        if(!$data['supplier_id']||!$data['room_priceId']||!$data['room_id']){
            return ['status'=>0,'msg'=>'操作失败!'];
        }

        $roomSave['star_level']=$data['star_level'];
        $roomSave['room_name']=$data['room_name'];
        $roomRes=M('public_single_room')->where(array('room_id'=>$data['room_id']))->save($roomSave);

        $save['audit_id']=session('operator_user.operator_id');
        $save['money_type']=$data['money_type'];
        $save['total_actual_cost']=$data['child_actual_cost']+$data['adult_actual_cost'];
        $save['child_actual_cost']=$data['child_actual_cost'];
        $save['adult_actual_cost']=$data['adult_actual_cost'];
        $save['room_count']=$data['room_count'];
        $save['star_level']=$data['star_level'];
        $save['user_count']=$data['user_count'];
        $save['is_review']=$data['is_review'];

        $res=M('public_single_room_price')->where(array('room_priceId'=>$data['room_priceId']))->save($save);
        if($roomRes!==false&&$res!==false){
            return ['status'=>1,'msg'=>'操作成功!'];
        }
        return ['status'=>0,'msg'=>'操作失败!'];
    }

    /**
     * 获取酒店详情
     */
    public  function  getHotelDetail($room_priceId){
        return M('public_single_room_price as p')
            ->join('__PUBLIC_SINGLE_ROOM__ as r on p.room_id=r.room_id')
            ->where(array('room_priceId'=>$room_priceId))
            ->find();
    }

    /**
     * 交通方式列表,如汽车,轮船
     */
    public  function  trafficList(){
        return M('public_single_traffic')->select();
    }

    /**
     * 根据交通单品供应商ID获取产品列表
     */
    public  function  trafficProductById($where){
        $db_prefix=C('DB_PREFIX');
        $count      = M('public_single_traffic_price as p')
            ->join("{$db_prefix}public_single_supplier as si on p.supplier_id=si.supplier_id")
            ->join("LEFT JOIN {$db_prefix}operator as sp on p.entry_id=sp.operator_id")
            ->where($where)->count();// 查询满足要求的总记录数
        $Page       = new \Think\Page($count,25);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show       = $Page->show();// 分页显示输出
        // 进行分页数据查询 注意limit方法的参数要使用Page类的属性
        $list=M('public_single_traffic_price as p')
            ->join("{$db_prefix}public_single_supplier as si on p.supplier_id=si.supplier_id")
            ->join("LEFT JOIN __PUBLIC_SINGLE_TRAFFIC_SEAT__ as s on s.seat_id=p.seat_id")
            ->join("{$db_prefix}public_single_traffic as r on p.traffic_id=r.traffic_id")
            ->join("LEFT JOIN {$db_prefix}operator as sp on p.entry_id=sp.operator_id")
            ->where($where)
            ->field('p.*,si.supplier_name as si_name,sp.operator_name as sp_name,r.traffic_name,s.seat_name')
            ->order('p.traffic_priceId desc')
            ->limit($Page->firstRow.','.$Page->listRows)
            ->select();
        return ['list'=>$list,'show'=>$show];

    }

    /**
     * 获取线路详情
     */
    public  function  getTraficInfo($traffic_priceId){
        return M('public_single_traffic_price as p')
            ->join('__PUBLIC_SINGLE_TRAFFIC__ as t on t.traffic_id=p.traffic_id')
            ->join("__PUBLIC_SINGLE_TRAFFIC_SEAT__ as s on s.seat_id=p.seat_id")
            ->where(['traffic_priceId'=>$traffic_priceId])->find();
    }

    /**
     * 查询出发城市及目的城市,用于编辑
     */
    public  function getTrafficCity($info){
        $list['start_province_list']=D('Platform')->getProvinceByCountryId($info['start_country']);
        $list['start_city_list']=D('Platform')->getCity($info['start_province']);
        $list['start_area_list']=D('Platform')->getArea($info['start_city']);

        $list['goal_province_list']=D('Platform')->getProvinceByCountryId($info['goal_country']);
        $list['goal_city_list']=D('Platform')->getCity($info['goal_province']);
        $list['goal_area_list']=D('Platform')->getArea($info['goal_city']);

        return $list;
    }





    /**
     * 编辑线路
     */
    public  function  editTraffic($data){
        $save=$data;
        $save['audit_id']=session('operator_user.operator_id');
        $res=M('public_single_traffic_price')->where(array('traffic_priceId'=>$data['traffic_priceId']))->save($save);
        if($res){
            return  ['status'=>1,'msg'=>'操作成功!'];
        }
        return  ['status'=>0,'msg'=>'操作失败!'];
    }


    /**
     * 添加景点及价格
     */
    public function  addScenic($data){

        if(!$data['supplier_id']){
            return ['status'=>0,'msg'=>'添加失败!'];
        }


        $scenicData['scenic_name']=$data['scenic_name'];
        $scenicData['supplier_id']=$data['supplier_id'];
        $scenicData['scenic_level']=$data['scenic_level'];

        $scenicData['create_time']=time();
        M()->startTrans();
        $scenicRes=M('public_single_scenic')->add($scenicData);


        $priceData['scenic_id']=$scenicRes;
        $priceData['money_type']=$data['money_type'];

        $priceData['total_actual_cost']=$data['child_actual_cost']+$data['adult_actual_cost'];
        $priceData['child_actual_cost']=$data['child_actual_cost'];
        $priceData['adult_actual_cost']=$data['adult_actual_cost'];
        $priceData['seat_num']=$data['seat_num'];
        $priceData['create_time']=time();
        $priceData['entry_id']=session('operator_user.operator_id');
        $priceData['supplier_id']=$data['supplier_id'];
        $priceData['product_sn']=self::scenicSn();

        $priceRes=M('public_single_scenic_price')->add($priceData);

        if($scenicRes&&$priceRes){
            M()->commit();
            return ['status'=>1,'msg'=>'添加成功!'];
        }
        M()->rollback();
        return ['status'=>0,'msg'=>'添加失败!'];

    }

    /**
     * 景区编号
     */
    public  function scenicSn(){
        $product_sn= M('public_single_scenic_price')->order('product_sn desc')->getField('product_sn');
        if(!$product_sn){
            return "10000".mt_rand(100,999);
        }
        return $product_sn+mt_rand(100,999);
    }


    /**
     * 根据景区单品供应商ID获取产品列表
     */
    public  function  scenicProductById($where){
        $db_prefix=C('DB_PREFIX');
        $count      = M('public_single_scenic_price as p')
            ->join("{$db_prefix}public_single_supplier as si on p.supplier_id=si.supplier_id")
            ->join("{$db_prefix}public_single_scenic as sc on p.scenic_id=sc.scenic_id")
            ->join("LEFT JOIN {$db_prefix}operator as sp on p.entry_id=sp.operator_id")

            ->where($where)->count();// 查询满足要求的总记录数
        $Page       = new \Think\Page($count,25);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show       = $Page->show();// 分页显示输出
        // 进行分页数据查询 注意limit方法的参数要使用Page类的属性
        $list=M('public_single_scenic_price as p')
            ->join("{$db_prefix}public_single_supplier as si on p.supplier_id=si.supplier_id")
            ->join("{$db_prefix}public_single_scenic as sc on sc.scenic_id=p.scenic_id")
            ->join("LEFT JOIN {$db_prefix}operator as sp on p.entry_id=sp.operator_id")
            ->where($where)
            ->field('p.*,si.supplier_name as si_name,sp.operator_name as sp_name,sc.*')
            ->order('p.scenic_priceId desc')
            ->limit($Page->firstRow.','.$Page->listRows)
            ->select();
        return ['list'=>$list,'show'=>$show];

    }


    /**
     * 某个景区供应商商品导出Excel
     */
    public  function scenicExportExcelBySupplierId($where){
        $db_prefix=C('DB_PREFIX');
        $list=M('public_single_scenic_price as p')
            ->join("{$db_prefix}public_single_supplier as si on p.supplier_id=si.supplier_id")
            ->join("{$db_prefix}public_single_scenic as sc on sc.scenic_id=p.scenic_id")
            ->join("LEFT JOIN {$db_prefix}operator as sp on p.entry_id=sp.operator_id")
            ->where($where)
            ->field('p.*,si.supplier_name as si_name,sp.operator_name as sp_name,sc.*')
            ->order('p.scenic_priceId desc')
            ->select();
        foreach ($list as $k=>$v){
            $list[$k]['actual_cost']="成人:".$v['adult_actual_cost']."/人 \n\r"."儿童:".$v['child_actual_cost']."/人";
            $list[$k]['is_review']=self::is_review($v['is_review']);
            $list[$k]['is_shelves']=$v['is_shelves']==1?'上架':'下架';
        }

        // 币种
        $currency=A('Operator/Base')->getCurrency();

        $expCellName  = array(
            array('scenic_name','景区名称'),
            array('scenic_level','等级'),
            array('product_sn','编号'),
            array('actual_cost','成本价('.$currency.")"),
            array('sp_name','操作人'),
            array('is_review','审核状态'),
            array('is_shelves','是否上架'),
        );
        $fileName=$list[0]['si_name']."景区列表";
        parent::exportExcel($fileName,$expCellName,$list);
    }



    /**
     * 景区单品上下架
     */
    public  function  scenicShelvesById($data){
        $roomInfo=M('public_single_scenic_price')->where(array('scenic_priceId'=>$data['scenic_priceId']))->find();
        if($roomInfo['is_review']!=1){
            return ['status'=>-1,'msg'=>'操作失败,此景区尚未审核!'];
        }
        $res=M('public_single_scenic_price')->where(array('scenic_priceId'=>$data['scenic_priceId']))->setField(['is_shelves'=>$data['is_shelves']]);
        if($res){
            return ['status'=>1,'msg'=>'操作成功!'];
        }
        return ['status'=>0,'msg'=>'操作失败!'];
    }

    /**
     * 获取景区详情
     */
    public  function  getScenicInfo($scenic_priceId){
        $info=M('public_single_scenic_price as p')
            ->join('__PUBLIC_SINGLE_SCENIC__ as sc on sc.scenic_id=p.scenic_id')
            ->where(array('p.scenic_priceId'=>$scenic_priceId))
            ->find();
        return $info;
    }


    /**
     * 查询出景区所在地信息
     */
    public  function getScenicCity($info){
        $list['province_list']=D('Platform')->getProvinceByCountryId($info['country']);
        $list['city_list']=D('Platform')->getCity($info['province']);
        $list['area_list']=D('Platform')->getArea($info['city']);
        return $list;
    }


    /**
     * 审核景点
     */
    public  function  editScenic($data){
        if(!$data['supplier_id']||!$data['scenic_priceId']){
            return ['status'=>0,'msg'=>'操作失败!'];
        }


        $scenicData['scenic_name']=$data['scenic_name'];
        $scenicData['supplier_id']=$data['supplier_id'];


        $scenicRes=M('public_single_scenic')->where(array('scenic_id'=>$data['scenic_id']))->save($scenicData);



        $priceData['money_type']=$data['money_type'];

        $priceData['total_actual_cost']=$data['child_actual_cost']+$data['adult_actual_cost'];
        $priceData['child_actual_cost']=$data['child_actual_cost'];
        $priceData['adult_actual_cost']=$data['adult_actual_cost'];
        $priceData['seat_num']=$data['seat_num'];
        $priceData['is_review']=$data['is_review'];
        $priceData['audit_id']=session('operator_user.operator_id');

        $priceRes=M('public_single_scenic_price')->where(array('scenic_priceId'=>$data['scenic_priceId']))->save($priceData);

        if($scenicRes!==false&&$priceRes!==false){
            return ['status'=>1,'msg'=>'操作成功!'];
        }

        return ['status'=>0,'msg'=>'操作失败!'];
    }


    /**
     * 添加保险
     */
    public function addInsure($data){

        if(!$data['supplier_id']){
            return ['status'=>0,'msg'=>'添加失败!'];
        }
        $scenicData['insure_name']=$data['insure_name'];
        $scenicData['supplier_id']=$data['supplier_id'];

        $scenicData['create_time']=time();
        M()->startTrans();
        $scenicRes=M('public_single_insure')->add($scenicData);

        $priceData['insure_id']=$scenicRes;
        $priceData['money_type']=$data['money_type'];


        $priceData['total_actual_cost']=$data['child_actual_cost']+$data['adult_actual_cost'];
        $priceData['child_actual_cost']=$data['child_actual_cost'];
        $priceData['adult_actual_cost']=$data['adult_actual_cost'];
        $priceData['seat_num']=$data['seat_num'];
        $priceData['create_time']=time();
        $priceData['entry_id']=session('operator_user.operator_id');
        $priceData['supplier_id']=$data['supplier_id'];
        $priceData['product_sn']=self::insureSn();

        $priceRes=M('public_single_insure_price')->add($priceData);

        if($scenicRes&&$priceRes){
            M()->commit();
            return ['status'=>1,'msg'=>'添加成功!'];
        }
        M()->rollback();
        return ['status'=>0,'msg'=>'添加失败!'];
    }


    /**
     * 保险编号
     */
    public  function insureSn(){
        $product_sn= M('public_single_insure_price')->order('product_sn desc')->getField('product_sn');
        if(!$product_sn){
            return "10000".mt_rand(100,999);
        }
        return $product_sn+mt_rand(100,999);
    }



    /**
     * 根据景区单品供应商ID获取产品列表
     */
    public  function  insureProductById($where){
        $db_prefix=C('DB_PREFIX');
        $count      = M('public_single_insure_price as p')
            ->join("{$db_prefix}public_single_supplier as si on p.supplier_id=si.supplier_id")
            ->join("{$db_prefix}public_single_insure as sc on p.insure_id=sc.insure_id")
            ->join("LEFT JOIN {$db_prefix}operator as sp on p.entry_id=sp.operator_id")

            ->where($where)->count();// 查询满足要求的总记录数
        $Page       = new \Think\Page($count,25);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show       = $Page->show();// 分页显示输出
        // 进行分页数据查询 注意limit方法的参数要使用Page类的属性
        $list=M('public_single_insure_price as p')
            ->join("{$db_prefix}public_single_supplier as si on p.supplier_id=si.supplier_id")
            ->join("{$db_prefix}public_single_insure as sc on sc.insure_id=p.insure_id")
            ->join("LEFT JOIN {$db_prefix}operator as sp on p.entry_id=sp.operator_id")
            ->where($where)
            ->field('p.*,si.supplier_name as si_name,sp.operator_name as sp_name,sc.*')
            ->order('p.insure_priceId desc')
            ->limit($Page->firstRow.','.$Page->listRows)
            ->select();
        return ['list'=>$list,'show'=>$show];

    }



    /**
     *  保险单品上下架
     */
    public  function  insureShelvesById($data){
        $roomInfo=M('public_single_insure_price')->where(array('insure_priceId'=>$data['insure_priceId']))->find();
        if($roomInfo['is_review']!=1){
            return ['status'=>-1,'msg'=>'操作失败,此保险尚未审核!'];
        }
        $res=M('public_single_insure_price')->where(array('insure_priceId'=>$data['insure_priceId']))->setField(['is_shelves'=>$data['is_shelves']]);
        if($res){
            return ['status'=>1,'msg'=>'操作成功!'];
        }
        return ['status'=>0,'msg'=>'操作失败!'];
    }


    /**
     * 审核保险
     */
    public function  editInsure($data){
        if(!$data['supplier_id']||!$data['insure_priceId']||!$data['insure_id']){
            return ['status'=>0,'msg'=>'添加失败!'];
        }
        $scenicData['insure_name']=$data['insure_name'];
        $scenicRes=M('public_single_insure')->where(['insure_id'=>$data['insure_id']])->save($scenicData);



        $priceData['money_type']=$data['money_type'];


        $priceData['total_actual_cost']=$data['child_actual_cost']+$data['adult_actual_cost'];
        $priceData['child_actual_cost']=$data['child_actual_cost'];
        $priceData['adult_actual_cost']=$data['adult_actual_cost'];
        $priceData['seat_num']=$data['seat_num'];
        $priceData['is_review']=$data['is_review'];
        $priceData['audit_id']=session('operator_user.operator_id');

        $priceRes=M('public_single_insure_price')->where(['insure_priceId'=>$data['insure_priceId']])->save($priceData);
        if($scenicRes!==false&&$priceRes!==false){
            return ['status'=>1,'msg'=>'操作成功!'];
        }

        return ['status'=>0,'msg'=>'添加失败!'];
    }

    /**
     * 获取保险详情
     */
    public  function  getInsureDetail($insure_priceId){
        return M('public_single_insure_price as p')
            ->join('__PUBLIC_SINGLE_INSURE__ as r on p.insure_id=r.insure_id')
            ->where(array('insure_priceId'=>$insure_priceId))
            ->find();
    }



    /**
     * 某个保险供应商商品导出Excel
     */
    public  function insureExportExcelBySupplierId($where){
        $db_prefix=C('DB_PREFIX');
        $list=M('public_single_insure_price as p')
            ->join("{$db_prefix}public_single_supplier as si on p.supplier_id=si.supplier_id")
            ->join("{$db_prefix}public_single_insure as sc on sc.insure_id=p.insure_id")
            ->join("LEFT JOIN {$db_prefix}operator_line_supplier as sp on p.entry_id=sp.supplier_id")
            ->where($where)
            ->field('p.*,si.supplier_name as si_name,sp.supplier_name as sp_name,sc.*')
            ->order('p.insure_priceId desc')
            ->select();
        foreach ($list as $k=>$v){
            $list[$k]['actual_cost']="成人:".$v['adult_actual_cost']."/人 \n\r"."儿童:".$v['child_actual_cost']."/人";
            $list[$k]['is_review']=self::is_review($v['is_review']);
            $list[$k]['is_shelves']=$v['is_shelves']==1?'上架':'下架';
        }

        // 币种
        $currency=A('Operator/Base')->getCurrency();

        $expCellName  = array(
            array('si_name','保险名称'),
            array('insure_name','保险名称'),
            array('product_sn','编号'),
            array('actual_cost','成本价('.$currency.")"),
            array('sp_name','操作人'),
            array('is_review','审核状态'),
            array('is_shelves','是否上架'),
        );

        $fileName=$list[0]['si_name']."保险列表";
        parent::exportExcel($fileName,$expCellName,$list);
    }


    /**
     * 交通供应商合作记录
     */
    public  function  trafficPartnerRecord($id){

        //供应商名称
        $supplier_name=M('public_single_supplier')->where(array('supplier_id'=>$id))->getField('supplier_name');
        $countWhere['u.unit_supplier_id']=$id;
        $countWhere['u.source_type']=1;//交通
        $count=M('order_unit_detail as u')
            ->where($countWhere)
            ->count();
        $Page       = new \Think\Page($count,25);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show       = $Page->show();// 分页显示输出

        //包含了此单品供应的团号列表信息
        $groupList=M('order_unit_detail as u')
            ->join("LEFT JOIN __OPERATOR__ as op on u.closing_operator_id=op.operator_id")
            ->join('LEFT JOIN __LINE_ORDERS__ as o on o.order_id=u.order_id')
            ->join("LEFT JOIN __PUBLIC_LINE__ as l on l.line_id=o.line_id")
            ->join("LEFT JOIN __AREAS__ as a on a.areaId=l.origin_id")
            ->join("lEFT JOIN __PUBLIC_SINGLE_TRAFFIC_PRICE__ as tp on tp.traffic_priceId=u.source_id")
            ->join('LEFT JOIN __PUBLIC_SINGLE_TRAFFIC_SEAT__ as ts on ts.seat_id=tp.seat_id')
            ->where($countWhere)
            ->field('u.*,op.operator_name,l.line_name,ts.seat_name,tp.routes_name,a.areaName')
            ->limit($Page->firstRow.','.$Page->listRows)
            ->select();
        foreach ($groupList as $k=>$v){
            //总人数
            $groupList[$k]['headcount']='成人'.$v['adult_count'].',儿童'.$v['child_count'].',老人'.$v['old_man_count'];
            //供应商名称
            $groupList[$k]['supplier_name']=$supplier_name;
        }
        return ['list'=>$groupList,'show'=>$show];
    }



    /**
     * @param $id 交通供应商supplier_id
     * @return array 导出交通单品供应商的合作记录
     */
    public function exportTrafficPartnerRecord($id){
        //供应商名称
        $supplier_name=M('public_single_supplier')->where(array('supplier_id'=>$id))->getField('supplier_name');
        $countWhere['u.unit_supplier_id']=$id;
        $countWhere['u.source_type']=1;//交通

        //包含了此单品供应的团号列表信息
        $groupList=M('order_unit_detail as u')
            ->join("LEFT JOIN __OPERATOR__ as op on u.closing_operator_id=op.operator_id")
            ->join('LEFT JOIN __LINE_ORDERS__ as o on o.order_id=u.order_id')
            ->join("LEFT JOIN __PUBLIC_LINE__ as l on l.line_id=o.line_id")
            ->join("LEFT JOIN __AREAS__ as a on a.areaId=l.origin_id")
            ->join("lEFT JOIN __PUBLIC_SINGLE_TRAFFIC_PRICE__ as tp on tp.traffic_priceId=u.source_id")
            ->join('LEFT JOIN __PUBLIC_SINGLE_TRAFFIC_SEAT__ as ts on ts.seat_id=tp.seat_id')
            ->where($countWhere)
            ->field('u.*,op.operator_name,l.line_name,ts.seat_name,tp.routes_name')
            ->select();
        foreach ($groupList as $k=>$v){
            //总人数
            $groupList[$k]['headcount']='成人'.$v['adult_count'].',儿童'.$v['child_count'].',老人'.$v['old_man_count'];
            $groupList[$k]['supplier_name']=$supplier_name;
            $groupList[$k]['item']='交通';
            $groupList[$k]['operator_name']=$v['operator_name']?$v['operator_name']:"无";
            $groupList[$k]['closing_time']=$v['closing_time']?date('Y-m-d H:i:s',$v['closing_time']):'无';
        }


        $expCellName  = array(
            array('supplier_name','供应商名称'),
            array('routes_name','航线名称'),
            array('areaName','出发城市'),
            array('group_num','团号'),
            array('item','项目'),
            array('headcount','数量'),
            array('unit_cost','结算金额'),
            array('closing_time','结算时间'),
            array('operator_name','操作人（结算）'),
        );

        $fileName=$supplier_name."交通供应商合作记录";
        parent::exportExcel($fileName,$expCellName,$groupList);

    }


    /**
     * 保险供应商合作记录
     */
    public  function  insurePartnerRecord($id){
        //供应商名称
        $supplier_name=M('public_single_supplier')->where(array('supplier_id'=>$id))->getField('supplier_name');
        $countWhere['u.unit_supplier_id']=$id;
        $countWhere['u.source_type']=4;//保险
        $count=M('order_unit_detail as u')
            ->where($countWhere)
            ->count();
        $Page       = new \Think\Page($count,25);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show       = $Page->show();// 分页显示输出

        //包含了此单品供应的团号列表信息
        $groupList=M('order_unit_detail as u')
            ->join("LEFT JOIN __OPERATOR__ as op on u.closing_operator_id=op.operator_id")
            ->join('LEFT JOIN __LINE_ORDERS__ as o on o.order_id=u.order_id')
            ->join("LEFT JOIN __PUBLIC_LINE__ as l on l.line_id=o.line_id")
            ->where($countWhere)
            ->field('u.*,op.operator_name,l.line_name')
            ->limit($Page->firstRow.','.$Page->listRows)
            ->select();
        foreach ($groupList as $k=>$v){
            //总人数
            $groupList[$k]['headcount']='成人'.$v['adult_count'].',儿童'.$v['child_count'].',老人'.$v['old_man_count'];
            //供应商名称
            $groupList[$k]['supplier_name']=$supplier_name;
        }
        return ['list'=>$groupList,'show'=>$show];
    }


    /**
     * 保险单品供应商合作记录导出EXCEL
     */
    public  function exportInsurePartnerRecord($id){
        //供应商名称
        $supplier_name=M('public_single_supplier')->where(array('supplier_id'=>$id))->getField('supplier_name');
        //包含了此单品供应的团号列表信息
        $countWhere['u.unit_supplier_id']=$id;
        $countWhere['u.source_type']=4;//保险
        $groupList=M('order_unit_detail as u')
            ->join("LEFT JOIN __OPERATOR__ as op on u.closing_operator_id=op.operator_id")
            ->join('LEFT JOIN __LINE_ORDERS__ as o on o.order_id=u.order_id')
            ->join("LEFT JOIN __PUBLIC_LINE__ as l on l.line_id=o.line_id")
            ->where($countWhere)
            ->field('u.*,op.operator_name,l.line_name')
            ->select();
        foreach ($groupList as $k=>$v){
            //总人数
            $groupList[$k]['headcount']='成人'.$v['adult_count'].',儿童'.$v['child_count'].',老人'.$v['old_man_count'];
            //供应商名称
            $groupList[$k]['supplier_name']=$supplier_name;
            $groupList[$k]['item']='保险';
            $groupList[$k]['operator_name']=$v['operator_name']?$v['operator_name']:'无';
            $groupList[$k]['closing_time']=$v['closing_time']?date('Y-m-d H:i:s',$v['closing_time']):'无';
        }

        $expCellName  = array(
            array('supplier_name','供应商名称'),
            array('line_name','线路名称'),
            array('group_num','团号'),
            array('item','项目'),
            array('headcount','数量'),
            array('unit_cost','结算金额'),
            array('closing_time','结算时间'),
            array('operator_name','操作人（结算）'),
        );
        $fileName=$supplier_name."保险供应商合作记录";
        parent::exportExcel($fileName,$expCellName,$groupList);

    }



    /**
     * 景区供应商
     */
    public function  scenicPartnerRecord($id){
        //供应商名称
        $supplier_name=M('public_single_supplier')->where(array('supplier_id'=>$id))->getField('supplier_name');
        $countWhere['u.unit_supplier_id']=$id;
        $countWhere['u.source_type']=3;//景区
        $count=M('order_unit_detail as u')
            ->where($countWhere)
            ->count();
        $Page       = new \Think\Page($count,25);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show       = $Page->show();// 分页显示输出

        //包含了此单品供应的团号列表信息
        $groupList=M('order_unit_detail as u')
            ->join("LEFT JOIN __OPERATOR__ as op on u.closing_operator_id=op.operator_id")
            ->join('LEFT JOIN __LINE_ORDERS__ as o on o.order_id=u.order_id')
            ->join("LEFT JOIN __PUBLIC_LINE__ as l on l.line_id=o.line_id")
            ->where($countWhere)
            ->field('u.*,op.operator_name,l.line_name')
            ->limit($Page->firstRow.','.$Page->listRows)
            ->select();
        foreach ($groupList as $k=>$v){
            //总人数
            $groupList[$k]['headcount']='成人'.$v['adult_count'].',儿童'.$v['child_count'].',老人'.$v['old_man_count'];
            //供应商名称
            $groupList[$k]['supplier_name']=$supplier_name;
        }
        return ['list'=>$groupList,'show'=>$show];
    }


    /**
     * 景区单品供应商导出 EXCEL
     */
    public function  exportScenicPartnerRecord($id){
        //供应商名称
        $supplier_name=M('public_single_supplier')->where(array('supplier_id'=>$id))->getField('supplier_name');

        $countWhere['u.unit_supplier_id']=$id;
        $countWhere['u.source_type']=3;//景区

        //包含了此单品供应的团号列表信息
        $groupList=M('order_unit_detail as u')
            ->join("LEFT JOIN __OPERATOR__ as op on u.closing_operator_id=op.operator_id")
            ->join('LEFT JOIN __LINE_ORDERS__ as o on o.order_id=u.order_id')
            ->join("LEFT JOIN __PUBLIC_LINE__ as l on l.line_id=o.line_id")
            ->where($countWhere)
            ->field('u.*,op.operator_name,l.line_name')
            ->select();
        foreach ($groupList as $k=>$v){
            //总人数
            $groupList[$k]['headcount']='成人'.$v['adult_count'].',儿童'.$v['child_count'].',老人'.$v['old_man_count'];
            //供应商名称
            $groupList[$k]['supplier_name']=$supplier_name;
            $groupList[$k]['item']='景区';
            $groupList[$k]['operator_name']=$v['operator_name']?$v['operator_name']:'无';
            $groupList[$k]['closing_time']=$v['closing_time']?date('Y-m-d H:i:s',$v['closing_time']):'无';
        }

        $expCellName  = array(
            array('supplier_name','供应商名称'),
            array('line_name','线路名称'),
            array('group_num','团号'),
            array('item','项目'),
            array('headcount','数量'),
            array('unit_cost','结算金额'),
            array('closing_time','结算时间'),
            array('operator_name','操作人（结算）'),
        );

        $fileName=$supplier_name."景区供应商合作记录";
        parent::exportExcel($fileName,$expCellName,$groupList);

    }



    /**
     * 酒店供应商合作记录
     */
    public function  hotelPartnerRecord($id){
        //供应商名称
        $supplier_name=M('public_single_supplier')->where(array('supplier_id'=>$id))->getField('supplier_name');
        $countWhere['u.unit_supplier_id']=$id;
        $countWhere['u.source_type']=2;//酒店
        $count=M('order_unit_detail as u')
            ->where($countWhere)
            ->count();
        $Page       = new \Think\Page($count,25);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show       = $Page->show();// 分页显示输出

        //包含了此单品供应的团号列表信息
        $groupList=M('order_unit_detail as u')
            ->join("LEFT JOIN __OPERATOR__ as op on u.closing_operator_id=op.operator_id")
            ->join('LEFT JOIN __LINE_ORDERS__ as o on o.order_id=u.order_id')
            ->join("LEFT JOIN __PUBLIC_LINE__ as l on l.line_id=o.line_id")
            ->where($countWhere)
            ->field('u.*,op.operator_name,l.line_name')
            ->limit($Page->firstRow.','.$Page->listRows)
            ->select();
        foreach ($groupList as $k=>$v){
            //总人数
            $groupList[$k]['headcount']=$v['room_count'];
            //供应商名称
            $groupList[$k]['supplier_name']=$supplier_name;
        }
        return ['list'=>$groupList,'show'=>$show];
    }


    /**
     * 导出酒店单品供应商合作记录
     *
     */
    public  function  exportHotelPartnerRecord(){
        //供应商名称
        $supplier_name=M('public_single_supplier')->where(array('supplier_id'=>$id))->getField('supplier_name');
        $countWhere['u.unit_supplier_id']=$id;
        $countWhere['u.source_type']=2;//酒店
        $groupList=M('order_unit_detail as u')
            ->join("LEFT JOIN __OPERATOR__ as op on u.closing_operator_id=op.operator_id")
            ->join('LEFT JOIN __LINE_ORDERS__ as o on o.order_id=u.order_id')
            ->join("LEFT JOIN __PUBLIC_LINE__ as l on l.line_id=o.line_id")
            ->where($countWhere)
            ->field('u.*,op.operator_name,l.line_name')
            ->select();
        foreach ($groupList as $k=>$v){
            //总人数
            $groupList[$k]['headcount']=$v['room_count'];
            //供应商名称
            $groupList[$k]['supplier_name']=$supplier_name;
            $groupList[$k]['closing_time']=$v['closing_time']?date('Y-m-d H:i:s',$v['closing_time']):'无';
            $groupList[$k]['item']='酒店';
            $groupList[$k]['operator_name']=$v['operator_name']?$v['operator_name']:'无';
        }

        $expCellName  = array(
            array('supplier_name','供应商名称'),
            array('line_name','线路名称'),
            array('group_num','团号'),
            array('item','项目'),
            array('headcount','数量'),
            array('unit_cost','结算金额'),
            array('closing_time','结算时间'),
            array('operator_name','操作人（结算）'),
        );

        $fileName=$supplier_name."酒店供应商合作记录";
        parent::exportExcel($fileName,$expCellName,$groupList);
    }






    /**
     * 注册供应商列表数据
     */
    public  function lineShopsInfo($where){
        $count      = M('operator_line_supplier')->where($where)->count();// 查询满足要求的总记录数
        $Page       = new \Think\Page($count,25);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show       = $Page->show();// 分页显示输出
        // 进行分页数据查询 注意limit方法的参数要使用Page类的属性
        $list=M('operator_line_supplier')->where($where)->order('create_time desc')->limit($Page->firstRow.','.$Page->listRows)->select();
        // 获取详细地址
        foreach ($list as $key => $info) {
            $province_name = M('areas')->where(array('areaId'=>$info['province_id']))->getField('areaName');
            $city_name = M('areas')->where(array('areaId'=>$info['city_id']))->getField('areaName');
            $area_name = M('areas')->where(array('areaId'=>$info['area_id']))->getField('areaName');
            $list[$key]['complete_address'] = $province_name.$city_name.$area_name.$info['detailed_address'];
            if ($city_name==$province_name) {
                $list[$key]['complete_address'] = $province_name.$area_name.$info['detailed_address'];
            }
        }
        // dump(M('operator_line_supplier')->_sql());
        return ['list'=>$list,'show'=>$show];

    }

    /**
     * 添加注册供应商表
     */
    public function addLineShops($data){
        $admin_id=session('operator_user.pid')==0?session('operator_user.operator_id'):session('operator_user.pid');
        $data['operator_id']=$admin_id;
        $m = M('operator_line_supplier');
        if($m->create($data)){
            return $m->add();
        }
        return false;
    }

    /**
     * 修改供应商信息
     */
    public  function  editLineShops($data){
        $m = M('operator_line_supplier');
        if($m->save($data)){
            return true;
        }
        return false;
    }


    /**
     * 根据ID获取注册供应商信息
     */
    public function getLineShopsById($id){
        $supplierInfo=M('operator_line_supplier')->where(array('supplier_id'=>$id))->select();
        $supplierInfo=$supplierInfo[0];
        $supplierInfo['start_time'] = date('Y-m-d',$supplierInfo['start_time']);
        $supplierInfo['end_time'] = date('Y-m-d',$supplierInfo['end_time']);

        //获取已选中的省市区列表
        $provincePid=M('areas')->where(array('areaId'=>$supplierInfo['province_id']))->getField('parentId');
        $cityPid=M('areas')->where(array('areaId'=>$supplierInfo['city_id']))->getField('parentId');
        $areaPid=M('areas')->where(array('areaId'=>$supplierInfo['area_id']))->getField('parentId');

        $supplierInfo['province']=M('areas')->where(array('parentId'=>$provincePid,'areaFlag'=>1))->select();
        $supplierInfo['city']=M('areas')->where(array('parentId'=>$cityPid,'areaFlag'=>1))->select();
        $supplierInfo['area']=M('areas')->where(array('parentId'=>$areaPid,'areaFlag'=>1))->select();

        return $supplierInfo;
    }

    /**
     * 分销商列表数据
     */
    public function resellerShopsInfo($where){
        $count      = M('operator_line_reseller')->where($where)->count();// 查询满足要求的总记录数
        $Page       = new \Think\Page($count,25);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show       = $Page->show();// 分页显示输出
        // 进行分页数据查询 注意limit方法的参数要使用Page类的属性
        $list=M('operator_line_reseller')->where($where)->order('create_time desc')->limit($Page->firstRow.','.$Page->listRows)->select();
        // 获取详细地址
        foreach ($list as $key => $info) {
            $province_name = M('areas')->where(array('areaId'=>$info['province_id']))->getField('areaName');
            $city_name = M('areas')->where(array('areaId'=>$info['city_id']))->getField('areaName');
            $area_name = M('areas')->where(array('areaId'=>$info['area_id']))->getField('areaName');
            $list[$key]['complete_address'] = $province_name.$city_name.$area_name.$info['detailed_address'];
            if ($city_name==$province_name) {
                $list[$key]['complete_address'] = $province_name.$area_name.$info['detailed_address'];
            }
        }
        // dump(M('operator_line_reseller')->_sql());
        return ['list'=>$list,'show'=>$show];

    }

    /**
     * 添加分销商
     */
    public function addResellerShops($data){
        $admin_id=session('operator_user.pid')==0?session('operator_user.operator_id'):session('operator_user.pid');
        $data['operator_id']=$admin_id;

        $m = M('operator_line_reseller');

        if($m->create($data)){
            $rs = $m->add();
            if ($rs) {
                // 创建积分设置默认1:1
                $newData['reseller_id'] = $rs;
                $newData['amount'] = 1;
                $newData['score'] = 1;
                M('public_score_configs')->add($newData);
            }
            return $rs;
        }
        return false;

    }

    /**
     * 修改分销商资料
     */
    public  function  saveResellerShops($data){
        $m = M('operator_line_reseller');
        if($m->save($data)){
            return true;
        }
        return false;
    }


    /**
     * 开通与禁用分销商状态
     */
    public function changeresellerStatusById($data){
        return M('operator_line_reseller')->where(['reseller_id'=>$data['id']])->setField(['reseller_status'=>$data['reseller_status']]);
    }

    /**
     * 开通与禁用供应商状态
     */
    public function changeSupplierStatusById($data){
        return M('operator_line_supplier')->where(['supplier_id'=>$data['id']])->setField(['supplier_status'=>$data['supplier_status']]);
    }

    /**
     *  线路供应商列表 导出Excel
     */
    public  function lineShopsExportExcel($where){
        $list=M('operator_line_supplier')->where($where)->order('create_time desc')->select();
        // 获取详细地址
        foreach ($list as $key => $info) {
            $province_name = M('areas')->where(array('areaId'=>$info['province_id']))->getField('areaName');
            $city_name = M('areas')->where(array('areaId'=>$info['city_id']))->getField('areaName');
            $area_name = M('areas')->where(array('areaId'=>$info['area_id']))->getField('areaName');
            $list[$key]['complete_address'] = $province_name.$city_name.$area_name.$info['detailed_address'];
            if ($city_name==$province_name) {
                $list[$key]['complete_address'] = $province_name.$area_name.$info['detailed_address'];
            }
            $list[$key]['supplier_status']=$info['supplier_status']==1?'开通':'锁定';
            $list[$key]['is_review']=self::is_review($info['is_review']);
            $list[$key]['start_time']= date("Y-m-d H:i:s",$info['start_time']);
            $list[$key]['end_time']= date("Y-m-d H:i:s",$info['end_time']);
            $list[$key]['create_time']= date("Y-m-d H:i:s",$info['create_time']);
        }

        $expCellName  = array(
            array('supplier_name','供应商名称'),
            array('supplier_account','供应商账号'),
            array('supplier_sn','供应商编号'),
            array('supplier_phone','移动电话'),
            array('linkman','联系人'),
            array('complete_address','地址'),
            array('start_time','有效时间'),
            array('end_time','截止时间'),
            array('create_time','注册时间'),
            array('service_level','服务等级'),
            array('supplier_status','帐户状态'),
            array('is_review','审核状态'),
        );

        $fileName='注册供应商列表';
        parent::exportExcel($fileName,$expCellName,$list);
    }


    /**
     *  线路分销商 导出Excel
     */
    public  function lineResellerExportExcel($where){
        $list=M('operator_line_reseller')->where($where)->order('create_time desc')->select();
        // 获取详细地址
        foreach ($list as $key => $info) {
            $province_name = M('areas')->where(array('areaId'=>$info['province_id']))->getField('areaName');
            $city_name = M('areas')->where(array('areaId'=>$info['city_id']))->getField('areaName');
            $area_name = M('areas')->where(array('areaId'=>$info['area_id']))->getField('areaName');
            $list[$key]['complete_address'] = $province_name.$city_name.$area_name.$info['detailed_address'];
            if ($city_name==$province_name) {
                $list[$key]['complete_address'] = $province_name.$area_name.$info['detailed_address'];
            }
            $list[$key]['reseller_status']=$info['supplier_status']==1?'开通':'锁定';
            $list[$key]['is_review']=self::is_review($info['is_review']);
            $list[$key]['start_time']= date("Y-m-d H:i:s",$info['start_time']);
            $list[$key]['end_time']= date("Y-m-d H:i:s",$info['end_time']);
            $list[$key]['create_time']= date("Y-m-d H:i:s",$info['create_time']);
        }

        $expCellName  = array(
            array('reseller_name','供应商名称'),
            array('reseller_account','供应商账号'),
            array('reseller_sn','供应商编号'),
            array('reseller_phone','移动电话'),
            array('linkman','联系人'),
            array('complete_address','地址'),
            array('start_time','有效时间'),
            array('end_time','截止时间'),
            array('create_time','注册时间'),
            array('service_level','服务等级'),
            array('supplier_status','帐户状态'),
            array('is_review','审核状态'),
        );

        $fileName='分销商列表';
        parent::exportExcel($fileName,$expCellName,$list);
    }


    //获取分销商详情信息
    public  function  getResellerShopsById($id){

        $supplierInfo=M('operator_line_reseller')->where(array('reseller_id'=>$id))->find();
        $time = time();
        $supplierInfo['start_time'] = $supplierInfo['start_time'] ? date('Y-m-d',$supplierInfo['start_time'] ) : date('Y-m-d',$time );
        $supplierInfo['end_time'] = $supplierInfo['end_time'] ? date('Y-m-d',$supplierInfo['end_time']) : date('Y-m-d',$time+86400*365);


        //获取已选中的省市区列表

        $supplierInfo['province']=M('areas')->where(array('parentId'=>820303,'areaFlag'=>1))->select();

        $provincePid=M('areas')->where(array('areaId'=>$supplierInfo['province_id']))->getField('parentId');
        $cityPid=M('areas')->where(array('areaId'=>$supplierInfo['city_id']))->getField('parentId');
        $areaPid=M('areas')->where(array('areaId'=>$supplierInfo['area_id']))->getField('parentId');
        if($provincePid){
            $supplierInfo['province']=M('areas')->where(array('parentId'=>$provincePid,'areaFlag'=>1))->select();
        }

        $supplierInfo['city']=M('areas')->where(array('parentId'=>$cityPid,'areaFlag'=>1))->select();
        $supplierInfo['area']=M('areas')->where(array('parentId'=>$areaPid,'areaFlag'=>1))->select();

        return $supplierInfo;
    }



}

