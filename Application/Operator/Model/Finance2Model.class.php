<?php
namespace Operator\Model;
/**
 * 财务
 */
class Finance2Model extends BaseModel{

    public function financeList($where){

        $count = M('public_group as g')
            ->join("__PUBLIC_LINE__ as l on g.line_id=l.line_id")
            ->where($where)
            ->count();
        // 分页
        $Page  = new \Think\Page($count,25);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show  = $Page->show();// 分页显示输出
        // 查询
        $list  = M('public_group as g')
            ->join("LEFT JOIN __PUBLIC_LINE__ as l on g.line_id=l.line_id")
            ->join('LEFT JOIN __OPERATOR__ as o on o.operator_id=g.admin2_id')
            ->where($where)
            ->order('g.group_id desc')
            ->field('g.*,o.operator_name as admin2_name')
            ->limit($Page->firstRow.','.$Page->listRows)
            ->select();
        return ['list'=>$list,'show'=>$show];

    }



    //点击出团的时候,成本
    public  function costHandle($group_id){

        //线路成本
        $groupInfo=M('public_group')->where(array('group_id'=>$group_id))->find();
        $nowLineInfo=M('public_line')->where(array('line_id'=>$groupInfo['line_id']))->find();
        $originLineInfo=M('public_line')->where(array('line_id'=>$nowLineInfo['origin_line_id']))->find();

        $additionDB=M('order_addition as oa');
        $orderUnitDB=M('order_unit_detail');
        $orderCostDB=M('order_cost_detail');

        //所有订单 -6 取消，-5 已退款 ，-4 拒绝退款， -3 退款审核中，-2 审核不通过，-1 未支付，1 待审核(预订成功)(已支付)，2 审核中，3 已审核(已结算)
        $allOrders=M('line_orders')
            ->where(array('group_id'=>$group_id,'order_status'=>array('egt',-1)))
            ->select();



        //团号线路总成本
        $closing_total_money=0;
        //如果是供应商提供的线路,有供应商线路成本(线路打包价格,附加产品,保险酒店交通升级费用)

        if($originLineInfo){
            //线路成本
            $line_cost=0;
            $addition_cost_money=0;
            $userCount=0;
            $adult_num=0;
            $child_num=0;
            $old_num=0;

            //所有订单ID
            $all_order_id=[];

            //供应商优惠价
            $discounts=0;

            //供应商签证总价格
            $visa_cost=0;

            foreach ($allOrders as $k=>$v){

                //是供应商提供的才进行相加
                if($originLineInfo['visa_id']>0){
                    $visa_cost+=$v['visa_price']*$v['total_num'];
                }

                //供应商优惠
                if($v['preferential']>0&&$v['total_num']>=$originLineInfo['discounts_condition']&&$originLineInfo['discounts']>0){

                    $discounts+=$originLineInfo['discounts'];
                }

                $all_order_id[]=$v['order_id'];
                //线路基础成本
                $base_cost=0;
                //附加产品成本
                $addition_cost=0;
                //$base_cost=$v['adult_num']*$nowLineInfo['adult_money']+$v['child_num']*$nowLineInfo['child_money']+$v['oldMan_num']*$nowLineInfo['oldman_money'];
                //+$v['oldMan_num']*$nowLineInfo['oldman_money']

                //判断当天是否有特殊价格日期
                $out_date=date('Ymd',$v['out_time']);
                $special_price=M('operator_special_price')->where(array('line_id'=>$v['line_id'],'day'=>$out_date))->find();
                if($special_price){
                    $base_cost=$v['adult_num']*$special_price['origin_adult_price']+$v['child_num']*$special_price['origin_child_price'];
                }else{
                    $base_cost=$v['adult_num']*$nowLineInfo['adult_money']+$v['child_num']*$nowLineInfo['child_money'];
                }

                $adult_num+=$v['adult_num'];
                $child_num+=$v['child_num'];
                $old_num+=$v['oldMan_num'];
               // $userCount+=$v['adult_num']+$v['child_num']+$v['oldMan_num'];
                $userCount+=$v['adult_num']+$v['child_num'];


                //订单的附加产品
                $additionList=$additionDB
                    ->join("__LINE_ADDITIONAL_PRODUCT__ as ap on ap.product_id=oa.product_id")
                    ->where(array('oa.order_id'=>$v['order_id'],'ap.supplier_id'=>array('gt',0)))
                    ->field('origin_money,product_num')
                    ->select();
                if($additionList){
                    foreach ($additionList as $ak=>$av){
                        $addition_cost+=$av['origin_money']*$av['product_num'];
                    }
                }
                $addition_cost_money+=$addition_cost;


                $room_diff_cost=0;
                $upgrade_cost=0;
                //订单升级使用产品 is_compose=>0,供应商提供的产品
                $unitUpgrade=$orderUnitDB
                    ->where(array('order_id'=>$v['order_id'],'is_compose'=>0))
                    ->select();

                //
                $hotel=0;
                $trafic=0;
                $insure=0;

                if($unitUpgrade){
                    foreach ($unitUpgrade as $uk=>$uv){
                        //补房差价
                        if($uv['room_diff_cost']){
                            $room_diff_cost+=$uv['room_diff_cost'];
                        }
                        //升级的成本 1交通,2酒店,3景区,4保险
                        if($uv['is_upgrade']==1){
                            //酒店
                            if($uv['source_type']==2){
                                //已经算好总价
                                $upgrade_cost+=$uv['room_to_operator_price'];
                                //$upgrade_cost+=$uv['room_count']*$uv['room_to_operator_price'];
                                $hotel+=$uv['room_to_operator_price'];
                            }else{
                                //交通
                                if($uv['source_type']==1){
                                    //+$uv['upgrade_adult_price']*$uv['old_man_count']
                                    $upgrade_cost+=$uv['upgrade_adult_price']*$uv['adult_count']+$uv['upgrade_child_price']*$uv['child_count'];
                                    $trafic+=$uv['upgrade_adult_price']*$uv['adult_count']+$uv['upgrade_child_price']*$uv['child_count'];
                                }
                            }
                        }else{
                            //保险
                            if($uv['source_type']==4){
                                //+$uv['upgrade_adult_price']*$uv['old_man_count']
                                $upgrade_cost+=$uv['upgrade_adult_price']*$uv['adult_count']+$uv['upgrade_child_price']*$uv['child_count'];
                                $insure+=$uv['upgrade_adult_price']*$uv['adult_count']+$uv['upgrade_child_price']*$uv['child_count'];
                            }
                        }
                    }


                }
                $line_cost+=$base_cost+$addition_cost+$room_diff_cost+$upgrade_cost;
            }


            //供应商所有使用到的单品供应商的商品总价
            $supplier_unit_cost_total=M('order_unit_detail')->where(array('order_id'=>array('in',implode(',',$all_order_id))))->sum('unit_cost');


            //币种
            $currency_id=M('operator_line_supplier')->where(array('supplier_id'=>$originLineInfo['supplier_id']))->getField('currency_id');
            $currency=M('currency')->where(array('currency_id'=>$currency_id))->getField('currency_name');

            //成本入库
            $supplierData['group_num']=$groupInfo['group_num'];
            $supplierData['cost_type']=1;//成本类型:1供应商线路,2交通,3酒店,4保险,5景区
            $supplierData['num']=$userCount;//总人数
            $supplierData['money_type']=$currency;
            $supplierData['group_id']=$groupInfo['group_id'];
            $supplierData['cost_total_money']=$line_cost+$visa_cost-$discounts;//加上签证成本  且减去优惠价格
            $supplierData['addition_money']=$addition_cost_money;
            $supplierData['supplier_id']=$originLineInfo['supplier_id'];
            $supplierData['operator_id']=$nowLineInfo['operator_id'];

            //签证成本
            $supplierData['visa_cost_total']=$visa_cost;
            //供应商线路优惠
            $supplierData['discounts']=$discounts;

            $supplierData['adult_num']=$adult_num;
            $supplierData['child_num']=$child_num;
            $supplierData['old_num']=$old_num;
            $supplierData['line_id']=$groupInfo['line_id'];

            $supplierData['supplier_unit_cost_total']=$supplier_unit_cost_total;


            $orderCostDB->add($supplierData);

            //团号总成本->供应商部分
            $closing_total_money+=$line_cost+$visa_cost;


        }

            //运营商拼的或者自营的成本结算

            //只计算单品成本
            //1.交通供应商成本处理
            $trafficUnit=$orderUnitDB
                ->alias('u')
                ->join("LEFT JOIN __LINE_ORDERS__ as o on o.order_id=u.order_id")
                ->where(array('u.group_id'=>$group_id,'o.order_status'=>array('gt',0),'u.is_compose'=>1,'source_type'=>1))
                ->select();

            if($trafficUnit){
                //所有交通ID
                $trafficId=[];
                foreach ($trafficUnit as $k=>$v){
                    if(!in_array($v['source_id'],$trafficId)){
                        $trafficId[]=$v['source_id'];
                    }
                }

                //所有交通入库数组
                foreach ($trafficId as $v){
                    $trafficData=[];
                    $temp_cost=0;
                    $temp_adult_num=0;
                    $temp_child_num=0;
                    $temp_old_num=0;
                    foreach ($trafficUnit as $kk=>$vv){
                        if($v==$vv['source_id']){
                            $temp_adult_num+=$vv['adult_count'];
                            $temp_child_num+=$vv['child_count'];
                            $temp_old_num+=$vv['old_man_count'];
                            $temp_cost+=$vv['adult_price']*$vv['adult_count']+$vv['child_price']*$vv['child_count']+$vv['adult_price']*$vv['old_man_count'];
                        }
                    }

                    $unitInfo=M('public_single_traffic_price')->where(array('traffic_priceId'=>$v))->find();

                    $trafficData['group_num']=$groupInfo['group_num'];
                    $trafficData['cost_type']=2;//成本类型:1供应商线路,2交通,3酒店,4保险,5景区
                    $trafficData['num']=$temp_adult_num+$temp_child_num+$temp_old_num;//总人数
                    $trafficData['money_type']=$unitInfo['money_type'];
                    $trafficData['group_id']=$groupInfo['group_id'];
                    $trafficData['cost_total_money']=$temp_cost;
                    $trafficData['addition_money']=0;
                    $trafficData['supplier_id']=0;
                    $trafficData['operator_id']=$nowLineInfo['operator_id'];

                    $trafficData['unit_supplier_id']=$unitInfo['supplier_id'];
                    $trafficData['unit_product_id']=$v;

                    $trafficData['adult_num']=$temp_adult_num;
                    $trafficData['child_num']=$temp_child_num;
                    $trafficData['old_num']=$temp_old_num;
                    $trafficData['line_id']=$groupInfo['line_id'];

                    $orderCostDB->add($trafficData);

                    $closing_total_money+=$temp_cost;
                }
            }


            //2.酒店成本处理
            $hotelUnit=$orderUnitDB
                ->alias('u')
                ->join("LEFT JOIN __LINE_ORDERS__ as o on o.order_id=u.order_id")
                ->where(array('u.group_id'=>$group_id,'o.order_status'=>array('gt',0),'u.is_compose'=>1,'source_type'=>2))
                ->select();
            if($hotelUnit){
                //所有交通ID
                $hotelId=[];
                foreach ($hotelUnit as $k=>$v){
                    if(!in_array($v['source_id'],$hotelId)){
                        $hotelId[]=$v['source_id'];
                    }
                }
                //所有交通入库数组
                foreach ($hotelId as $v){
                    $hotelData=[];
                    $temp_cost=0;
                    $temp_adult_num=0;
                    $temp_child_num=0;
                    $temp_old_num=0;
                    $temp_root_num=0;
                    foreach ($hotelUnit as $kk=>$vv){
                        if($v==$vv['source_id']){
                            $temp_root_num+=$vv['room_count'];
                            $temp_adult_num+=$vv['adult_count'];
                            $temp_child_num+=$vv['child_count'];
                            $temp_old_num+=$vv['old_man_count'];
                            $temp_cost+=$vv['adult_price']*$vv['room_count'];
                        }
                    }

                    $unitInfo=M('public_single_room_price')->where(array('room_priceId'=>$v))->find();

                    $hotelData['group_num']=$groupInfo['group_num'];
                    $hotelData['cost_type']=3;//成本类型:1供应商线路,2交通,3酒店,4保险,5景区
                    $hotelData['num']=$temp_root_num;//总人数
                    $hotelData['money_type']=$unitInfo['money_type'];
                    $hotelData['group_id']=$groupInfo['group_id'];
                    $hotelData['cost_total_money']=$temp_cost;
                    $hotelData['addition_money']=0;
                    $hotelData['supplier_id']=0;
                    $hotelData['operator_id']=$nowLineInfo['operator_id'];

                    $hotelData['unit_supplier_id']=$unitInfo['supplier_id'];
                    $hotelData['unit_product_id']=$v;

                    $hotelData['adult_num']=$temp_adult_num;
                    $hotelData['child_num']=$temp_child_num;
                    $hotelData['old_num']=$temp_old_num;
                    $hotelData['line_id']=$groupInfo['line_id'];

                    $orderCostDB->add($hotelData);
                    $closing_total_money+=$temp_cost;
                }
            }



            //3.景区成本处理
            $scenicUnit=$orderUnitDB
                ->alias('u')
                ->join("LEFT JOIN __LINE_ORDERS__ as o on o.order_id=u.order_id")
                ->where(array('u.group_id'=>$group_id,'o.order_status'=>array('gt',0),'u.is_compose'=>1,'source_type'=>3))
                ->select();
            if($scenicUnit){
                //所有交通ID
                $scenicId=[];
                foreach ($scenicUnit as $k=>$v){
                    if(!in_array($v['source_id'],$scenicId)){
                        $scenicId[]=$v['source_id'];
                    }
                }

                foreach ($scenicId as $v){
                    $scenicData=[];
                    $temp_cost=0;
                    $temp_adult_num=0;
                    $temp_child_num=0;
                    $temp_old_num=0;
                    foreach ($scenicUnit as $kk=>$vv){
                        if($v==$vv['source_id']){
                            $temp_adult_num+=$vv['adult_count'];
                            $temp_child_num+=$vv['child_count'];
                            $temp_old_num+=$vv['old_man_count'];
                            $temp_cost+=$vv['adult_price']*$vv['adult_count']+$vv['child_price']*$vv['child_count']+$vv['adult_price']*$vv['old_man_count'];
                        }
                    }

                    $unitInfo=M('public_single_scenic_price')->where(array('scenic_priceId'=>$v))->find();

                    $scenicData['group_num']=$groupInfo['group_num'];
                    $scenicData['cost_type']=5;//成本类型:1供应商线路,2交通,3酒店,4保险,5景区
                    $scenicData['num']=$temp_adult_num+$temp_child_num+$temp_old_num;//总人数
                    $scenicData['money_type']=$unitInfo['money_type'];
                    $scenicData['group_id']=$groupInfo['group_id'];
                    $scenicData['cost_total_money']=$temp_cost;
                    $scenicData['addition_money']=0;
                    $scenicData['supplier_id']=0;
                    $scenicData['operator_id']=$nowLineInfo['operator_id'];

                    $scenicData['unit_supplier_id']=$unitInfo['supplier_id'];
                    $scenicData['unit_product_id']=$v;

                    $scenicData['adult_num']=$temp_adult_num;
                    $scenicData['child_num']=$temp_child_num;
                    $scenicData['old_num']=$temp_old_num;
                    $scenicData['line_id']=$groupInfo['line_id'];

                    $orderCostDB->add($scenicData);

                    $closing_total_money+=$temp_cost;
                }
            }



            //4.保险成本处理
            $insureUnit=$orderUnitDB
                ->alias('u')
                ->join("LEFT JOIN __LINE_ORDERS__ as o on o.order_id=u.order_id")
                ->where(array('u.group_id'=>$group_id,'o.order_status'=>array('gt',0),'u.is_compose'=>1,'source_type'=>4))
                ->select();

            if($insureUnit){
                //所有交通ID
                $insureId=[];
                foreach ($insureUnit as $k=>$v){
                    if(!in_array($v['source_id'],$insureId)){
                        $insureId[]=$v['source_id'];
                    }
                }

                foreach ($insureId as $v){
                    $insureData=[];
                    $temp_cost=0;
                    $temp_adult_num=0;
                    $temp_child_num=0;
                    $temp_old_num=0;
                    foreach ($insureUnit as $kk=>$vv){
                        if($v==$vv['source_id']){
                            $temp_adult_num+=$vv['adult_count'];
                            $temp_child_num+=$vv['child_count'];
                            $temp_old_num+=$vv['old_man_count'];
                            $temp_cost+=$vv['adult_price']*$vv['adult_count']+$vv['child_price']*$vv['child_count']+$vv['adult_price']*$vv['old_man_count'];
                        }
                    }

                    $unitInfo=M('public_single_insure_price')->where(array('insure_priceId'=>$v))->find();

                    $insureData['group_num']=$groupInfo['group_num'];
                    $insureData['cost_type']=4;//成本类型:1供应商线路,2交通,3酒店,4保险,5景区
                    $insureData['num']=$temp_adult_num+$temp_child_num+$temp_old_num;//总人数
                    $insureData['money_type']=$unitInfo['money_type'];
                    $insureData['group_id']=$groupInfo['group_id'];
                    $insureData['cost_total_money']=$temp_cost;
                    $insureData['addition_money']=0;
                    $insureData['supplier_id']=0;
                    $insureData['operator_id']=$nowLineInfo['operator_id'];

                    $insureData['unit_supplier_id']=$unitInfo['supplier_id'];
                    $insureData['unit_product_id']=$v;

                    $insureData['adult_num']=$temp_adult_num;
                    $insureData['child_num']=$temp_child_num;
                    $insureData['old_num']=$temp_old_num;
                    $insureData['line_id']=$groupInfo['line_id'];

                    $orderCostDB->add($insureData);
                    $closing_total_money+=$temp_cost;
                }
            }





        //更新团号的总结算金额
        //M('public_group')->where(array('group_id'=>$group_id))->save(array('closing_total_money'=>$closing_total_money));

        return true;

    }


    /**
     * 结算明细
     * @param $id 团id
     * @return array
     */
    public  function  groupCostDetail($id){
        $admin_id=session('operator_user.pid')==0?session('operator_user.operator_id'):session('operator_user.pid');

        $groupInfo=M('public_group')
            ->alias('g')
            ->field('g.group_num,g.line_name,g.group_time')
            ->where(array('group_id'=>$id))
            ->find();

       $list=M('order_cost_detail')->alias('cd')
           ->join("LEFT JOIN __PUBLIC_GROUP__ as p on p.group_id=cd.group_id")
           ->join("LEFT JOIN __OPERATOR__ as o on o.operator_id=cd.closing_operator_id")
           ->where(array('cd.group_id'=>$id,'cd.operator_id'=>$admin_id))
           ->field("cd.*,p.group_time,p.group_num,o.operator_name as closing_operator ")
           ->select();

       foreach ($list as $k=>$v){
           $list[$k]['group_time']=$v['group_time']?date('Y-m-d',$v['group_time']):'';
           $list[$k]['closing_time']=$v['closing_time']?date('Y-m-d',$v['closing_time']):'';
           //成本类型:1供应商线路,2交通,3酒店,4保险,5景区
           switch ($v['cost_type']){
               case 1:
                   $list[$k]['supplier_name']=M('operator_line_supplier')->where(array('supplier_id'=>$v['supplier_id']))->getField('supplier_name');
                   break;
               case 2:
                   //unit_product_id
                   $info=M('public_single_traffic_price as tp')
                       ->join("LEFT JOIN __PUBLIC_SINGLE_TRAFFIC__ st on st.traffic_id=tp.traffic_id")
                       ->join("LEFT JOIN __PUBLIC_SINGLE_TRAFFIC_SEAT__ sts on sts.seat_id=tp.seat_id")
                       ->join("LEFT JOIN __PUBLIC_SINGLE_SUPPLIER__ ss on ss.supplier_id=tp.supplier_id")
                       ->where(array('traffic_priceId'=>$v['unit_product_id']))
                       ->field('st.traffic_name,sts.seat_name,ss.supplier_name')
                       ->find();
                   $list[$k]['supplier_name']=$info['supplier_name'].$info['traffic_name'].$info['seat_name'];
                   break;
               case 3:
                   $info=M('public_single_room_price as rp')
                       ->join("LEFT JOIN __PUBLIC_SINGLE_ROOM__ as sr on sr.room_id=rp.room_id")
                       ->join("LEFT JOIN __PUBLIC_SINGLE_SUPPLIER__ ss on ss.supplier_id=rp.supplier_id")
                       ->where(array('room_priceId'=>$v['unit_product_id']))
                       ->field("sr.room_name,ss.supplier_name")
                       ->find();

                   $list[$k]['supplier_name']=$info['supplier_name'].$info['room_name'];
                   break;
               case 4:
                   $info=M('public_single_insure_price as ip')
                       ->join("LEFT JOIN __PUBLIC_SINGLE_INSURE__ as si on si.insure_id=ip.insure_id")
                       ->join("LEFT JOIN __PUBLIC_SINGLE_SUPPLIER__ ss on ss.supplier_id=ip.supplier_id")
                       ->where(array('insure_priceId'=>$v['unit_product_id']))
                       ->field("si.insure_name,ss.supplier_name")
                       ->find();
                   $list[$k]['supplier_name']=$info['supplier_name'].$info['insure_name'];
                   break;
               case 5:
                   $info=M('public_single_scenic_price as ip')
                       ->join("LEFT JOIN __PUBLIC_SINGLE_SCENIC__ as si on si.scenic_id=ip.scenic_id")
                       ->join("LEFT JOIN __PUBLIC_SINGLE_SUPPLIER__ ss on ss.supplier_id=ip.supplier_id")
                       ->where(array('scenic_priceId'=>$v['unit_product_id']))
                       ->field("si.scenic_name,ss.supplier_name")
                       ->find();
                   $list[$k]['supplier_name']=$info['supplier_name'].$info['scenic_name'];
                   break;
           }

           $list[$k]['cost_type_name']=self::costTypeName($v['cost_type'],$groupInfo);
           $list[$k]['closing_status_name']=self::closingStatusName($v['closing_status']);

       }



       return ['list'=>$list,'group'=>$groupInfo];

    }

    /**
     * 返回成本类型
     * @param $type 类型
     * @param $groupInfo 团信息
     * @return string 返回类型文字说明
     */
    private function costTypeName($type,$groupInfo){
        $typeName='';
        switch ($type){
            case 1 :$typeName=$groupInfo['line_name'];break;
            case 2 :$typeName='交通';break;
            case 3 :$typeName='酒店';break;
            case 4 :$typeName='保险';break;
            case 5 :$typeName='景区';break;
        }
        return $typeName;
    }

    /**
     * 返回结算状态
     * @param $status 状态
     * @return string 状态名称
     */
    private function closingStatusName($status){
        //-1待结算、1结算中、2已结算,3 确认完成结算
        $statusName='';
        switch ($status){
            case -1: $statusName='待结算';break;
            case 1: $statusName='结算中';break;
            case 2: $statusName='已结算';break;
            case 3: $statusName='确认完成结算';break;
        }
        return $statusName;
    }


    /**
     * 导出团成本列表
     * @param $id 团ID
     */
    public  function  exportGroupCostDetail($id){
        $admin_id=session('operator_user.pid')==0?session('operator_user.operator_id'):session('operator_user.pid');

        $groupInfo=M('public_group')
            ->alias('g')
            ->field('g.group_num,g.line_name,g.group_time')
            ->where(array('g.group_id'=>$id))
            ->find();

        $list=M('order_cost_detail')->alias('cd')
            ->join("LEFT JOIN __PUBLIC_GROUP__ as p on p.group_id=cd.group_id")
            ->join("LEFT JOIN __OPERATOR__ as o on o.operator_id=cd.closing_operator_id")
            ->where(array('cd.group_id'=>$id,'cd.operator_id'=>$admin_id))
            ->field("cd.*,p.group_time,p.group_num,o.operator_name as closing_operator ")
            ->select();

        foreach ($list as $k=>$v){
            $list[$k]['group_time']=$v['group_time']?date('Y-m-d',$v['group_time']):'';
            $list[$k]['closing_time']=$v['closing_time']?date('Y-m-d',$v['closing_time']):'';
            $list[$k]['closing_status_name']=self::closingStatusName($v['closing_status']);
            //成本类型:1供应商线路,2交通,3酒店,4保险,5景区
            switch ($v['cost_type']){
                case 1:
                    $list[$k]['supplier_name']=M('operator_line_supplier')->where(array('supplier_id'=>$v['supplier_id']))->getField('supplier_name');
                    break;
                case 2:
                    //unit_product_id
                    $info=M('public_single_traffic_price as tp')
                        ->join("LEFT JOIN __PUBLIC_SINGLE_TRAFFIC__ st on st.traffic_id=tp.traffic_id")
                        ->join("LEFT JOIN __PUBLIC_SINGLE_TRAFFIC_SEAT__ sts on sts.seat_id=tp.seat_id")
                        ->join("LEFT JOIN __PUBLIC_SINGLE_SUPPLIER__ ss on ss.supplier_id=tp.supplier_id")
                        ->where(array('traffic_priceId'=>$v['unit_product_id']))
                        ->field('st.traffic_name,sts.seat_name,ss.supplier_name')
                        ->find();
                    $list[$k]['supplier_name']=$info['supplier_name'].$info['traffic_name'].$info['seat_name'];
                    break;
                case 3:
                    $info=M('public_single_room_price as rp')
                        ->join("LEFT JOIN __PUBLIC_SINGLE_ROOM__ as sr on sr.room_id=ro.room_id")
                        ->join("LEFT JOIN __PUBLIC_SINGLE_SUPPLIER__ ss on ss.supplier_id=rp.supplier_id")
                        ->field("sr.room_name,ss.supplier_name")
                        ->find();
                    $list[$k]['supplier_name']=$info['supplier_name'].$info['room_name'];
                    break;
                case 4:
                    $info=M('public_single_insure_price as ip')
                        ->join("LEFT JOIN __PUBLIC_SINGLE_INSURE__ as si on si.insure_id=ip.insure_id")
                        ->join("LEFT JOIN __PUBLIC_SINGLE_SUPPLIER__ ss on ss.supplier_id=ip.supplier_id")
                        ->field("si.insure_name,ss.supplier_name")
                        ->find();
                    $list[$k]['supplier_name']=$info['supplier_name'].$info['insure_name'];
                    break;
                case 5:
                    $info=M('public_single_scenic_price as ip')
                        ->join("LEFT JOIN __PUBLIC_SINGLE_SCENIC__ as si on si.scenic_id=ip.scenic_id")
                        ->join("LEFT JOIN __PUBLIC_SINGLE_SUPPLIER__ ss on ss.supplier_id=ip.supplier_id")
                        ->field("si.scenic_name,ss.supplier_name")
                        ->find();
                    $list[$k]['supplier_name']=$info['supplier_name'].$info['scenic_name'];
                    break;
            }
            $list[$k]['cost_type_name']=self::costTypeName($v['cost_type'],$groupInfo);
        }


        $expCellName  = array(
            array('supplier_name','供应商'),
            array('group_num','团号'),
            array('cost_type_name','项目'),
            array('num','数量'),
            array('cost_total_money','结算金额'),
            array('money_type','币种'),
            array('exchange_rate','汇率'),
            array('actually_money','实付人民币'),
            array('group_time','成团日期'),
            array('closing_status_name','结算状态'),
            array('closing_time','结算时间'),
            array('closing_operator','操作人'),
        );

        $fileName=$groupInfo['group_num'].'结算明细';
        parent::exportExcel($fileName,$expCellName,$list);

    }

    /**
     * 返回某个成本详情
     * @param $id 成本ID
     * @return mixed
     */
    public  function getCostDetailById($id){
        $info=M('order_cost_detail')->where(array('id'=>$id))->find();
        return $info;
    }


    public  function  closingStatus($data){
        if(!$data['id']){
            return ['status'=>-1,'msg'=>'非法数据请求!'];
        }
        if(!$data['actually_money']){
            return ['status'=>-1,'msg'=>'请填写实付人民币!'];
        }
        if(!$data['exchange_rate']){
            return ['status'=>-1,'msg'=>'请填写汇率!'];
        }

        $info=M('order_cost_detail')->where(array('id'=>$data['id']))->find();

        if($info['closing_status']>=2){
            return ['status'=>0,'msg'=>'已经结算过!'];
        }
        //更新单项结算状态
        $save['actually_money']=$data['actually_money'];
        $save['exchange_rate']=$data['exchange_rate'];
        $save['closing_time']=time();
        $save['closing_status']=2;
        $save['closing_time']=time();
        $save['closing_operator_id']=session('operator_user.operator_id');
        $res=M('order_cost_detail')->where(array('id'=>$data['id']))->save($save);

        if($res){
            //全部已经结算,计算出总结算金额,并更新团状态
            $isClosing=M('order_cost_detail')->where(array('actually_money'=>array('elt',0),'group_id'=>$info['group_id']))->select();
            if(!$isClosing){
                //所有全部结算完成
                $closing_total_money=M('order_cost_detail')->where(array('actually_money'=>array('gt',0),'group_id'=>$info['group_id']))->sum('actually_money');
                $groupSave['closing_status']=2;
                $groupSave['admin2_id']=session('operator_user.operator_id');
                $groupSave['closing_time']=time();
                $groupSave['closing_total_money']=$closing_total_money;
                M('public_group')->where(array('group_id'=>$info['group_id']))->save($groupSave);
            }
            return ['status'=>1,'msg'=>'结算操作成功!'];
        }
        return ['status'=>0,'msg'=>'操作失败!'];


    }


    public  function  groupOrderDetail($where){

        $count = M('line_orders as o')
            ->where($where)
            ->count();
        // 分页
        $Page  = new \Think\Page($count,25);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show  = $Page->show();// 分页显示输出
        // 查询
        $list  = M('line_orders as o')
            ->join("LEFT JOIN __PUBLIC_LINE__ as l on o.line_id=l.line_id")
            ->join("LEFT JOIN __PUBLIC_TOURISTS__ as t on o.contact_id=t.tourists_id")
            ->where($where)
            ->order('o.order_id desc')
            ->field('o.end_need_pay,o.order_id,o.order_num,o.create_time,o.need_pay,o.sales_id,o.order_type,l.line_name,o.total_num,t.tourists_name,t.tourists_phone')
            ->limit($Page->firstRow.','.$Page->listRows)
            ->select();


        $operatorDB=M('operator');
        $sellerDB=M('operator_line_reseller');

        //销售员
        foreach ($list as $k=>$v){
            $list[$k]['sale_name']=$sellerDB->where(array('reseller_id'=>$v['sales_id']))->getField('reseller_name');
        }

        return ['list'=>$list,'show'=>$show];


    }

}

