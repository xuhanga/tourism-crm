<?php
namespace Operator\Model;
/**
 * ModelName
 */
class ProductModel extends BaseModel{

    protected $tableName = 'public_line';

    /**
     * 线路列表
     */
     public  function  lineList($where){

         $count      = $this->alias('l')
             ->join('LEFT JOIN __AREAS__ as a on a.areaId=l.origin_id')
             ->join('LEFT JOIN __OPERATOR__ as s on l.verifier_id=s.operator_id')
             ->join('LEFT JOIN __OPERATOR_LINE_SUPPLIER__ as su on su.supplier_id=l.supplier_id')
             ->where($where)->count();// 查询满足要求的总记录数
         $Page       = new \Think\Page($count,25);// 实例化分页类 传入总记录数和每页显示的记录数(25)
         $show       = $Page->show();// 分页显示输出

         // 进行分页数据查询 注意limit方法的参数要使用Page类的属性
         $list=M('public_line as l')
             ->join('LEFT JOIN __AREAS__ as a on a.areaId=l.origin_id')
             ->join('LEFT JOIN __OPERATOR__ as s on l.verifier_id=s.operator_id')
             ->join('LEFT JOIN __OPERATOR_LINE_SUPPLIER__ as su on su.supplier_id=l.supplier_id')
             ->field('l.travel_days,l.source_type,l.group_num,l.till_day,l.adult_money,l.child_money,l.oldman_money,l.line_id,l.line_name,l.origin_id,l.line_sn,l.create_time,a.areaName,l.line_status,s.operator_name,su.supplier_name')
             ->order('l.line_id desc')
             ->where($where)
             ->limit($Page->firstRow.','.$Page->listRows)
             ->select();


         return ['list'=>$list,'show'=>$show];
     }






     /**
      * 待审核线路总数
      */
     public  function  waitAudit($adminId){
         $count      = M('public_line')
             ->where(['operator_id'=>$adminId,'line_status'=>0])->count();// 查询满足要求的总记录数
         return $count;
     }

     /**
      * 线路详情
      */
     public function  getLineDetail($where){

         $admin_id=session('operator_user.pid')==0?session('operator_user.operator_id'):session('operator_user.pid');
         $field="a.areaName,s.supplier_name,l.*";
         $info=M('public_line as l')
             ->join('__AREAS__ as a on a.areaId=l.origin_id')
             ->join('LEFT JOIN __OPERATOR_LINE_SUPPLIER__ as s on s.supplier_id=l.supplier_id')
             ->field($field)
             ->where($where)
             ->find();

         //目的城市
         $destination=M('areas')->where(array('areaId'=>array('in',$info['destination_id'])))->select();
         $destinationCity=[];
         foreach ($destination as $k=>$v){
             $destinationCity[]=$v['areaName'];
         }
         $info['destinationCity']=implode(',',$destinationCity);


         //线路主题
         $theme=M('line_theme')->where(['theme_id'=>['in',$info['theme_id']]])->select();
         $themeName=[];
         foreach ($theme as $k=>$v){
             $themeName[]=$v['theme_name'];
         }
         $info['theme_name']='元';
         if($theme){
             $info['theme_name']=implode(',',$themeName);
         }



         //给运营商的特殊日期价格
         $info['specialPrice']=M('operator_special_price')->where(array('line_id'=>$info['line_id']))->select();

         $db_prefix=C('DB_PREFIX');
         //行程安排
         $trip=M('public_trip as t')
             ->where(['t.line_id'=>$where['l.line_id']])
             ->field("t.* ")
             ->select();

         $model=M();
         foreach ($trip as $k=>$v){
             //当天景区
             if($v['scenic_spot']){
                 $sql="select CONCAT(s.scenic_name) as scenic_name  from {$db_prefix}public_single_scenic_price as p,{$db_prefix}public_single_scenic as s  where s.scenic_id=p.scenic_id and p.scenic_priceId in ({$v['scenic_spot']})";
                 $scenicName='';
                 $scenic=$model->query($sql);
                 foreach ($scenic as $vv){
                     $scenicName.=$vv['scenic_name'].',';
                 }
                 $trip[$k]['scenic_name']=trim($scenicName,',');
             }
             //当天酒店
             if($v['lodging']){
                 $sql="select CONCAT_WS('-',su.supplier_name,sr.room_name) as hotel_name from {$db_prefix}public_single_room_price as rp
                 left join {$db_prefix}public_single_room as sr on sr.room_id=rp.room_id
                 left join {$db_prefix}public_single_supplier as su on su.supplier_id=rp.supplier_id where rp.room_priceId in ({$v['lodging']})";
                 $hotelName='';
                 $hotel=$model->query($sql);
                 foreach ($hotel as $vv){
                     $hotelName.=$vv['hotel_name'].',';
                 }
                 $trip[$k]['hotel_name']=trim($hotelName,',');
             }
             //当天可升级酒店
             if($v['lodging']){
                 $sql="select CONCAT_WS('-',su1.supplier_name,sr1.room_name) as up_hotel_name from {$db_prefix}public_single_room_price as rp1
                left join {$db_prefix}public_single_room as sr1 on sr1.room_id=rp1.room_id
                left join {$db_prefix}public_single_supplier as su1 on su1.supplier_id=rp1.supplier_id where rp1.room_priceId in ( select room_price_id from {$db_prefix}line_upgrade_hotel where {$db_prefix}line_upgrade_hotel.up_hotel_id in( {$v['upgrade_lodging']}))
                  ";
                 $up_hotel_name='';
                 $up_hotel=$model->query($sql);
                 foreach ($up_hotel as $vv){
                     $up_hotel_name.=$vv['up_hotel_name'].',';
                 }
                 $trip[$k]['up_hotel_name']=trim($up_hotel_name,',');
             }

         }
         foreach ($trip as $k=>$v){
             $trip[$k]['play']=M('public_play')->where(array('trip_id'=>$v['trip_id']))->select();
             $trip[$k]['shopping']=M('public_shopping')->where(array('trip_id'=>$v['trip_id']))->select();
         }
         $info['trip']=$trip;

         //签证信息
         $info['visa']='';
         $visa=M('public_visa')->where(array('visa_id'=>$info['visa_id']))->find();
         if($visa){
             $info['visa']=$visa['visa_name'].'-'.$visa['visa_price'].'元';
         }


         $info['addition']=M('line_additional_product')->where(array('line_id'=>$info['line_id']))->select();

         //已选的基础服务,如酒店,交通,景区,保险
         $goTraffic=M('public_single_traffic_price as tp')
             ->join('left join __PUBLIC_SINGLE_TRAFFIC_SEAT__ as se on se.seat_id=tp.seat_id')
             ->join('left join __PUBLIC_SINGLE_TRAFFIC__ as tr on tr.traffic_id=tp.traffic_id')
             ->join('left join __PUBLIC_SINGLE_SUPPLIER__ as pu on pu.supplier_id=tp.supplier_id')
             ->field('tp.traffic_priceId,pu.supplier_name,tp.routes_name,tr.traffic_name,tp.adult_actual_cost,tp.child_actual_cost,tp.seat_num,se.seat_name,tp.money_type')
             ->where(array('tp.traffic_priceId'=>$info['go_price_id']))
             ->find();
         $info['goTraffic']=$goTraffic;
         //已选的回来交通方式
         $backTraffic=M('public_single_traffic_price as tp')
             ->join('left join __PUBLIC_SINGLE_TRAFFIC_SEAT__ as se on se.seat_id=tp.seat_id')
             ->join('left join __PUBLIC_SINGLE_TRAFFIC__ as tr on tr.traffic_id=tp.traffic_id')
             ->join('left join __PUBLIC_SINGLE_SUPPLIER__ as pu on pu.supplier_id=tp.supplier_id')
             ->field('tp.traffic_priceId,pu.supplier_name,tp.routes_name,tr.traffic_name,tp.adult_actual_cost,tp.child_actual_cost,tp.seat_num,se.seat_name,tp.money_type')
             ->where(array('tp.traffic_priceId'=>$info['back_price_id']))
             ->find();
         $info['backTraffic']=$backTraffic;
         //已选的酒店
         $checkedHotel=M('public_single_room_price as rp')
             ->join('__PUBLIC_SINGLE_ROOM__ as sr on sr.room_id=rp.room_id')
             ->join('LEFT JOIN __PUBLIC_SINGLE_SUPPLIER__ as su on su.supplier_id=rp.supplier_id')
             ->field('sr.room_name,sr.star_level,rp.room_count,rp.room_priceId,rp.adult_actual_cost,rp.child_actual_cost,rp.money_type,su.supplier_name')
             ->where(array('rp.room_priceId'=>array('in',$info['room_price_id'])))
             ->select();
         $info['checkedHotel']=$checkedHotel;
         //已选景区
         $checkedScenic=M('public_single_scenic_price as rp')
             ->join('__PUBLIC_SINGLE_SCENIC__ as sr on sr.scenic_id=rp.scenic_id')
             ->join('LEFT JOIN __PUBLIC_SINGLE_SUPPLIER__ as su on su.supplier_id=rp.supplier_id')
             ->field('sr.scenic_name,sr.scenic_level,rp.seat_num,rp.scenic_priceId,rp.adult_actual_cost,rp.child_actual_cost,rp.money_type,su.supplier_name')
             ->where(array('rp.scenic_priceId'=>array('in',$info['scenic_price_id'])))
             ->select();
         $info['checkedScenic']=$checkedScenic;

         //已选保险
         $checkedInsure=M('line_upgrade_insure as li')
             ->join('LEFT JOIN __PUBLIC_SINGLE_INSURE_PRICE__ as rp on rp.insure_priceId=li.insure_price_id')
             ->join('LEFT JOIN __PUBLIC_SINGLE_INSURE__ as sr on sr.insure_id=rp.insure_id')
             ->join('LEFT JOIN __PUBLIC_SINGLE_SUPPLIER__ as su on su.supplier_id=rp.supplier_id')
             ->field('li.origin_adult_price,li.origin_child_price,li.now_adult_price,li.now_child_price,sr.insure_name,rp.seat_num,rp.insure_priceId,rp.adult_actual_cost,rp.child_actual_cost,rp.money_type,su.supplier_name,li.now_adult_price,li.now_child_price')
             ->where(array('li.line_id'=>$info['line_id']))
             ->select();

         $info['checkedInsure']=$checkedInsure;

         //已包含的升级服务
         $up_supplier_info=M('line_upgrade_hotel as lh')
             ->join('LEFT JOIN __PUBLIC_SINGLE_ROOM_PRICE__ as rp on rp.room_priceId=lh.room_price_id')
             ->join('LEFT JOIN __PUBLIC_SINGLE_SUPPLIER__ as su on rp.supplier_id=su.supplier_id')
             ->join('LEFT JOIN __PUBLIC_SINGLE_ROOM__ as sr on sr.room_id=rp.room_id')
             ->where(array('lh.line_id'=>$info['line_id']))
             ->field('lh.origin_adult_price,lh.origin_child_price,su.supplier_id,su.supplier_name,rp.money_type,rp.adult_actual_cost,rp.child_actual_cost,rp.star_level,rp.room_count,rp.user_count,sr.room_name,su.supplier_name,lh.to_reseller_price,lh.to_operator_price')
             ->select();

         $info['upgradeHotel']=$up_supplier_info;




         //可选和交通升级列表
         //$upgradeTrafficList=M('line_upgrade_traffic')->where(array('line_id'=>$info['line_id']))->select();
         $upgradeTrafficList=M('line_upgrade_traffic as u')
             ->field('u.*,l.back_traffic_custom,l.go_traffic_custom')
             ->join('__PUBLIC_LINE__ as l on l.line_id=u.line_id')
             ->where(array('u.line_id'=>$info['line_id']))
             ->select();
         $info['upgradeTrafficList']=$upgradeTrafficList;

         //销售价格列表
         $priceList=M('public_price')->where(array('line_id'=>$info['line_id']))->select();
         $info['priceList']=$priceList;

         return $info;
     }


     /**
      * 线路下架
      */
     public  function  soldOut($id){
         $res=$this->where(['line_id'=>$id])->setField(['line_status'=>-1]);
         if($res!==false){
             return true;
         }
         return false;
     }

     /**
      * 线路表信息
      */
     public  function  lineSimpleInfo($id){
         return $this->where(['line_id'=>$id])->find();
     }


     /**
      * 开通此供应商的运营商
      */
     public  function  supplierParentOperator($where){

         $admin_id=session('operator_user.pid')==0?session('operator_user.operator_id'):session('operator_user.pid');
         $admin_operator_id=M('operator_line_supplier')->where(array('supplier_id'=>$admin_id))->getField('operator_id');

         $lineId=I('id');

         if($where['operator_name']){
             $newWhere['operator_flag']=1;
             $newWhere['_string']=" operator_name  like '%{$where['operator_name']}%'  or operator_sn  like '%{$where['operator_name']}%'";
             $res=M('operator as o')
                 ->field('o.*,lo.partner_status')
                 ->join("LEFT JOIN __LINE_OPERATOR__ as lo on lo.operator_id=o.operator_id and  lo.line_id={$lineId}")
                 ->where($newWhere)->select();
         }else{
             $res=M('operator as o')
                 ->field('o.*,lo.partner_status')
                 ->join("LEFT JOIN __LINE_OPERATOR__ as lo on lo.operator_id=o.operator_id and  lo.line_id={$lineId}")
                 ->where(array('o.operator_id'=>$admin_operator_id))->select();
         }
         return $res;
     }

     /**
      * 申请合作
      */
     public  function  applyLinePartner($data){
        if(!is_numeric($data['id'])||!is_numeric($data['oid'])){
            return ['msg'=>'非法数据请求!','status'=>-1];
        }
        $exists=M('line_operator')->where(['operator_id'=>$data['oid'],'line_id'=>$data['id']])->find();
        if($exists){
            return ['msg'=>'已经申请过!请勿重复申请','status'=>-1];
        }

        $addData['operator_id']=$data['oid'];
        $addData['line_id']=$data['id'];
        $addData['create_time']=time();
        $addData['partner_status']=0;
        $res=M('line_operator')->add($addData);
        if($res){
            return ['msg'=>'申请成功!请耐心等待运营商处理','status'=>-1];
        }
         return ['msg'=>'操作失败!','status'=>-1];
     }


     /**
      * 合作中的运营商列表
      */
     public  function  partnerLineOperator($lineId){
         $count      = M('line_operator as l')
                        ->where(array('l.line_id'=>$lineId))->count();// 查询满足要求的总记录数
         $Page       = new \Think\Page($count,25);// 实例化分页类 传入总记录数和每页显示的记录数(25)
         $show       = $Page->show();// 分页显示输出

         // 进行分页数据查询 注意limit方法的参数要使用Page类的属性

         //所有合作线路列表
         $list=M('line_operator as l')
             ->join("__OPERATOR__ as o on l.operator_id=o.operator_id")
             ->where(array('l.line_id'=>$lineId))
             ->field('l.*,o.operator_name')
             ->limit($Page->firstRow.','.$Page->listRows)
             ->select();
         return ['list'=>$list,'show'=>$show];

     }

     /**
      * 团列表
      */
     public  function  groupList($where){
         $countList      = M('public_group as g')
             ->join("LEFT JOIN __LINE_ORDERS__ as o on o.group_id=g.group_id")
             ->join("LEFT JOIN __PUBLIC_LINE__ as pl on pl.line_id=g.line_id")
             ->join('LEFT JOIN __OPERATOR__ as op on op.operator_id=g.admin1_id')
             ->join('LEFT JOIN __AREAS__ as a on a.areaId=pl.origin_id')
             ->where($where)
             ->field('g.group_id')
             ->group('g.group_id')
             ->select();// 查询满足要求的总记录数

         $count=count($countList);
         $Page       = new \Think\Page($count,25);// 实例化分页类 传入总记录数和每页显示的记录数(25)
         $show       = $Page->show();// 分页显示输出

         // 进行分页数据查询 注意limit方法的参数要使用Page类的属性
         $field="g.*,pl.line_sn,op.operator_name,a.areaName";
         $list=M('public_group as g')
             ->join("LEFT JOIN __LINE_ORDERS__ as o on o.group_id=g.group_id")
             ->join("LEFT JOIN __PUBLIC_LINE__ as pl on pl.line_id=g.line_id")
             ->join('LEFT JOIN __OPERATOR__ as op on op.operator_id=g.admin1_id')
             ->join('LEFT JOIN __AREAS__ as a on a.areaId=pl.origin_id')
             ->field($field)
             ->where($where)
             ->group('g.group_id')
             ->order("g.group_id desc")
             ->limit($Page->firstRow.','.$Page->listRows)
             ->select();

         return ['list'=>$list,'show'=>$show];
     }

     /**
      * 导出团列表
      */
     public  function exportExcelGroupList($where){

         $field="g.*,pl.line_sn,op.operator_name,a.areaName";
         $list=M('public_group as g')
             ->join("LEFT JOIN __LINE_ORDERS__ as o on o.group_id=g.group_id")
             ->join("LEFT JOIN __PUBLIC_LINE__ as pl on pl.line_id=g.line_id")
             ->join('LEFT JOIN __OPERATOR__ as op on op.operator_id=g.admin1_id')
             ->join('LEFT JOIN __AREAS__ as a on a.areaId=pl.origin_id')
             ->field($field)
             ->group('g.line_id')
             ->where($where)
             ->select();

         foreach ($list as $k=>$v){
             $list[$k]['group_time']=$v['group_time']?date("Y-m-d",$v['group_time']):"";
             $list[$k]['out_time']=date("Y-m-d",$v['out_time']);
             $list[$k]['group_status']=self::groupStatus($v['group_status']);
         }



         $expCellName  = array(
             array('group_num','团号'),
             array('line_name','产品名称'),
             array('group_time','成团时间'),
             array('out_time','出团日期'),
             array('areaName','出发地点'),
             array('day_num','天数'),
             array('received_num','已收客人数'),
             array('expect_num','预计收客'),
             array('group_status','状态')
         );


         $admin_id=session('operator_user.pid')==0?session('operator_user.operator_id'):session('operator_user.pid');
         $operatorName=M('operator')->where(array('operator_id'=>$admin_id))->getField('operator_name');
         $fileName=$operatorName.'团列表';
         parent::exportExcel($fileName,$expCellName,$list);

     }
     /**
      * 成团状态
      */
     public  function groupStatus($status){
         $result='';
         switch ($status){
             case 0: $result='待处理';break;
             case -1: $result='不成团';break;
             case 1: $result='已成团';break;
             case 2: $result='待出团';break;
             case 3: $result='已回团';break;
         }
         return $result;
     }


     /**
      * 团号详情
      */
     public  function  groupDetail($group_id){
            $groupInfo=M('public_group')->where(['group_id'=>$group_id])->find();
            //线路出发地
            $lineInfo=M('public_line')->where(['line_id'=>$groupInfo['line_id']])->find();
            $groupInfo['startAddr']=M('areas')->where(['areaId'=>$lineInfo['origin_id']])->getField('areaName');

             //销售人
             $groupInfo['shop_name']=M('operator_line_reseller')->where(['reseller_id'=>$groupInfo['shop_id']])->getField('reseller_name');

            $where['o.group_id']=$group_id;

            //已审核的订单才显示
            //$where['o.order_status']=3;

            $count=M('line_orders as o')
             ->where($where)
             ->count();

            $Page       = new \Think\Page($count,25);// 实例化分页类 传入总记录数和每页显示的记录数(25)
            $show       = $Page->show();// 分页显示输出


            $field="o.order_status,o.need_pay,o.create_time,o.pay_time,o.sales_id,o.order_type,o.order_id,o.order_num,t.tourists_name,t.tourists_phone,l.line_name,l.line_sn,l.travel_days,o.adult_num,o.child_num,o.oldMan_num,o.order_status";
            $list=M('line_orders as o')
                ->join("LEFT JOIN __PUBLIC_TOURISTS__ as t on t.tourists_id=o.contact_id")
                ->join(" LEFT JOIN __PUBLIC_LINE__ as l on l.line_id=o.line_id")
                ->field($field)
                ->where($where)
                ->limit($Page->firstRow.','.$Page->listRows)
                ->select();


            foreach ($list as $k=>$v){
                $list[$k]['order_status']=self::orderStatus($v['order_status']);
                //结算金额 直营门店1、分销商2
                $list[$k]['saleMan']=M('operator_line_reseller')->where(array('reseller_id'=>$v['sales_id']))->getField('reseller_name');
            }




         return ['list'=>$list,'show'=>$show,'group'=>$groupInfo];
     }


    /**
     * 订单状态
     */
    public  function orderStatus($status){
        $result='';
        switch ($status){
            case -1: $result='未支付';break;
            case -2: $result='审核不通过';break;
            case -3: $result='退款审核中';break;
            case -4: $result='拒绝退款';break;
            case -5: $result='订单已退款';break;
            case -6: $result='订单取消';break;
            case 1: $result='待审核';break;
            case 2: $result='审核中';break;
            case 3: $result='已审核';break;
        }
        return $result;
    }



     /**
      * 导出团详情
      */
     public  function  exportExcelGroupDetail($group_id){
         $groupInfo=M('public_group')->where(['group_id'=>$group_id])->find();

         //线路出发地
         $lineInfo=M('public_line')->where(['line_id'=>$groupInfo['line_id']])->find();

         $where['o.group_id']=$group_id;

         $field="o.order_status,o.need_pay,o.create_time,o.pay_time,o.create_time,o.sales_id,o.order_type,o.order_id,o.order_num,t.tourists_name,t.tourists_phone,l.line_name,l.line_sn,l.travel_days,o.adult_num,o.child_num,o.oldMan_num,o.order_status";
         $list=M('line_orders as o')
             ->join("LEFT JOIN __PUBLIC_TOURISTS__ as t on t.tourists_id=o.contact_id")
             ->join("LEFT JOIN __PUBLIC_LINE__ as l on l.line_id=o.line_id")
             ->field($field)
             ->where($where)
             ->select();

         foreach ($list as $k=>$v){
             $list[$k]['order_status']=self::orderStatus($v['order_status']);
             //结算金额 直营门店1、分销商2
             $list[$k]['saleMan']=M('operator_line_reseller')->where(array('reseller_id'=>$v['sales_id']))->getField('reseller_name');
             $list[$k]['adult_num']=$v['adult_num']+$v['child_num']+$v['oldMan_num'];
             $list[$k]['create_time']='';
             if($v['create_time']){
                 $list[$k]['create_time']=date('Y-m-d H:i:s',$v['create_time']);
             }

         }
         $expCellName  = array(
             array('order_num','订单号'),
             array('line_name','线路名称'),
             array('tourists_name','游客姓名'),
             array('tourists_phone','联系方式'),
             array('adult_num','人数'),
             array('need_pay','实收金额'),
             array('create_time','下单时间'),
             array('order_status','订单状态'),
             array('saleMan','销售人'),

         );
         $fileName=$groupInfo['group_num'].'团订单列表';
         parent::exportExcel($fileName,$expCellName,$list);
     }

/**
 * 审核线路
 */
public  function  auditLine(){
    $id=I('id',0,'intval');
    $status=I('status',0,'intval');

    if(!$id||!$status){
        return false;
    }

    $admin_id=session('operator_user.pid')==0?session('operator_user.operator_id'):session('operator_user.pid');
    $data['line_status']=$status;
    $data['verifier_id']=$admin_id;
    $res=M('public_line')->where(array('line_id'=>$id))->save($data);

    if($res){
        return true;
    }
    return false;
}

/**
 * 运营商有多少待审核的线路
 */
public function  cooperationCount($admin_id){
    $count= M('line_operator')->where(array('operator_id'=>$admin_id,'partner_status'=>0))->group('p.line_id')->count();
    return $count;
}

    /**
     * 供应商申请与运营商合作的线路列表
     */
    public function  cooperationList($where){

        $count=M('line_operator as p')
            ->join('LEFT JOIN __PUBLIC_LINE__ as l on l.operator_id=p.operator_id')
            ->group('l.line_id')
            ->where($where)->count();// 查询满足要求的总记录数
        $Page       = new \Think\Page($count,25);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show       = $Page->show();// 分页显示输出
        // 进行分页数据查询 注意limit方法的参数要使用Page类的属性
        $list=M('line_operator as p')
            ->join('LEFT JOIN __PUBLIC_LINE__ as l on l.line_id=p.line_id')
            ->join('LEFT JOIN __PUBLIC_LINE__ as ll on ll.line_id=p.new_line_id')
            ->join('LEFT JOIN __AREAS__ as a on a.areaId=l.origin_id')
            ->join('LEFT JOIN __OPERATOR_LINE_SUPPLIER__ as su on su.supplier_id=l.supplier_id')
            ->field('p.line_id,p.new_line_id,ll.line_status,p.partner_id,l.supplier_id,l.group_num,l.till_day,l.travel_days,l.adult_money,l.child_money,l.oldman_money,l.line_id,l.line_name,l.origin_id,l.line_sn,l.create_time,a.areaName,su.supplier_name,p.partner_status,p.create_time')
            ->order('p.partner_id desc')
            ->where($where)
            ->group('l.line_id')
            ->limit($Page->firstRow.','.$Page->listRows)
            ->select();
        return ['list'=>$list,'show'=>$show];
    }




    /**
     * 同意线路合作,拷贝一份到线路信息
     */
    public  function copyLineToOperator($data){

        $admin_id=session('operator_user.pid')==0?session('operator_user.operator_id'):session('operator_user.pid');
        $line_id=M('line_operator')->where(array('partner_id'=>$data['id']))->getField('line_id');
        $originLineInfo=M('public_line')->where(array('line_id'=>$line_id))->find();

        $originSupplierInfo=M('operator_line_supplier')->where(array('supplier_id'=>$originLineInfo['supplier_id']))->find();
        $currency=S('currency');

        $adminOperatorInfo=M('operator')->where(array('operator_id'=>$admin_id))->find();

        $originMoneyType=$currency[$originSupplierInfo['currency_id']];
        $nowMoneyType=$currency[$adminOperatorInfo['currency_id']];


        //第一步 线路拷贝
        $copyLineData=$originLineInfo;
        $copyLineData['add_user_id']=session('operator_user.operator_id');
        $copyLineData['operator_id']=$admin_id;
        $copyLineData['line_status']=-3;
        $copyLineData['origin_line_id']=$line_id;
        $copyLineData['platform']=2;
        $copyLineData['source_type']=2;
        //判断供应商有没有提供产品
        $copyLineData['room_custom']=$originLineInfo['room_price_id']>0?0:1;
        $copyLineData['go_traffic_custom']=$originLineInfo['go_price_id']>0?0:1;
        $copyLineData['back_traffic_custom']=$originLineInfo['back_price_id']>0?0:1;
        $copyLineData['scenic_custom']=$originLineInfo['scenic_price_id']>0?0:1;
        $copyLineData['insure_custom']=$originLineInfo['insure_price_id']>0?0:1;
        unset($copyLineData['line_id']);

;
        $new_line_id=M('public_line')->add($copyLineData);

        //可升级信息处理

        //2.保险给运营商的价格
        $origin_up_insure=M('line_upgrade_insure')->where(array('line_id'=>$line_id))->select();
        $new_up_insure_data=[];
        foreach ($origin_up_insure as $k=>$v){
            unset($v['up_insure_id']);
            $v['now_money_type']=$v['origin_money_type'];
            $v['origin_adult_price']=$v['now_adult_price'];
            $v['origin_child_price']=$v['now_child_price'];
            $v['line_id']=$new_line_id;
            $new_up_insure_data[]=$v;
        }
        if(count($new_up_insure_data)>0){
            M('line_upgrade_insure')->addAll($new_up_insure_data);
        }

        //3.来回交通供应商升级价格
        $origin_up_traffic=M('line_upgrade_traffic')->where(array('line_id'=>$line_id))->select();
        $new_up_traffic_data=[];
        foreach ($origin_up_traffic as $k=>$v){
            unset($v['up_traffic_id']);
            $v['now_money_type']=$v['origin_money_type'];
            $v['origin_adult_price']=$v['now_adult_price'];
            $v['origin_child_price']=$v['now_child_price'];
            $v['line_id']=$new_line_id;
            $new_up_traffic_data[]=$v;
        }
        if(count($new_up_traffic_data)>0){
            M('line_upgrade_traffic')->addAll($new_up_traffic_data);
        }

        //第二步
        //2.1特殊价格日期
        $origin_special_price=M('operator_special_price')->where(array('line_id'=>$line_id))->select();
        $new_special_price_data=[];
        foreach ($origin_special_price as $k=>$v){
            unset($v['sprice_id']);
            $v['line_id']=$new_line_id;
            $v['origin_adult_price']=$v['adult_price'];
            $v['origin_child_price']=$v['child_price'];
            $v['origin_oldman_price']=$v['oldman_price'];
            $v['origin_money_type']=$originMoneyType;
            $new_special_price_data[]=$v;
        }
        if(count($new_special_price_data)>0){
            M('operator_special_price')->addAll($new_special_price_data);
        }

        $special_price_all= M('operator_special_price')->where(array('line_id'=>$new_line_id))->select();
        if($special_price_all){
            $special_price_id=[];
                foreach ($special_price_all as $k=>$v){
                    $special_price_id[]=$v['sprice_id'];
                }
            //更新特殊日期价格
            M('public_line')->where(array('line_id'=>$new_line_id))->setField(array('sprice_id'=>implode(',',$special_price_id)));
        }



        //第三步 行程处理
        //3.0行程处理
        $all_new_trip_id=[];

        $origin_trip=M('public_trip')->where(array('line_id'=>$line_id))->select();

        $line_upgrade_hotel_DB=M('line_upgrade_hotel');
        foreach ($origin_trip as $k=>$v){

            //这天行程可升级酒店
            $origin_up_hotel=$line_upgrade_hotel_DB->where(array('up_hotel_id'=>array('in',$v['upgrade_lodging'])))->select();

            $up_hotel_id=[];
            foreach ($origin_up_hotel as $upk=>$upv){
                //判断升级酒店表里面是否已经存在
                $upHotelExists=$line_upgrade_hotel_DB->where(array('line_id'=>$new_line_id,'room_price_id'=>$upv['room_price_id']))->find();
                if($upHotelExists){
                    $up_hotel_id[]=$upHotelExists['up_hotel_id'];
                }else{
                    unset($upv['up_hotel_id']);
                    $upv['now_adult_price']=$upv['to_operator_price'];
                    $upv['to_reseller_price']=$upv['to_operator_price'];
                    $upv['line_id']=$new_line_id;
                    $up_hotel_id[]= M('line_upgrade_hotel')->add($upv);
                }
            }

            $origin_trip_id=$v['trip_id'];
            unset($v['trip_id']);
            $v['line_id']=$new_line_id;
            $v['operator_id']=$admin_id;

            $new_trip_id=M('public_trip')->add($v);
            $all_new_trip_id[]=$new_trip_id;
            //3.1购物项目处理
            $origin_shopping_data=M('public_shopping')->where(array('trip_id'=>$origin_trip_id))->select();
            $new_shopping_data=[];
            foreach ($origin_shopping_data as $kk=>$vv){
                unset($vv['shopping_id']);
                $vv['line_id']=$new_line_id;
                $vv['operator_id']=$admin_id;
                $vv['trip_id']=$new_trip_id;
                $new_shopping_data[]=$vv;
            }
            if(count($new_shopping_data)>0){
                M('public_shopping')->addAll($new_shopping_data);
            }

            //3.2游玩项目
            $origin_play_data=M('public_play')->where(array('trip_id'=>$origin_trip_id))->select();
            $new_play_data=[];
            foreach ($origin_play_data as $kkk=>$vvv){
                unset($vvv['play_id']);
                $vvv['operator_id']=$admin_id;
                $vvv['line_id']=$new_line_id;
                $vvv['trip_id']=$new_trip_id;
                $new_play_data[]=$vvv;
            }
            if(count($new_play_data)>0){
                M('public_play')->addAll($new_play_data);
            }

            $trip_shopping_id=M('public_shopping')->where(array('trip_id'=>$new_trip_id))->field('shopping_id')->select();
            $trip_play_id=M('public_play')->where(array('trip_id'=>$new_trip_id))->field('play_id')->select();
            $play_id=[];
            $shopping_id=[];
            foreach ($trip_shopping_id as $sk=>$sv){
                $shopping_id[]=$sv['shopping_id'];
            }
            foreach ($trip_play_id as $pk=>$pv){
                $play_id[]=$pv['play_id'];
            }

            $saveShoppingPlay['shopping_id']=implode(',',$shopping_id);
            $saveShoppingPlay['play_id']=implode(',',$play_id);
            //当天可升级的酒店
            $saveShoppingPlay['upgrade_lodging']=implode(',',$up_hotel_id);

            M('public_trip')->where(array('trip_id'=>$new_trip_id))->save($saveShoppingPlay);

        }

        //第四步 签证

        //第五步附加产品处理
        $origin_additional_product=M('line_additional_product')->where(array('line_id'=>$line_id))->select();
        $new_additional_data=[];
        foreach ($origin_additional_product as $k=>$v){
            $v['origin_product_price']=$v['product_price'];
            unset($v['product_id']);
            $v['line_id']=$new_line_id;
            $v['origin_line_id']=$line_id;
            $v['origin_line_id']=$line_id;
            $v['operator_id']=$admin_id;
            $v['origin_product_name']=$v['product_name'];
            $v['origin_money_type']=$originMoneyType;
            $v['now_money_type']=$nowMoneyType;
            $new_additional_data[]=$v;
        }
        if(count($new_additional_data)>0){
            M('line_additional_product')->addAll($new_additional_data);
        }

        //把线路ID关联回来 line_operator 供应商线路-运营商合作表
        M('line_operator')->where(array('partner_id'=>$data['id']))->save(array('new_line_id'=>$new_line_id));



    }



    /**
     * 原线路信息
     */
    public  function  originLineInfo($id){
        $origin_line_id=M('public_line')->where(array('line_id'=>$id))->getField('origin_line_id');
        return M('public_line')->where(array('line_id'=>$origin_line_id))->find();
    }


    /**
     * 订单详情
     * @return [$data] [订单详情数组]
     */
    public function orderDetail($id){
        $data = [];
        //订单信息
        $data['order'] = M('line_orders l')
            ->field('l.*,o.line_name,g.group_num')
            ->join('LEFT JOIN __PUBLIC_LINE__ o on l.line_id = o.line_id')
            ->join('LEFT JOIN __PUBLIC_GROUP__ g on l.group_id = g.group_id')
            ->where(['l.order_id'=>$id])
            ->find();
        //毛利
        $data['order']['profit'] = $data['order']['end_need_pay'] - $data['order']['need_pay'] ;

        //发票信息
        $data['receipt'] = M('public_receipt')
            ->where(['order_id'=>$data['order']['order_id']])
            ->find();

        //游客信息
        $data['tourism'] = M('public_tourists')
            ->where(['order_id'=>$data['order']['order_id']])
            ->select();

        //订单联系人
        foreach ($data['tourism'] as  $v) {
            if($v['is_contact'] == 1){
                $data['contact']['name'] = $v['tourists_name'];
                $data['contact']['phone'] = $v['tourists_phone'];
                break;
            }
        }
        switch ($data['order']['order_status']) {
            case -6:
                $data['order']['order_status_content'] = '取消';
                break;

            case -5:
                $data['order']['order_status_content'] = '已退款';
                break;

            case -4:
                $data['order']['order_status_content'] = '拒绝退款';
                break;

            case -3:
                $data['order']['order_status_content'] = '退款审核中';
                break;

            case -2:
                $data['order']['order_status_content'] = '审核不通过';
                break;

            case -1:
                $data['order']['order_status_content'] = '未支付';
                break;

            case 1:
                $data['order']['order_status_content'] = '待审核';
                break;

            case 2:
                $data['order']['order_status_content'] = '审核中';
                break;
            case 3:
                $data['order']['order_status_content'] = '已审核';
                break;
        }

        return $data;
    }

    public  function changeGroupStatus($group_id,$status){
        $groupInfo=M('public_group')->where(array('group_id'=>$group_id))->find();
        //2已出团、1已成团、不成团-1、已回团3)默认0
        if($status==1){

            if($groupInfo['group_status']==$status){
                return ['status'=>-1,'msg'=>'请勿重复操作!'];
            }
            $saveData['group_time']=time();
            $saveData['group_status']=1;
            $saveData['admin1_id']=session('operator_user.operator_id');
            $res=M('public_group')->where(array('group_id'=>$group_id))->save($saveData);
            if($res){

                return ['status'=>1,'msg'=>'操作成功!'];
            }
            return ['status'=>0,'msg'=>'操作失败!'];
        }else if($status==2){

            //判断有没有订单没有审核的
            $isExistsNotPayOrders=M('line_orders')->where(array('group_id'=>$group_id,'order_status'=>array('in','1,2')))->count('order_id');
            if($isExistsNotPayOrders){
                return ['status'=>-1,'msg'=>'还有订单尚未审核,操作失败!'];
            }


            if($groupInfo['group_status']==$status){
                return ['status'=>-1,'msg'=>'请勿重复操作!'];
            }

            $saveData['group_status']=2;
            $res=M('public_group')->where(array('group_id'=>$group_id))->save($saveData);

            if($res){
                //出团的时候,算出成本
                D('Finance2')->costHandle($group_id);
                return ['status'=>1,'msg'=>'操作成功!'];
            }
            return ['status'=>0,'msg'=>'操作失败!'];
        }else if($status==3){
            if($groupInfo['group_status']==$status){
                return ['status'=>-1,'msg'=>'请勿重复操作!'];
            }
            $saveData['group_status']=3;
            $res=M('public_group')->where(array('group_id'=>$group_id))->save($saveData);
            if($res){
                return ['status'=>1,'msg'=>'操作成功!'];
            }
            return ['status'=>0,'msg'=>'操作失败!'];
        }else if($status==-1){
            if($groupInfo['group_status']==$status){
                return ['status'=>-1,'msg'=>'请勿重复操作!'];
            }
            $saveData['group_status']=-1;
            $res=M('public_group')->where(array('group_id'=>$group_id))->save($saveData);
            if($res){
                //运营商已审核的订单,进行退款处理 ,订单进入待退款列表里面
                $list=M('line_orders')->where(array('group_id'=>$group_id,'order_status'=>3))->field('end_need_pay,order_id,back_money')->select();
                $refundModel= D('Operator/SaleManger');
                foreach ($list as $k=>$v){
                    $refundModel->refuse($v['end_need_pay'],$v['order_id'],$v['end_need_pay']);
                }
                return ['status'=>1,'msg'=>'操作成功!'];
            }
            return ['status'=>0,'msg'=>'操作失败!'];
        }

    }



}

