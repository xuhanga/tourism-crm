<?php
namespace Operator\Model;
use Think\Model;
/**
 * 平台基本资料模型
 */
class CityModel extends Model{

    protected $tableName = 'areas';

    /**
     * 国家列表
     */
    public  function  countryList(){
        return $this->where(['areaFlag'=>1,'areaType'=>4])->order('areaSort asc')->select();
    }


    /**
     * 省列表
     */
    public  function  provinceList(){
        return $this->where(['areaFlag'=>1,'areaType'=>0])->order('areaSort asc')->select();
    }


    /**
     * 删除成功 ,传一个ID, 相应的下级也删除
     */
    public  function  delCity($areaId){
        return $this->where(['areaId'=>$areaId])->setField(['areaFlag'=>-1]);

    }

    /**
     * 添加国家
     */
    public  function  addCountry($data){
        if(!$data['areaName']){
            return ['status'=>-1,'msg'=>'名称不能为空!'];
        }

        //判断是否重复
        $exists=$this->where(['areaName'=>$data['areaName'],'areaFlag'=>1])->find();
        if($exists){
            return ['status'=>-2,'msg'=>'已经存在,请勿重复添加!'];
        }

        $addData['areaName']=$data['areaName'];
        $addData['areaSort']=intval($data['areaSort']);
        $addData['parentId']=0;
        $addData['areaType']=4;
        $res=$this->add($addData);

        if($res){
            return ['status'=>1,'msg'=>'添加成功!'];
        }
        return ['status'=>0,'msg'=>'添加失败!'];
    }


    /**
     * 编辑地区
     */
     public  function  editCity($data){
         $saveData['areaName']=$data['areaName'];
         $saveData['areaSort']=intval($data['areaSort']);
        return $this->where(['areaId'=>$data['areaId']])->save($saveData);
     }

    /**
     * 添加省市区
     */
    public  function  addCity($data){
        if(!$data['areaName']){
            return ['status'=>-1,'msg'=>'名称不能为空!'];
        }

        //判断是否重复
        $exists=$this->where(['areaName'=>$data['areaName'],'areaFlag'=>1])->find();
        if($exists){
            return ['status'=>-2,'msg'=>'已经存在,请勿重复添加!'];
        }

        $addData['areaName']=$data['areaName'];
        $addData['areaSort']=$data['areaSort'];
        $addData['parentId']=$data['parentId'];
        $addData['areaType']=$data['areaType'];
        $res=$this->add($addData);
        if($res){
            return ['status'=>1,'msg'=>'添加成功!'];
        }
        return ['status'=>0,'msg'=>'添加失败!'];
    }

    /**
     * 地区名称详情
     */
    public  function  getCityDetail($areaId){
        return $this->find($areaId);
    }


    /**
     * 获取地区列表,根据父ID
     */

    public  function  cityList($parentId){
        return $this->where(['parentId'=>$parentId,'areaFlag'=>1])->order('areaSort asc')->select();
    }

    /**
     * 显示NAV
     */
    public  function  bread($areaId){
        $areaName=[];
        $info=self::getCityDetail($areaId);
        $areaName[]=$info['areaName'];
        if($info['parentId']){
            $info=self::getCityDetail($info['parentId']);
            $areaName[]=$info['areaName'];
            if($info['parentId']){
                $info=self::getCityDetail($info['parentId']);
                $areaName[]=$info['areaName'];
                if($info['parentId']){
                    $info=self::getCityDetail($info['parentId']);
                    $areaName[]=$info['areaName'];
                }
            }
        }
        $areaName=array_reverse($areaName);
        $areaName=implode('>',$areaName);
        $areaName= trim($areaName,'>');
        return $areaName;
    }


    public  function  changeIsShow($areaId,$isShow){
        $res=$this->where(['areaId'=>$areaId])->setField(['isShow'=>$isShow]);
        if($res){
            return ['status'=>1,'msg'=>'操作成功!'];
        }
        return ['status'=>1,'msg'=>'操作失败!'];
    }

}
