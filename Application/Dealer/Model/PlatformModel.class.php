<?php
namespace Dealer\Model;
use Think\Model;
/**
 * 平台基本资料模型
 */
class PlatformModel extends Model{

    protected $tableName = 'operator_line_supplier';

    public  function  getPlatformInfo(){
         $uid=session('dealer_user.pid')==0?session('dealer_user.supplier_id'):session('dealer_user.pid');
         return $this->find($uid);
     }

    /**
     * 全部国家
     */
     public  function getCountry(){
        return M('areas')->where(['areaFlag'=>1,'isShow'=>1,'areaType'=>4])->select();
     }

     /**
     * 全部省份
     */
     public  function getProvince(){
        return M('areas')->where(['areaFlag'=>1,'isShow'=>1,'areaType'=>0])->select();
     }

    /**
     * 全部省
     */
    public  function getProvinceByCountryId($country_id){
        if(!$country_id){
            return array();
        }
        return M('areas')->where(['areaFlag'=>1,'isShow'=>1,'parentId'=>$country_id])->select();
    }


    /**
     * 全部市区
     */
     public  function getCity($provinceId){
         if(!$provinceId){
             return array();
         }
        return M('areas')->where(['areaFlag'=>1,'isShow'=>1,'parentId'=>$provinceId])->select();
     }

    /**
     * 全部市区
     */
    public  function getArea($cityId){
        if(!$cityId){
            return array();
        }
        return M('areas')->where(['areaFlag'=>1,'isShow'=>1,'parentId'=>$cityId])->select();
    }


    /**
     * 编辑基本资料
     */
    public  function  editUserInfo($data){
        $uid=session('dealer_user.pid')==0?session('dealer_user.supplier_id'):session('dealer_user.supplier_id');
        $res=$this->data($data)->where(['supplier_id'=>$uid])->save($data);
        return $res;
    }


    /**
     * 修改密码
     */
    public  function  editPassword($data){
        $uid=session('dealer_user.supplier_id');
        if($data['new_pwd']!=$data['com_pwd']){
            return ['status'=>-1,'msg'=>'两次密码不一致!'];
        }
        $userInfo=$this->find($uid);
        if($userInfo['loginPwd']!=md5($data['old_pwd'].$userInfo['loginSecret'])){
            return ['status'=>-2,'msg'=>'旧密码错误!'];
        }
        $saveData['loginSecret']=mt_rand(1000,9999);
        $saveData['loginPwd']=md5($data['new_pwd'].$saveData['loginSecret']);
        $res=$this->where(['supplier_id'=>$uid])->save($saveData);
        if($res!==false){
            return ['status'=>1,'msg'=>'密码修改成功!'];
        }else{
            return ['status'=>0,'msg'=>'密码修改失败!'];
        }
    }

}
