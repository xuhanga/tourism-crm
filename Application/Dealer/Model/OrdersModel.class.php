<?php
namespace Dealer\Model;
/**
 * 订单
 */
class OrdersModel extends BaseModel{

    protected $tableName = 'line_orders';


    /**
     * 订单列表
     */
    public  function  orderList($where){
        $db_prefix=C('DB_PREFIX');
        $field="o.order_status,o.operator_cost,o.order_id,o.order_num,o.tourists_ids,o.contact_id,l.group_num,l.line_name,o.adult_num,o.child_num,o.oldMan_num,o.create_time,o.out_time,o.order_status,op.operator_name,tou.tourists_name,tou.tourists_phone";
        $count=M('line_orders as o')
            ->join("LEFT JOIN __PUBLIC_LINE__ as l on l.line_id=o.line_id")
            ->join("LEFT JOIN __OPERATOR__ as op on l.operator_id=op.operator_id")
            ->join("LEFT JOIN __PUBLIC_TOURISTS__ as tou on tou.tourists_id=o.contact_id")
            ->field($field)
            ->where($where)
            ->count();
        $Page       = new \Think\Page($count,25);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show       = $Page->show();// 分页显示输出

        $list=M('line_orders as o')
            ->join("LEFT JOIN __PUBLIC_LINE__ as l on l.line_id=o.line_id")
            ->join("LEFT JOIN __OPERATOR__ as op on l.operator_id=op.operator_id")
            ->join("LEFT JOIN __PUBLIC_TOURISTS__ as tou on tou.tourists_id=o.contact_id")
            ->field($field)
            ->limit($Page->firstRow.','.$Page->listRows)
            ->order('o.order_id desc')
            ->where($where)
            ->select();

        foreach ($list as $k=>$v){
            $list[$k]['order_status']=self::orderStatus($v['order_status']);
        }

        return ['list'=>$list,'show'=>$show];
    }

    /**
     * 导出订单列表
     */
    public  function  exportOrderList($where){
        ini_set('max_execution_time', '0');
        $field="o.order_id,o.order_num,o.tourists_ids,o.contact_id,l.group_num,l.line_name,o.adult_num,o.child_num,o.oldMan_num,o.create_time,o.out_time,o.order_status,op.operator_name,tou.tourists_name,tou.tourists_phone";
        $list=M('line_orders as o')
            ->join("LEFT JOIN __PUBLIC_LINE__ as l on l.line_id=o.line_id")
            ->join("LEFT JOIN __OPERATOR__ as op on l.operator_id=op.operator_id")
            ->join("LEFT JOIN __PUBLIC_TOURISTS__ as tou on tou.tourists_id=o.contact_id")
            ->order('o.order_id desc')
            ->field($field)
            ->where($where)
            ->select();


        foreach ($list as $k=>$v){
            $list[$k]['create_time']=date('Y-m-d',$v['create_time']);
            $list[$k]['out_time']=date('Y-m-d',$v['out_time']);
            $list[$k]['order_status']=self::orderStatus($v['order_status']);
            $list[$k]['tourists_name']=$v['tourists_name']?str_replace('==','',$v['tourists_name']):'无';
            $list[$k]['tourists_phone']=$v['tourists_phone']?$v['tourists_phone']:'无';
            $list[$k]['headcount']="成".$v['adult_num']."人;儿".$v['child_num']."人";
        }



        $expCellName  = array(
            array('order_num','订单号'),
            array('tourists_name','联系人'),
            array('tourists_phone','联系电话'),
            array('line_name','线路名称'),
            array('group_num','	团号'),
            array('headcount','人数'),
            array('create_time','下单时间'),
            array('out_time','	出团日期'),
            array('order_status','	订单状态'),
            array('operator_name','	运营商'),
        );


        $supplier_name=M('operator_line_supplier')->where(['supplier_id'=>$where['o.origin_line_supplier_id']])->getField('supplier_name');
        $fileName=$supplier_name."订单列表";

        parent::exportExcel($fileName,$expCellName,$list);

    }

    /**
     * 订单状态 -6 取消，-5 已退款 ，-4 拒绝退款， -3 退款审核中，-2 审核不通过，-1 未支付，1 待审核(预订成功)(已支付)，2 审核中，3 已审核(已结算)
     */
    public  function  orderStatus($status){
        $name='';
        switch ($status){
            case 3:$name='已审核';break;
            case 2:$name='审核中';break;
            case 1:$name='待审核';break;
            case -1:$name='未支付';break;
            case -3:$name='退款审核中';break;
            case -4:$name='拒绝退款';break;
            case -5:$name='已退款';break;
            case -6:$name='订单取消';break;
        }
        return $name;
    }


    /**
     * 订单详情
     */
    public  function  detail($id){

        $field="o.order_remarks,o.order_id,o.order_num,o.tourists_ids,o.contact_id,l.group_num,l.line_name,o.adult_num,o.child_num,o.oldMan_num,o.create_time,o.out_time,o.order_status,op.operator_name,tou.tourists_name,tou.tourists_phone";

        $list=M('line_orders as o')
            ->join("LEFT JOIN __PUBLIC_LINE__ as l on l.line_id=o.line_id")
            ->join("LEFT JOIN __OPERATOR__ as op on l.operator_id=op.operator_id")
            ->join("LEFT JOIN __PUBLIC_TOURISTS__ as tou on tou.tourists_id=o.contact_id")
            ->field($field)
            ->where(['o.order_id'=>$id])
            ->find();

        $list['create_time']=date('Y-m-d',$list['create_time']);
        $list['out_time']=date('Y-m-d',$list['out_time']);
        $list['order_status']=self::orderStatus($list['order_status']);
        $list['headcount']="成".$list['adult_num']."人;儿".$list['child_num']."人";

        //发票信息
        $invoice=M('public_receipt')->where(['order_id'=>$id])->find();


        //游客信息
        $tourist=M('public_tourists')->where(['order_id'=>$id])->select();

        return ['orderInfo'=>$list,'invoice'=>$invoice,'tourist'=>$tourist];


    }
}

