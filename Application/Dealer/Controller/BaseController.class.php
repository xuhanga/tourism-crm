<?php
namespace Dealer\Controller;
use Think\Controller;
/**
 * Base基类控制器
 */
class BaseController extends Controller{
    protected  $admin_id;
    /**
     * 初始化方法
     */
    public function __construct(){
        parent::__construct();

        //币种列表
        $currency=S('currency');
        if(!$currency){
            $currencyList=M('currency')->select();
            $tempCurrency=[];
            foreach ($currencyList as $k=>$v){
                $tempCurrency[$v['currency_id']]=$v['currency_name'];
                $tempCurrency[$v['currency_id'].'_sym']=$v['currency_symbol'];
            }
            $currency=$tempCurrency;
            S('currency',$tempCurrency,3600);
        }
        $this->assign('currency',$currency);


        //超管资料
        $supplierInfo=session('dealer_user');
        if(!$supplierInfo){
            $admin_id=session('dealer_user.pid')==0?session('dealer_user.supplier_id'):session('dealer_user.pid');
            $supplierInfo=M('operator_line_supplier')->where(array('supplier_id'=>$admin_id))->find();
            S('supplierInfo',$supplierInfo,3600);
        }
        $supplierInfo['currency']=$currency[$supplierInfo['currency_id']];

        $this->assign('supplierInfo',$supplierInfo);

        $auth=new \Think\Auth();
        $rule_name=MODULE_NAME.'/'.CONTROLLER_NAME.'/'.ACTION_NAME;

        //不需要验证的控制器方法列表
        $notValidate=C('NOT_VALIDATE');

        //登录验证
        if(!session('dealer_user')){
            header('Location:'.U('Dealer/Login/login'));
            exit();
        }

        //超级管理员ID
        $userInfo=session('dealer_user');
        if($userInfo['pid']==0){
            $this->admin_id=$userInfo['supplier_id'];
        }else{
            $this->admin_id=$userInfo['pid'];
        }

        //登录控制器不用验证
        if(!in_array($rule_name,$notValidate)){

            $result=$auth->check($rule_name,session('dealer_user.supplier_id'));
            if(!$result){
                $this->error('您没有权限访问',U('Dealer/Login/login'));
            }
        }
        // 分配菜单数据  'platform'=>2 在模型里面配置一下参数 1运营商,2经销商,3分销
        $nav_data=D('AdminNav')->getTreeData('level','order_number,id');
        $assign=array(
            'nav_data'=>$nav_data
        );
        $this->assign($assign);

    }


    public  function getCurrency(){
        $currency=S('currency');
        $supplierInfo=session('dealer_user');
        return $currency[$supplierInfo['currency_id']];
    }


    /**
     * ajax上传文件
     */
    public function ajax_upload_file()
    {
        $upload = new \Think\Upload();// 实例化上传类
        $upload->maxSize   =     31457280 ;// 设置附件上传大小
        $upload->exts     	=     array('jpg', 'gif', 'png', 'jpeg','doc','docx','xlsx','pdf','wps','txt','xls');// 设置附件上传类型
        $upload->rootPath	= 	  './Upload';
        $upload->savePath  =      '/business/'; // 设置附件上传目录
        // 上传文件
        $info   =   $upload->upload();
        if(!$info)
        {
            // 上传错误提示错误信息
            $this->error($upload->getError());
        }else{
            $path='/Upload'.$info['file']['savepath'].$info['file']['savename'];
            $this->ajaxReturn(['path'=>$path,'name'=>$info['file']['name'],'status'=>1]);
        }
    }



}
