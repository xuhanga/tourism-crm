<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>
        
    </title>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="/Public/statics/aceadmin/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="/Public/statics/font-awesome-4.4.0/css/font-awesome.min.css"/>
    <!--[if IE 7]>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/font-awesome-ie7.min.css"/><![endif]-->
    <!-- <link rel="stylesheet" href="/Public/statics/aceadmin/css/ace.min.css"/> -->
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/ace-ie.min.css"/><![endif]--><!--[if lt IE 9]>
    <script src="/Public/statics/aceadmin/js/html5shiv.js"></script>
    <script src="/Public/statics/aceadmin/js/respond.min.js"></script><![endif]-->
    <link rel="stylesheet" href="/Tpl/Operator/css/base.css"/>
    <link rel="stylesheet" href="/Tpl/Operator/css/common.css"/>
    
</head>
<body>



<script src="/Public/statics/js/jquery-1.10.2.min.js"></script><!-- <![endif]--><!--[if IE]>
<script src="/Public/statics/js/jquery-1.10.2.min.js"></script><![endif]--><!--[if !IE]> -->
<script type="text/javascript">
    window.jQuery || document.write("<script src='/Public/statics/aceadmin/js/jquery-2.0.3.min.js'>" + "<" + "script>");
</script><!-- <![endif]--><!--[if IE]>
<script type="text/javascript">
    window.jQuery || document.write("<script src='/Public/statics/aceadmin/js/jquery-1.10.2.min.js'>" + "<" + "script>");
</script><![endif]-->
<script type="text/javascript">
    if ("ontouchend" in document) document.write("<script src='/Public/statics/aceadmin/js/jquery.mobile.custom.min.js'>" + "<" + "script>");
</script>
<script src="/Public/statics/aceadmin/js/bootstrap.min.js"></script>
<script src="/Public/statics/aceadmin/js/typeahead-bs2.min.js"></script>
<!--[if lte IE 8]>
<script src="/Public/statics/aceadmin/js/excanvas.min.js"></script><![endif]-->
<script src="/Public/statics/aceadmin/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.ui.touch-punch.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.slimscroll.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.easy-pie-chart.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.sparkline.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.pie.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.resize.min.js"></script>
<script src="/Public/statics/aceadmin/js/ace-elements.min.js"></script>
<!-- <script src="/Public/statics/aceadmin/js/ace.min.js"></script> -->
<script src="/Public/statics/Operator/js/base.js"></script>
<script src="/Public/statics/layer/layer.js"></script>

</body>
</html>
<style>#table .btn-primary{ margin-bottom: 5px; }</style>

    <div class="page-header"><h1>销售管理 &gt;订单列表</h1></div>
    <form id="form" action="<?php echo U('orderList',['p'=>1]);?>" >
        <div class="col-xs-12 clearfix">
            <div class="col-xs-3">
                <label for="" class="col-xs-4">订单号</label>
                <div class="col-xs-8"><input name="order_num" value="<?php if($_GET['order_num']): echo ($_GET['order_num']); endif; ?>" type="text" class="form-control" /></div>
            </div>
            <div class="col-xs-3">
                <label for="" class="col-xs-4">线路名称</label>
                <div class="col-xs-8"><input name="line_name" type="text" value="<?php if($_GET['line_name']): echo (urldecode( $_GET['line_name'])); endif; ?>" class="form-control" /></div>
            </div>
            <div class="col-xs-3">
                <label for="" class="col-xs-4">出发城市</label>
                <div class="col-xs-8"><input name="set_out" type="text" value="<?php if($_GET['set_out']): echo (urldecode( $_GET['set_out'] )); endif; ?>" class="form-control" class="form-control" /></div>
            </div>
            <div class="col-xs-3">
                <label for="" class="col-xs-4">订单状态</label>
                <div class="col-xs-8">
                    <select name="order_status">
                        <option value="1,3">全部</option>
                        <option value="1,3" <?php if($_GET['order_status'] == '1,3'): ?>selected<?php endif; ?>>已支付</option>
                        <option value="-5" <?php if($_GET['order_status'] == -5): ?>selected<?php endif; ?>>拒绝退款</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-xs-12 clearfix mt-15">
            <div class="col-xs-3">
                <label for="" class="col-xs-4">游客姓名</label>
                <div class="col-xs-8"><input  name="tourists_name" type="text" value="<?php if($_GET['tourists_name']): echo (urldecode( $_GET['tourists_name'] )); endif; ?>" class="form-control" class="form-control" /></div>
            </div>

            <div class="col-xs-3">
                <label for="" class="col-xs-4">联系方式</label>
                <div class="col-xs-8"><input type="text" name="tourists_phone"  value="<?php if($_GET['tourists_phone']): echo ($_GET['tourists_phone']); endif; ?>" class="form-control" class="form-control" /></div>
            </div>
            <!-- 分销商 -->
            <div class="col-xs-3">
                <label for="" class="col-xs-4">分销商</label>
                <div class="col-xs-8">
                    <select name="reseller_id">
                        <option value="">全部</option>
                        <?php if(is_array($reseller)): foreach($reseller as $key=>$v): ?><option value="<?php echo ($v['reseller_id']); ?>" <?php if($v['reseller_id'] == $_GET['reseller_id']): ?>selected<?php endif; ?>><?php echo ($v['reseller_name']); ?></option><?php endforeach; endif; ?>
                    </select>
                </div>
            </div>
            <!-- 直营门店 -->
            <?php if(shop): ?><div class="col-xs-3">
                    <label for="" class="col-xs-4">直营门店</label>
                    <div class="col-xs-8">
                        <select name="shop_id">
                            <option value="">全部</option>
                            <?php if(is_array($shop)): foreach($shop as $key=>$v): ?><option value="<?php echo ($v['reseller_id']); ?>" <?php if($v['reseller_id'] == $_GET['shop_id']): ?>selected<?php endif; ?>><?php echo ($v['reseller_name']); ?></option><?php endforeach; endif; ?>
                        </select>
                    </div>
                </div><?php endif; ?>
        </div>

        <div class="col-xs-12 clearfix mt-15">
            <div class="col-xs-6">
                <label for="" class="col-xs-2">下单时间</label>
                <div class="col-xs-4"><input name="start_time" id="start" placeholder="开始时间" value="<?php if($_GET['start_time']): echo (urldecode( $_GET['start_time'] )); endif; ?>" class="form-control" class="form-control" /></div>
                <!-- <div class="col-xs-1"></div> -->
                <div class="col-xs-4"><input name="end_time" id="end" placeholder="结束时间" value="<?php if($_GET['end_time']): echo (urldecode( $_GET['end_time'] )); endif; ?>" class="form-control" class="form-control" /></div>
            </div>


            <!-- <div class="col-xs-3">
                <label for="" class="col-xs-4">销售人</label>
                <div class="col-xs-8">
                    <select name="sales_id">
                        <option value="">全部</option>
                        <?php if(is_array($staff)): foreach($staff as $key=>$v): ?><option value="<?php echo ($v['reseller_id']); ?>" <?php if($v['reseller_id'] == $_GET['sales_id']): ?>selected<?php endif; ?>><?php echo ($v['reseller_name']); ?></option><?php endforeach; endif; ?>
                    </select>
                </div>
            </div> -->

        </div>
        <div class="col-xs-12 clearfix mt-15">
            <div class="col-xs-9">
                <button type="button" id="export" class="btn btn-primary" style="margin-right: 10px">导出</button>
                <button class="btn btn-primary" style="margin-right: 10px" >查询</button>
                <div class="dropdown" style="display: inline-block">
                    <button type="button" class="btn btn-primary dropdown-toggle"  id="dropdownMenu1" data-toggle="dropdown">筛选<span class="caret"></span></button>
                    <ul id="menuList" class="dropdown-menu pull-right" role="menu" aria-labelledby="dropdownMenu1" style="padding:10px;">
                        <li><input type="checkbox" class="filtrate" checked="" value="0" id="filtrate_1"><label for="filtrate_1">订单号</label></li>
                        <li><input type="checkbox" class="filtrate" checked="" value="1" id="filtrate_2"><label for="filtrate_2">线路名称</label></li>
                        <li><input type="checkbox" class="filtrate" checked="" value="2" id="filtrate_3"><label for="filtrate_3">团号</label></li>
                        <li><input type="checkbox" class="filtrate" checked="" value="3" id="filtrate_4"><label for="filtrate_4">游客姓名</label></li>
                        <li><input type="checkbox" class="filtrate" checked="" value="4" id="filtrate_5"><label for="filtrate_5">联系方式</label></li>
                        <li><input type="checkbox" class="filtrate" checked="" value="5" id="filtrate_6"><label for="filtrate_6">预订人数</label></li>
                        <li><input type="checkbox" class="filtrate" checked="" value="6" id="filtrate_7"><label for="filtrate_7">销售单价</label></li>
                        <li><input type="checkbox" class="filtrate" checked="" value="7" id="filtrate_8"><label for="filtrate_8">订单总额</label></li>
                        <li><input type="checkbox" class="filtrate" checked="" value="8" id="filtrate_9"><label for="filtrate_9">直营门店</label></li>
                        <li><input type="checkbox" class="filtrate" checked="" value="9" id="filtrate_10"><label for="filtrate_10">销售人</label></li>
                        <li><input type="checkbox" class="filtrate" checked="" value="10" id="filtrate_11"><label for="filtrate_11">出发城市</label></li>
                        <li><input type="checkbox" class="filtrate" checked="" value="11" id="filtrate_12"><label for="filtrate_12">下单时间</label></li>
                        <li><input type="checkbox" class="filtrate" checked="" value="12" id="filtrate_13"><label for="filtrate_13">分销商</label></li>
                        <li><input type="checkbox" class="filtrate" checked="" value="13" id="filtrate_14"><label for="filtrate_14">余位</label></li>
                        <li><input type="checkbox" class="filtrate" checked="" value="14" id="filtrate_15"><label for="filtrate_15">状态</label></li>
                        <li><input type="checkbox" class="filtrate" checked="" value="15" id="filtrate_16"><label for="filtrate_16">审核状态</label></li>
                        <li><input type="checkbox" class="filtrate" checked="" value="16" id="filtrate_17"><label for="filtrate_17">负责人</label></li>
                        <li><input type="checkbox" class="filtrate" checked="" value="17" id="filtrate_18"><label for="filtrate_18">操作</label></li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-3 text-right ">销售总额：<span class="text-danger">&#165;<?php echo ($orderList['total_money']); ?></span>
            </div>
        </div>
    </form>

    <table id="table" class="table table-striped  table-bordered table-hover table-condensed  mt-15">
        <thead>
            <tr>
                <th>订单号</th>
                <th>线路名称</th>
                <th>団号</th>
                <th>游客姓名</th>
                <th>联系方式</th>
                <th>预定人数</th>
                <th>销售单价</th>

                <th>订单总额</th>
                <th>直营门店</th>

                <th>销售人</th>
                <th>出发城市</th>
                <th>下单时间</th>
                <th>分销商</th>
                <th>余位</th>
                <th>团状态</th>
                <th>状态</th>
                <th>审核状态</th>
                <th>负责人</th>
                <th>操作</th>
            </tr>
        </thead>
        <tbody>
            <?php if(is_array($orderList['list'])): foreach($orderList['list'] as $key=>$v): ?><tr>
                <td><?php echo ($v['order_num']); ?></td>
                <td><?php echo ($v['line_name']); ?></td>
                <td><?php echo ($v['group_num']); ?></td>
                <td><?php echo ($v['tourists_name']); ?></td>
                <td><?php echo ($v['tourists_phone']); ?></td>
                <td>
                    预订人数：<?php echo ($v['total_num']); ?><br/>
                    大：<?php echo ($v['adult_num']); ?><br/>
                    小：<?php echo ($v['child_num']); ?><br/>
                   <!-- 老：<?php echo ($v['oldMan_num']); ?>-->
                </td>
                <td>大：<?php echo ($v['adult_price']); ?><br/>
                    小：<?php echo ($v['child_price']); ?><br/>
                  <!--  老：<?php echo ($v['oldman_price']); ?>-->
                </td>
                <td><?php echo ($v['total_money']); ?></td>
                <td><?php echo ($v['store']); ?></td>
                <td><?php echo ($v['sales']); ?></td>
                <td><?php echo ($v['set_out']); ?></td>
                <td><?php echo ($v['create_time']); ?></td>
                <td><?php echo ($v['reseller']); ?></td>
                <td><?php echo ($v['leave_num']); ?></td>
                <td><?php echo ($v['group_text']); ?></td>
                <?php echo ($v['html']); ?>
                <td><?php echo ($v['manager']); ?></td>
                <td>
                    <?php if($v['order_status'] == -1): ?><button class="btn btn-primary"  onclick="cancel(<?php echo ($v['order_id']); ?>)">取消</button><?php endif; ?>
                    <!-- 有权限and订单已审核and(待成团or已成团) -->
                    <?php if(($v['order_status'] == 3) && ($v['group_status'] == 0 || $v['group_status'] == 1 || $v['group_status'] == -1)): ?><button class="btn btn-primary" onclick="refuse('<?php echo ($v['order_num']); ?>', <?php echo ($v['order_id']); ?>)">退款</button><?php endif; ?>

                    <button class="btn btn-primary"  onclick="openDetail(<?php echo ($v['order_id']); ?>)">详情</button><br/>

                    <?php if($v['order_status'] == 1): ?><button class="btn btn-primary" onclick="passAduit(<?php echo ($v['order_id']); ?>, '<?php echo ($v['order_num']); ?>')">通过审核</button>
                        <button class="btn btn-primary" onclick="refuseAduit(<?php echo ($v['order_id']); ?>, '<?php echo ($v['order_num']); ?>')">拒绝审核</button><?php endif; ?>

                </td>
            </tr><?php endforeach; endif; ?>
        </tbody>
    </table>
    <ul class="pagination pull-right"><?php echo ($orderList['show']); ?></ul>


<script src="/Tpl/Operator/js/jedate/jedate.js"></script>
<script>
/**
 * 订单审核
 * @param  {[int]} oid    [订单id]
 * @param  {[string]} num [订单编号]
 */
function passAduit(oid, num){
    layer.confirm('订单号：'+num,{
        btn : ['通过', '返回'],
        title: '订单审核'
    },function(){
        layer.closeAll();
        aduitRequest(oid, 1);
    })
}

/**
 * 订单审核
 * @param  {[int]} oid    [订单id]
 * @param  {[string]} num [订单编号]
 */
function refuseAduit(oid, num){
    layer.confirm('订单号：'+num,{
        btn : [ '拒绝', '返回'],
        title: '订单审核'
    },function(){
        layer.closeAll();
        aduitRequest(oid, 2);
    })
}

/**
 * 发送点单审核请求
 * @param  {[int]} oid [订单id]
 * @param  {[int]} type [1：通过  2：拒绝]
 */
function aduitRequest(oid, type){
    $.post('<?php echo U('aduit');?>', {order_id : oid, type : type}, function(data){
        if(data.status ==1 ){
            layer.msg('操作成功', {icon: 6})
            setTimeout(function(){
                location.reload();
            }, 1000)
        }else{
            layer.msg('操作失败', {icon: 5})
        }
    }, 'json' )
}

//筛选
$('#menuList').on('change', '.filtrate', function(){
    var index = $(this).val();
    if($(this).prop('checked')){
        $('#table tr').each(function(i,n){  $(n).children().eq(index).show(); })
    }else{
        $('#table tr').each(function(i,n){  $(n).children().eq(index).hide();})
    }
})

$form = $('#form');
//导出excel
$('#export').click(function(){
    $form.attr('action', '<?php echo U('exportExcels');?>');
    $form.submit();
    $form.attr('action', '');
})

dateFormat($('#start'), 'YYYY-MM-DD');
dateFormat($('#end'), 'YYYY-MM-DD');

/**
 * 绑定日期选择器
 * @param  {[obj]}    obj        [元素]
 * @param  {[string]} dateFormat [时间格式]
 */
function dateFormat(obj, dateFormat){
    jeDate({
        dateCell: '#'+$(obj).attr('id'),
        format: dateFormat,
        isinitVal:false,
        isTime:true, //isClear:false,
        okfun:function(val){
        }
    });
}

/**
 * 订单取消
 * @param  {[int]}    orderId     [订单id]
 */
function cancel(oId){
    layer.confirm('将此订单放入订单回收站?',
    { btn: ['确认', '取消'] },function(){
        $.post('<?php echo U('cancel');?>',{order_id: oId}, function(data){
            layer.msg(data.msg);
            if(data.status == 1){
                setTimeout(function(){
                    location.reload();
                },1000)
                return ;
            }
        },'json')
    })
}

/**
 * 打开订单详情页面
 * @param  {[int]}    orderId     [订单id]
 */
function openDetail(oId){
    layer.open({
        type: 2,
        area: ['100%','100%'],
        title: '订单详情',
        content: '/Operator/Index/orderDetail/order_id/'+oId,
    })
}

/**
 * 退款
 * @param  {[string]}    orderNum     [订单编号]
 * @param  {[int]}    oId     [订单id]
 */
function refuse(orderNum, oId){
    layer.open({
        type: 2,
        area: ['400px', '300px'],
        title: '订单详情',
        content: '/Operator/SaleManger/refuseHtml/orderNum/'+orderNum+'/orderId/'+oId,
    })
}

/**
 * 退款
 * @param  {[string]}    orderNum     [订单编号]
 * @param  {[int]}    oId     [订单id]
 */
// function refuse(orderNum, oId){
//     layer.confirm('更改订单状态为待退款?',
//     { btn: ['确认', '取消'] },function(){
//         $.post('<?php echo U('toWaitRefund');?>',{order_id: oId}, function(data){
//             if(data.status == 1){
//                 location.reload();
//                 return ;
//             }
//             layer.msg('操作失败');
//         },'json')
//     })
// }

/**
 * 确认支付
 * @param  {[int]}    orderId     [订单id]
 */
function confirmPay(oId){
    layer.confirm('更改该订单状态为已支付?',
    { btn: ['确认', '取消'] },function(){
        $.post('<?php echo U('confirmPay');?>',{order_id: oId}, function(data){
            if(data.status == 1){
                location.reload();
                return ;
            }
            layer.msg('操作失败');
        },'json')
    })
}

</script>