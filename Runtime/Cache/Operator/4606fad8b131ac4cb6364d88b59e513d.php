<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>
        
    </title>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="/Public/statics/aceadmin/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="/Public/statics/font-awesome-4.4.0/css/font-awesome.min.css"/>
    <!--[if IE 7]>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/font-awesome-ie7.min.css"/><![endif]-->
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/ace.min.css"/>
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/ace-ie.min.css"/><![endif]--><!--[if lt IE 9]>
    <script src="/Public/statics/aceadmin/js/html5shiv.js"></script>
    <script src="/Public/statics/aceadmin/js/respond.min.js"></script><![endif]-->
    <!-- <link rel="stylesheet" href="/Public/css/base.css"/> -->
    <style>
        ::-webkit-scrollbar {
            width: 10px;
            height: 5px;
        }

        ::-webkit-scrollbar-corner, ::-webkit-scrollbar-track {
            background-color: #e2e2e2;
        }

        ::-webkit-scrollbar-thumb {
            border-radius: 0;
            background-color: rgba(0,0,0,.3);
        }

        ::-webkit-scrollbar-corner, ::-webkit-scrollbar-track {
            background-color: #e2e2e2;
        }
        ul,li{ list-style: none; }
        ol{margin:0;}
        .jedatehms li{display: none;}
        #jedatebox ul{
            padding-right: 0;
            margin-right: 0;
        }
    </style>
    
</head>
<body>



<script src="/Public/statics/js/jquery-1.10.2.min.js"></script><!-- <![endif]--><!--[if IE]>
<script src="/Public/statics/js/jquery-1.10.2.min.js"></script><![endif]--><!--[if !IE]> -->
<script type="text/javascript">
    window.jQuery || document.write("<script src='/Public/statics/aceadmin/js/jquery-2.0.3.min.js'>" + "<" + "script>");
</script><!-- <![endif]--><!--[if IE]>
<script type="text/javascript">
    window.jQuery || document.write("<script src='/Public/statics/aceadmin/js/jquery-1.10.2.min.js'>" + "<" + "script>");
</script><![endif]-->
<script type="text/javascript">
    if ("ontouchend" in document) document.write("<script src='/Public/statics/aceadmin/js/jquery.mobile.custom.min.js'>" + "<" + "script>");
</script>
<script src="/Public/statics/aceadmin/js/bootstrap.min.js"></script>
<script src="/Public/statics/aceadmin/js/typeahead-bs2.min.js"></script>
<!--[if lte IE 8]>
<script src="/Public/statics/aceadmin/js/excanvas.min.js"></script><![endif]-->
<script src="/Public/statics/aceadmin/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.ui.touch-punch.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.slimscroll.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.easy-pie-chart.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.sparkline.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.pie.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.resize.min.js"></script>
<script src="/Public/statics/aceadmin/js/ace-elements.min.js"></script>
<script src="/Public/statics/aceadmin/js/ace.min.js"></script>
<script src="/Public/statics/Operator/js/base.js"></script>
<script src="/Public/statics/layer/layer.js"></script>
<!-- <script src="/Public/js/base.js"></script> -->

<script>
    var publicurl="/Public";
    var domainURL="";

    $(function () {
        var bodyH=$(document).height();
        try{
            //parent.resetFrameHeight(bodyH);
        }catch (err){

        }

    })
</script>
</body>
</html>

    <link rel="stylesheet" type="text/css" href="/Tpl/Operator/js/jedate/skin/jedate.css">
    <link rel="stylesheet" type="text/css" href="/Tpl/Operator/css/Index/index.css">


    <div class="page-header"><h1> 首页&gt; 数据统计</h1></div>
    <form action="" class="clearfix">
    <section id="dataCount" class="col-xs-12 row">
        <!-- 饼状图 -->
        <section class="data col-xs-6">
            <div class="row">
                <div class="col-xs-2"></div>
                <label for="circDate" class="col-xs-2 position-top">日期选择</label>
                <div class="col-xs-4">
                    <input id="circDate" name="day" value="<?php if($_GET['day']): echo ($_GET['day']); endif; ?>" type="text"  class="form-control" />
                </div>
                <div class="col-xs-2">
                    <button class="btn btn-primary ">查询</button>
                </div>
            </div>
            <div id="pieChart" class="shape"></div>
            <div id="pieChart2" class="shape"></div>
        </section>

        <!-- 条形图 -->
        <section  class="data col-xs-6">
            <div class="row">
                <div class="col-xs-2 position-top">
                    <select name="choose" id="dateChange">
                        <option value="0"  <?php if($_GET['month'] && !$_GET['choose']) echo 'selected'; ?> >月</option>
                        <option value="1" <?php if($_GET['years'] && $_GET['choose']) echo 'selected'; ?>>年</option>
                    </select>
                </div>
                <div class="col-xs-4">
                    <select id="selectYear" <?php if(!($_GET['years'] && $_GET['choose'])) echo 'style="display:none;"'; ?> name="years">
                        <?php if(is_array($years)): foreach($years as $key=>$v): ?><option <?php if($v == $selectYear): ?>selected<?php endif; ?> value="<?php echo ($v); ?>"><?php echo ($v); ?></option><?php endforeach; endif; ?>
                    </select>
                    <input id="barDate"  <?php if(($_GET['years'] && $_GET['choose'])) echo 'style="display:none;"'; ?> value='<?php if($_GET['month']): echo ($_GET['month']); endif; ?>' name="month" type="text"  class="form-control" />
                </div>
                <div class="col-xs-2">
                    <button class="btn btn-primary ">查询</button>
                </div>
            </div>
            <div id="barChart" class="bar-shape"></div>
        </section>
    </section>

     <!--产品审核-->
<style>th{text-align:center;}</style>
<p style="clear: both;"></p>


<h3>待确认订单</h3>
<div class="newMess">
    <table class="table table-striped table-bordered table-hover table-condensed text-center">
        <tbody>
        <tr>
            <th>订单号</th>
            <th>线路名称</th>
            <th>团号</th>
            <th>游客姓名</th>
            <th>联系方式</th>
            <th>预定人数</th>
            <th>订单总额</th>
            <th>直营门店</th>
            <th>下单时间</th>
            <th>分销商</th>
            <th>操作</th>
        </tr>
            <?php if(is_array($auditOrderList)): $i = 0; $__LIST__ = $auditOrderList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><tr>
                    <td><?php echo ($v['order_num']); ?></td>
                    <td><?php echo ($v['line_name']); ?></td>
                    <td><?php echo ($v['group_num']); ?></td>
                    <td><?php echo ($v['tourists_name']); ?></td>
                    <td><?php echo ($v['tourists_phone']); ?></td>
                    <td>
                        预订人数：<?php echo ($v['total_num']); ?><br/>
                        大：<?php echo ($v['adult_num']); ?><br/>
                        小：<?php echo ($v['child_num']); ?><br/>
                    </td>
                    <td><?php echo ($v['total_money']); ?></td>
                    <td><?php echo ($v['store']); ?></td>
                    <td><?php echo ($v['create_time']); ?></td>
                    <td><?php echo ($v['reseller']); ?></td>
                    <td>
                        <?php if(checkAuth('Operator/SaleManger/orderList')): ?><a href="<?php echo U('Operator/SaleManger/orderList');?>" class="btn btn-xs btn-primary">详情</a><?php endif; ?>
                    </td>
                </tr><?php endforeach; endif; else: echo "" ;endif; ?>
        </tbody>
    </table>
</div>


<h3>待结算订单</h3>
<div class="newMess">
    <table class="table table-striped table-bordered table-hover table-condensed text-center">
        <tbody>
        <tr>
            <th>订单信息</th>
            <th>预订人数</th>
            <th>门店/分销</th>
            <th>销售单价</th>
            <th>销售人</th>
            <th>结算总金额</th>
            <th>币种</th>
            <th>汇率</th>
            <th>实收人民币</th>
            <th>下单时间</th>
            <th>操作人</th>
            <th>操作</th>
        </tr>
        <?php if(is_array($closingList)): $i = 0; $__LIST__ = $closingList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><tr>
                <td>
                    订单号：<?php echo ($v["order_num"]); ?><br>
                    团号：<?php echo ($v["group_num"]); ?><br>
                    线路名称：<?php echo ($v["line_name"]); ?>
                    <?php if($v['order_type'] == 1): ?>(自营)
                        <?php elseif($v['order_type'] == 2): ?>(供应商)<?php endif; ?>
                </td>
                <td>
                    成人：<?php echo ($v["adult_num"]); ?><br>
                    儿童：<?php echo ($v["child_num"]); ?><br>
                    合计：<?php echo ($v["total_num"]); ?>人
                </td>
                <td><?php echo ($v['reseller']); echo ($v['order_type'] == 1 ? '(自营)' : '(分销)'); ?></td>
                <!-- <td></td> -->
                <td>
                    成人：<?php echo ($v['adult_price']); ?><br>
                    儿童：<?php echo ($v['oldman_price']); ?><br>
                </td>
                <td><?php echo ($v['sales']); ?></td>
                <td><?php echo ($v["total_money"]); ?></td>
                <td><?php echo ($v["currency_type"]); ?></td>
                <td><?php echo ($v["settlement_rate"]); ?></td>
                <td><?php echo ($v['end_need_pay']); ?></td>
                <td><?php echo ($v['create_time']); ?></td>
                <td><?php echo ($v['operation']); ?></td>
                <td>
                    <?php if(checkAuth('Operator/Finance/settlement_list')): ?><a href="<?php echo U('Operator/Finance/settlement_list');?>" class="btn btn-xs btn-primary">详情</a><?php endif; ?>
                </td>
            </tr><?php endforeach; endif; else: echo "" ;endif; ?>
        </tbody>
    </table>
</div>



<h3>待上架线路</h3>
<div class="newMess">
    <table class="table table-striped table-bordered table-hover table-condensed text-center">
        <tbody>
        <tr>
            <th class="filtrate_1">线路编号 <!--<input type="checkbox" class="checkAll">--></th>
            <th class="filtrate_2">线路名称</th>
            <th class="filtrate_2">团号</th>
            <th class="filtrate_3">行程天数</th>
            <th class="filtrate_4">出发地</th>
            <th class="filtrate_5">发布时间</th>
            <th class="filtrate_6">供应商</th>
            <th class="filtrate_6">状态</th>
            <th class="filtrate_6">审核人</th>
            <th class="filtrate_7" style="width:80px">操作</th>
        </tr>
        <?php if(is_array($lineList)): $i = 0; $__LIST__ = $lineList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
                <td><?php echo ($vo["line_sn"]); ?></td>
                <td><?php echo ($vo["line_name"]); ?></td>
                <td><?php echo ($vo["group_num"]); ?></td>
                <td><?php echo ($vo["travel_days"]); ?></td>
                <td><?php echo ($vo["areaName"]); ?></td>
                <td> <?php echo (date("Y-m-d",$vo["create_time"])); ?></td>
                <td> <?php echo ((isset($vo["supplier_name"]) && ($vo["supplier_name"] !== ""))?($vo["supplier_name"]):"无"); ?></td>
                <td>
                    <?php switch($vo["line_status"]): case "1": ?>已上架<?php break;?>
                        <?php case "0": ?>待审核<?php break;?>
                        <?php case "-1": ?>已下架<?php break;?>
                        <?php case "-2": ?>已拒绝<?php break; endswitch;?>
                </td>
                <td><?php echo ($vo["operator_name"]); ?></td>
                <td>
                    <?php if(checkAuth('Operator/Product/lineList')): ?><a href="<?php echo U('Operator/Product/lineList',array('status'=>0));?>" class="btn btn-xs btn-primary">详情</a><?php endif; ?>
                </td>
            </tr><?php endforeach; endif; else: echo "" ;endif; ?>
        </tbody>
    </table>
</div>

<h3>待合作线路</h3>
<div class="newMess">
    <table class="table table-striped table-bordered table-hover table-condensed text-center">
        <tbody>
        <tr>
            <!-- <th>全选 <input type="checkbox" class="checkAll"></th>-->
            <th>线路编号</th>
            <th>线路名称</th>
            <th>行程天数</th>
            <th>出发地</th>
            <th>供应商</th>
            <th>成本价</th>
            <th>状态</th>
            <th>操作</th>
        </tr>
            <?php if(is_array($cooperationList)): $i = 0; $__LIST__ = $cooperationList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
                    <!-- <td><input type="checkbox" class="chk" value=""></td>-->
                    <td><?php echo ($vo["line_sn"]); ?></td>
                    <td><?php echo ($vo["line_name"]); ?></td>
                    <td><?php echo ($vo["travel_days"]); ?></td>
                    <td><?php echo ($vo["areaName"]); ?></td>
                    <td> <?php echo ((isset($vo["supplier_name"]) && ($vo["supplier_name"] !== ""))?($vo["supplier_name"]):"无"); ?></td>
                    <td>
                        <?php if($vo['adult_money'] > 0): ?>成<?php echo ($vo["adult_money"]); ?>;儿<?php echo ($vo["child_money"]); ?><!--;老<?php echo ($vo["oldman_money"]); ?>--><?php endif; ?>
                    </td>
                    <td>
                        <?php switch($vo["partner_status"]): case "0": ?>[待处理]<?php break;?>
                            <?php case "-1": ?>[已拒绝]<?php break;?>
                            <?php case "1": ?>[合作中]<?php break; endswitch;?>
                        <?php switch($vo["line_status"]): case "-1": ?>[线路已下架]<?php break;?>
                            <?php case "0": ?>[待上架审核]<?php break;?>
                            <?php case "1": ?>[线路已上架]<?php break;?>
                            <?php case "-2": ?>[线路已被拒]<?php break;?>
                            <?php case "-4": ?>[线路已过期]<?php break;?>
                            <?php case "-3": ?>[线路未编辑]<?php break; endswitch;?>

                    </td>
                    <td>
                        <?php if(checkAuth('Operator/Product/cooperationList')): ?><a href="<?php echo U('Operator/Product/cooperationList');?>" class="btn btn-xs btn-primary">详情</a><?php endif; ?>
                    </td>
                </tr><?php endforeach; endif; else: echo "" ;endif; ?>
        </tbody>
    </table>
</div>

<h3>单品供应商审核</h3>
<div class="newMess">
    <table class="table table-striped table-bordered table-hover table-condensed text-center">
        <tbody>
            <tr>
                <th class="filtrate_1">供应商编号 <!--<input type="checkbox" class="checkAll">--></th>
                <th class="filtrate_2">供应商名称</th>
                <th class="filtrate_2">所在地</th>
                <th class="filtrate_3">项目</th>
                <th class="filtrate_4">联系人</th>
                <th class="filtrate_5">联系电话</th>
                <th class="filtrate_6">状态</th>
                <th class="filtrate_6">合作时间</th>
                <th class="filtrate_7" style="width:80px">操作</th>
            </tr>
        <?php if(is_array($singleSupplier)): $i = 0; $__LIST__ = $singleSupplier;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
                <td><?php echo ($vo["supplier_sn"]); ?>
                </td>
                <td><?php echo ($vo["supplier_name"]); ?></td>
                <td><?php echo ($vo["city"]); ?></td>
                <td><?php echo ($vo["typeName"]); ?></td>
                <td><?php echo ($vo["Linkman"]); ?></td>
                <td><?php echo ($vo["Linkman_phone"]); ?></td>
                <td>
                    <?php switch($vo["status"]): case "1": ?>合作<?php break;?>
                        <?php case "-1": ?>停止<?php break; endswitch;?>
                </td>
                <td><?php echo ($vo["start_time"]); ?>-<?php echo ($vo["end_time"]); ?></td>
                <td style="width:60px;">
                    <?php switch($vo["type"]): case "1": if(checkAuth('Operator/Shops/trafficShops')): ?><a href="<?php echo U('Operator/Shops/trafficShops');?>" class="btn btn-xs btn-primary">详情</a><?php endif; break;?>
                        <?php case "2": if(checkAuth('Operator/Shops/hotelShops')): ?><a href="<?php echo U('Operator/Shops/hotelShops');?>" class="btn btn-xs btn-primary">详情</a><?php endif; break;?>
                        <?php case "4": if(checkAuth('Operator/Shops/scenicShops')): ?><a href="<?php echo U('Operator/Shops/scenicShops');?>" class="btn btn-xs btn-primary">详情</a><?php endif; break; case "3": if(checkAuth('Operator/Shops/insureShops')): ?><a href="<?php echo U('Operator/Shops/insureShops');?>" class="btn btn-xs btn-primary">详情</a><?php endif; break; endswitch;?>
                </td>
            </tr><?php endforeach; endif; else: echo "" ;endif; ?>
        </tbody>
    </table>
</div>
<h3>单品审核</h3>
<div class="newMess">
    <table class="table table-striped table-bordered table-hover table-condensed text-center">
        <tbody>
        <tr >
            <th>供应商名称</th>
            <th>产品名称</th>
            <th>供应商编号</th>
            <th>销售单价</th>
          <!--  <th>合作时间</th>-->
            <th>操作人</th>
            <th>审核状态</th>
            <th>是否上架</th>
            <th style="width:80px;">操作</th>
        </tr>
        <!--酒店-->
        <?php if(is_array($auditList['hotelList'])): $i = 0; $__LIST__ = $auditList['hotelList'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
                <td><?php echo ($vo["supplier_name"]); ?></td>
                <td>酒店</td>
                <td><?php echo ($vo["supplier_sn"]); ?></td>
                <td><?php echo ($vo["room_name"]); ?>: 儿童价：<?php echo ($vo["child_actual_cost"]); ?>元/人<br>成人价：<?php echo ($vo["adult_actual_cost"]); ?>元/人 </td>
             <!--   <td><?php echo (date("Y-m-d",$vo["start_time"])); ?>至<?php echo (date("Y-m-d",$vo["end_time"])); ?></td>-->
                <td><?php echo ($vo["audit_name"]); ?></td>
                <td>
                    <?php switch($vo["is_review"]): case "1": ?>已审核<?php break;?>
                        <?php case "-1": ?>未审核<?php break;?>
                        <?php case "-2": ?>已拒绝<?php break; endswitch;?>
                </td>
                <td>
                    <?php switch($vo["is_shelves"]): case "1": ?>是<?php break;?>
                        <?php case "-1": ?>否<?php break; endswitch;?>
                </td>
                <td>
                    <?php if(checkAuth('Dealer/Shops/hotelProductById')): ?><a href="<?php echo U('Shops/hotelProductById',array('id'=>$vo['supplier_id']));?>" class="btn btn-xs btn-primary">详情</a><?php endif; ?>
                </td>
            </tr><?php endforeach; endif; else: echo "" ;endif; ?>
        <!--景区-->

        <?php if(is_array($auditList['scenicList'])): $i = 0; $__LIST__ = $auditList['scenicList'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
                <td><?php echo ($vo["supplier_name"]); ?></td>
                <td>景区</td>
                <td><?php echo ($vo["supplier_sn"]); ?></td>
                <td><?php echo ($vo["scenic_name"]); ?>: 儿童价：<?php echo ($vo["child_actual_cost"]); ?>元/人<br>成人价：<?php echo ($vo["adult_actual_cost"]); ?>元/人 </td>
                <!--<td><?php echo (date("Y-m-d",$vo["start_time"])); ?>至<?php echo (date("Y-m-d",$vo["end_time"])); ?></td>-->
                <td><?php echo ($vo["audit_name"]); ?></td>
                <td>
                    <?php switch($vo["is_review"]): case "1": ?>已审核<?php break;?>
                        <?php case "-1": ?>未审核<?php break;?>
                        <?php case "-2": ?>已拒绝<?php break; endswitch;?>
                </td>
                <td>
                    <?php switch($vo["is_shelves"]): case "1": ?>是<?php break;?>
                        <?php case "-1": ?>否<?php break; endswitch;?>
                </td>
                <td>
                    <?php if(checkAuth('Dealer/Shops/scenicProductById')): ?><a href="<?php echo U('Shops/scenicProductById',array('id'=>$vo['supplier_id']));?>" class="btn btn-xs btn-primary">详情</a><?php endif; ?>

                </td>
            </tr><?php endforeach; endif; else: echo "" ;endif; ?>
        <!--保险-->
        <?php if(is_array($auditList['insureList'])): $i = 0; $__LIST__ = $auditList['insureList'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
                <td><?php echo ($vo["supplier_name"]); ?></td>
                <td>保险</td>
                <td><?php echo ($vo["supplier_sn"]); ?></td>
                <td><?php echo ($vo["insure_name"]); ?>: 儿童价：<?php echo ($vo["child_actual_cost"]); ?>元/人<br>成人价：<?php echo ($vo["adult_actual_cost"]); ?>元/人 </td>
                <!--<td><?php echo (date("Y-m-d",$vo["start_time"])); ?>至<?php echo (date("Y-m-d",$vo["end_time"])); ?></td>-->
                <td><?php echo ($vo["audit_name"]); ?></td>
                <td>
                    <?php switch($vo["is_review"]): case "1": ?>已审核<?php break;?>
                        <?php case "-1": ?>未审核<?php break;?>
                        <?php case "-2": ?>已拒绝<?php break; endswitch;?>
                </td>
                <td>
                    <?php switch($vo["is_shelves"]): case "1": ?>是<?php break;?>
                        <?php case "-1": ?>否<?php break; endswitch;?>
                </td>
                <td>
                    <?php if(checkAuth('Dealer/Shops/insureProductById')): ?><a href="<?php echo U('Shops/insureProductById',array('id'=>$vo['supplier_id']));?>" class="btn btn-xs btn-primary">详情</a><?php endif; ?>
                </td>
            </tr><?php endforeach; endif; else: echo "" ;endif; ?>

        <!--交通-->
        <?php if(is_array($auditList['trafficList'])): $i = 0; $__LIST__ = $auditList['trafficList'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
                <td><?php echo ($vo["supplier_name"]); ?></td>
                <td>交通</td>
                <td><?php echo ($vo["supplier_sn"]); ?></td>
                <td><?php echo ($vo["traffic_name"]); ?>: 儿童价：<?php echo ($vo["child_actual_cost"]); ?>元/人<br>成人价：<?php echo ($vo["adult_actual_cost"]); ?>元/人 </td>
                <!--<td><?php echo (date("Y-m-d",$vo["start_time"])); ?>至<?php echo (date("Y-m-d",$vo["end_time"])); ?></td>-->
                <td><?php echo ($vo["audit_name"]); ?></td>
                <td>
                    <?php switch($vo["is_review"]): case "1": ?>已审核<?php break;?>
                        <?php case "-1": ?>未审核<?php break;?>
                        <?php case "-2": ?>已拒绝<?php break; endswitch;?>
                </td>
                <td>
                    <?php switch($vo["is_shelves"]): case "1": ?>是<?php break;?>
                        <?php case "-1": ?>否<?php break; endswitch;?>
                </td>
                <td>
                    <?php if(checkAuth('Dealer/Shops/trafficProductById')): ?><a href="<?php echo U('Shops/trafficProductById',array('id'=>$vo['supplier_id']));?>" class="btn btn-xs btn-primary">详情</a><?php endif; ?>
                </td>
            </tr><?php endforeach; endif; else: echo "" ;endif; ?>
        </tbody>
    </table>
</div>
<!--产品审核结束-->

   <!-- </form>
    <ul id="nav" class="nav nav-tabs">
        <?php if(checkAuth('Operator/Product/lineList')): ?><li role="presentation" class=""><a data-index="0" onclick="javascript:changePage(this)">线路审核/上架</a></li><?php endif; ?>
        <?php if(checkAuth('Operator/SaleManger/orderList')): ?><li role="presentation" class=""><a data-index="1" onclick="javascript:changePage(this)">订单确认</a></li><?php endif; ?>
        <?php if(checkAuth('Operator/Finance/settlement_list')): ?><li role="presentation" class=""><a data-index="2" onclick="javascript:changePage(this)">线路结算</a></li><?php endif; ?>
        <?php if(checkAuth('Operator/Index/waitAuditUnitShop')): ?><li role="presentation" class=""><a data-index="3" onclick="javascript:changePage(this)">商家审核</a></li><?php endif; ?>
        <?php if(checkAuth('Operator/Index/waitAuditUnitGoods')): ?><li role="presentation" class=""><a data-index="4" onclick="javascript:changePage(this)">产品审核</a></li><?php endif; ?>
    </ul>
    <iframe id="iframe" src="/Operator/Product/lineList/changeTitle/1"></iframe>-->



<script src="/Tpl/Operator/js/jedate/jedate.js"></script>
<script src="/Tpl/Operator/js/highcharts.js"></script>
<script src="/Tpl/Operator/js/index/welcome.js"></script>
<script>
createPieChart($('#pieChart'), '日订单统计', [['已处理订单( <?php echo ((isset($day['deal']) && ($day['deal'] !== ""))?($day['deal']):0); ?>)',   <?php echo ($day['deal']?$day['deal']:1); ?>],['未处理订单(<?php echo ((isset($day['wait']) && ($day['wait'] !== ""))?($day['wait']):0); ?>)', <?php echo ((isset($day['wait']) && ($day['wait'] !== ""))?($day['wait']):0); ?>]]);

createPieChart($('#pieChart2'), '日营业额统计', [['成本(<?php echo ((isset($day['closing_total_money']) && ($day['closing_total_money'] !== ""))?($day['closing_total_money']):0); ?>)', <?php echo ($day['closing_total_money']?$day['closing_total_money']:1); ?>],['利润( <?php echo ((isset($day['profit']) && ($day['profit'] !== ""))?($day['profit']):0); ?>)',<?php echo ((isset($day['profit']) && ($day['profit'] !== ""))?($day['profit']):0); ?>]]);

<?php if($_GET['years'] && $_GET['choose']): ?>createBarChart('年营业额统计', JSON.parse(<?php echo ($right['conf']); ?>), JSON.parse(<?php echo ($right['orderNum']); ?>), JSON.parse(<?php echo ($right['profit']); ?>), JSON.parse(<?php echo ($right['endNeedPay']); ?>));
<?php else: ?>
createBarChart('月营业额统计', JSON.parse(<?php echo ($right['conf']); ?>), JSON.parse(<?php echo ($right['orderNum']); ?>), JSON.parse(<?php echo ($right['profit']); ?>), JSON.parse(<?php echo ($right['endNeedPay']); ?>));<?php endif; ?>

</script>