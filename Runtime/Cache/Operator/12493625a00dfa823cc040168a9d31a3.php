<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>
        
    </title>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="/Public/statics/aceadmin/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="/Public/statics/font-awesome-4.4.0/css/font-awesome.min.css"/>
    <!--[if IE 7]>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/font-awesome-ie7.min.css"/><![endif]-->
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/ace.min.css"/>
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/ace-ie.min.css"/><![endif]--><!--[if lt IE 9]>
    <script src="/Public/statics/aceadmin/js/html5shiv.js"></script>
    <script src="/Public/statics/aceadmin/js/respond.min.js"></script><![endif]-->
    <!-- <link rel="stylesheet" href="/Public/css/base.css"/> -->
    <style>
        ::-webkit-scrollbar {
            width: 10px;
            height: 5px;
        }

        ::-webkit-scrollbar-corner, ::-webkit-scrollbar-track {
            background-color: #e2e2e2;
        }

        ::-webkit-scrollbar-thumb {
            border-radius: 0;
            background-color: rgba(0,0,0,.3);
        }

        ::-webkit-scrollbar-corner, ::-webkit-scrollbar-track {
            background-color: #e2e2e2;
        }
        ul,li{ list-style: none; }
        ol{margin:0;}
        .jedatehms li{display: none;}
        #jedatebox ul{
            padding-right: 0;
            margin-right: 0;
        }
    </style>
    
</head>
<body>



<script src="/Public/statics/js/jquery-1.10.2.min.js"></script><!-- <![endif]--><!--[if IE]>
<script src="/Public/statics/js/jquery-1.10.2.min.js"></script><![endif]--><!--[if !IE]> -->
<script type="text/javascript">
    window.jQuery || document.write("<script src='/Public/statics/aceadmin/js/jquery-2.0.3.min.js'>" + "<" + "script>");
</script><!-- <![endif]--><!--[if IE]>
<script type="text/javascript">
    window.jQuery || document.write("<script src='/Public/statics/aceadmin/js/jquery-1.10.2.min.js'>" + "<" + "script>");
</script><![endif]-->
<script type="text/javascript">
    if ("ontouchend" in document) document.write("<script src='/Public/statics/aceadmin/js/jquery.mobile.custom.min.js'>" + "<" + "script>");
</script>
<script src="/Public/statics/aceadmin/js/bootstrap.min.js"></script>
<script src="/Public/statics/aceadmin/js/typeahead-bs2.min.js"></script>
<!--[if lte IE 8]>
<script src="/Public/statics/aceadmin/js/excanvas.min.js"></script><![endif]-->
<script src="/Public/statics/aceadmin/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.ui.touch-punch.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.slimscroll.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.easy-pie-chart.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.sparkline.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.pie.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.resize.min.js"></script>
<script src="/Public/statics/aceadmin/js/ace-elements.min.js"></script>
<script src="/Public/statics/aceadmin/js/ace.min.js"></script>
<script src="/Public/statics/Operator/js/base.js"></script>
<script src="/Public/statics/layer/layer.js"></script>
<!-- <script src="/Public/js/base.js"></script> -->

<script>
    var publicurl="/Public";
    var domainURL="";

    $(function () {
        var bodyH=$(document).height();
        try{
            //parent.resetFrameHeight(bodyH);
        }catch (err){

        }

    })
</script>
</body>
</html>

    <style>
        th,td{text-align: center;}
        .row-2{
            padding: 5px 10px;

        }

        .row-input-width{
            /*width: 347px!important;*/
        }


        .pull-right{
            margin-right: 14px!important;
        }
        .orderNo-item{
            /*padding-left: 28px;*/
        }
        .bill-time-item{
            padding-left: 14px;
        }
        .range-item{
            padding: 7px 10px;
            background-color: #eee;
        }

        .input-left{
            margin-left: -4px;
        }
        .table-scroll{
            /*display: inline-block;*/
            /*width: 1200px!important;*/
            /*overflow: scroll;*/
            /*table-layout: fixed;*/
        }
        .total-item{
            text-align: right;
            padding: 0 3px 10px 3px;
            color: red;
        }
        .tab-content{ position: static; }
    </style>
    <div class="page-header"><h1><i class="fa fa-home"></i> 首页 &gt; 财务管理 &gt; 分销商结算列表</h1></div>

    <div class="col-xs-12">

        <div class="tab-content">
            <div class="row" >
                <form action="" id="form" class="form-inline " method="" style="margin-left:10px;" >
                    <input type="hidden" name="p" value="1"/>
                    <div class="row-2" >
                        <div class="form-group form-group-2">
                            <label for="" class="bill-time-item">选择日期:</label>
                            <input type="text" name="start_time" id="start" value="<?php if($_GET['start_time']): echo ($_GET['start_time']); endif; ?>">
                            <span class="range-item input-left">至</span>
                            <input class="input-left" type="text" name="end_time" id="end" value="<?php if($_GET['end_time']): echo ($_GET['end_time']); endif; ?>">
                        </div>
                    </div>
                    <div class="row-2" >
                        <div class="form-group">
                            <label for="group_num" class="orderNo-item"></label>
                            <input type="text" class="row-input-width" name="group_num" id="group_num" value="<?php if($_GET['group_num']): echo ($_GET['group_num']); endif; ?>" placeholder="请输入团号">
                        </div>
                        <div class="form-group">
                            <label for="reseller_sn" class="orderNo-item"></label>
                            <input type="text" class="row-input-width" name="reseller_sn" id="reseller_sn" value="<?php if($_GET['reseller_sn']): echo ($_GET['reseller_sn']); endif; ?>" placeholder="请输入分销商编号">
                        </div>
                        <div class="form-group">
                            <label for="order_num" class="orderNo-item"></label>
                            <input type="text" class="row-input-width" name="reseller_name" id="reseller_name" value="<?php if($_GET['reseller_name']): echo ($_GET['reseller_name']); endif; ?>" placeholder="请输入分销商名称">
                        </div>
                    </div>
                    <div class="col-xs-12 " style="padding:15px">
                        <button type="button" onclick="outExcel()" class="btn btn-primary">导出</button>
                        <button type="submit" class="btn btn-primary submit-btn">查询</button>
                        <div class="dropdown" style="display: inline-block">
                            <button type="button" class="btn btn-primary dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown">筛选<span class="caret"></span></button>
                            <ul id="menuList" class="dropdown-menu pull-right" role="menu" aria-labelledby="dropdownMenu1" style="padding:10px;"></ul>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="tabbable">
  <!--           <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab">
                <li class="active"><a href="<?php echo U('Rule/admin_user_list');?>">员工列表</a></li>
                <li><a href="<?php echo U('Rule/add_admin');?>">添加员工</a></li>
            </ul> -->

            <div class="tab-content">
                <div class="total-item">总结算金额统计：¥<?php echo ($all_total_end_need_pay); ?></div>
                <table id="table" class="table table-striped table-bordered table-hover table-condensed text-center table-scroll">
                    <thead>
                        <tr>
                            <th class="filtrate_0" rowspan="2">分销商</th>
                            <th class="filtrate_1" rowspan="2">分销商编号</th>
                            <th class="filtrate_2" rowspan="2">线路编号</th>
                            <th class="filtrate_3" rowspan="2">团号</th>
                            <th class="filtrate_4" rowspan="2">线路名称</th>
                            <th class="filtrate_5" rowspan="2">供应商</th>
                            <!-- <th class="filtrate_6" rowspan="2">订单总数</th> -->
                            <th class="filtrate_6" colspan="2">实际收客人数</th>
                            <!-- <th class="filtrate_8" colspan="3">结算金额(单价)</th> -->
                            <!-- <th class="filtrate_9" rowspan="2">其他费用</th> -->
                            <th class="filtrate_7" rowspan="2">总结算金额</th>
                            <th class="filtrate_8" rowspan="2">操作人</th>
                            <!-- <th width="100" class="filtrate_11" rowspan="2">结算时间</th> -->
                            <!-- <th rowspan="2">操作人</th> -->
                        </tr>
                        <tr>
                            <th class="filtrate_6">成人</th>
                            <th class="filtrate_6">儿童</th>
                           <!-- <th class="filtrate_7">老人</th>-->
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><tr >
                                <td class="filtrate_0"><?php echo ($v["reseller_name"]); ?></td>
                                <td class="filtrate_1"><?php echo ($v["reseller_sn"]); ?></td>
                                <td class="filtrate_2"><?php echo ($v["line_sn"]); ?></td>
                                <td class="filtrate_3"><?php echo ($v["group_num"]); ?></td>
                                <td class="filtrate_4"><?php echo ($v["line_name"]); ?></td>
                                <td class="filtrate_5"><?php echo ($v["supplier_name"]); ?></td>
                                <!-- <td class="filtrate_6"></td> -->
                                <td class="filtrate_6"><?php echo ($v["adult_num"]); ?></td>
                                <td class="filtrate_6"><?php echo ($v["child_num"]); ?></td>
                                <!--<td class="filtrate_7"><?php echo ($v["oldMan_num"]); ?></td>-->
                                <td class="filtrate_7"><?php echo ($v["total_money"]); ?></td>
                                <td class="filtrate_8"><?php echo ($v["sales"]); ?></td>
                                <!-- <td class="filtrate_11">结算时间xxxxxxxxx</td> -->
                                <!-- <td class="filtrate_12">操作人</td> -->
                            </tr><?php endforeach; endif; else: echo "" ;endif; ?>
                        <tr>
                            <td colspan="17">
                                <!--分页样式-->
                                <ul class="pagination">
                                    <?php echo ($show); ?>
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>


<script src="/Tpl/Operator/js/jedate/jedate.js"></script>
<script type="text/javascript">
    //导出excel
    function outExcel(){
        $form = $('#form');
        $form.attr('action', '<?php echo U('outResellerSettlementListExportExcel');?>');
        $form.submit();
        $form.attr('action', '');
    }

    //生成筛选菜单
    (function(){
        var str = '';
        $('#table').find('tr').eq(0).find('th').each(function(i, n){
            str += '<li><input type="checkbox" class="filtrate" checked="" value="'+i+'" id="filtrate_'+i+'"><label for="filtrate_'+i+'">'+$(n).html()+'</label></li>';
        })
        $('#menuList').append(str);
    }())

    $('#menuList').on('change', '.filtrate', function(){
        var index = parseInt($(this).val());
        if($(this).prop('checked')){
            $('.filtrate_'+index).show();
        }else{
            $('.filtrate_'+index).hide();
        }
    })
     /**
     * 绑定日期选择器
     * @param  {[obj]}    obj        [元素]
     * @param  {[string]} dateFormat [时间格式]
     */
    function dateFormat(obj, dateFormat){
        jeDate({
            dateCell: '#'+$(obj).attr('id'),
            format: dateFormat,
            isinitVal:false,
            isTime:true, //isClear:false,
            okfun:function(val){
            }
        });
    }
   dateFormat($('#start'), 'YYYY-MM-DD');
   dateFormat($('#end'), 'YYYY-MM-DD');
</script>