<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>
        
    </title>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="/Public/statics/aceadmin/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="/Public/statics/font-awesome-4.4.0/css/font-awesome.min.css"/>
    <!--[if IE 7]>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/font-awesome-ie7.min.css"/><![endif]-->
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/ace.min.css"/>
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/ace-ie.min.css"/><![endif]--><!--[if lt IE 9]>
    <script src="/Public/statics/aceadmin/js/html5shiv.js"></script>
    <script src="/Public/statics/aceadmin/js/respond.min.js"></script><![endif]-->
    <!-- <link rel="stylesheet" href="/Public/css/base.css"/> -->
    <style>
        ::-webkit-scrollbar {
            width: 10px;
            height: 5px;
        }

        ::-webkit-scrollbar-corner, ::-webkit-scrollbar-track {
            background-color: #e2e2e2;
        }

        ::-webkit-scrollbar-thumb {
            border-radius: 0;
            background-color: rgba(0,0,0,.3);
        }

        ::-webkit-scrollbar-corner, ::-webkit-scrollbar-track {
            background-color: #e2e2e2;
        }
        ul,li{ list-style: none; }
        ol{margin:0;}
        .jedatehms li{display: none;}
        #jedatebox ul{
            padding-right: 0;
            margin-right: 0;
        }
    </style>
    
    <link rel="stylesheet" href="/Public/statics/layui/css/layui.css"  media="all">
    <style>
        th{text-align:center;}
    </style>

</head>
<body>

    <div class="page-header"><h1><i class="fa fa-home"></i> 首页>产品管理>线路管理</h1></div>
     <div class="container-fluid">
         <div class="tabbable">
             <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab">
                 <li <?php if($post['status'] == 1): ?>class="active"<?php endif; ?> ><a href="javascript:checkStatus(1);" >已上架</a></li>
                 <li <?php if($post['status'] == 0): ?>class="active"<?php endif; ?>><a href="javascript:checkStatus(0);" >待审核
                     <?php if($waitAudit): ?><span class="badge"><?php echo ($waitAudit); ?></span><?php endif; ?>
                    </a></li>
                 <li <?php if($post['status'] == -2): ?>class="active"<?php endif; ?>><a href="javascript:checkStatus(-2);" >被拒线路</a></li>
                 <li <?php if($post['status'] == -1): ?>class="active"<?php endif; ?>><a href="javascript:checkStatus(-1);" >下架线路</a></li>
                 <li <?php if($post['status'] == -4): ?>class="active"<?php endif; ?>><a href="javascript:checkStatus(-4);" >过期线路</a></li>

             </ul>
             <div class="tab-content">
                 <div class="row" >
                     <form action="lineList" class="form-inline " method="get" style="margin-left:10px;" >
                         <input type="hidden" name="status" value="<?php echo ((isset($post["status"]) && ($post["status"] !== ""))?($post["status"]):0); ?>" style="display: none;">
                         <div class="form-group">
                             <label for="lineNum">线路编号</label>
                             <input type="text" class="" name="line_sn" id="line_sn" placeholder="线路编号" value="<?php echo ($post["line_sn"]); ?>">
                         </div>
                         <div class="form-group">
                             <label for="lineName">线路名称</label>
                             <input type="text" class="" name="line_name" id="line_name" placeholder="线路名称" value="<?php echo ($post["line_name"]); ?>">
                         </div>
                         <div class="form-group">
                             <label for="lineName">团号</label>
                             <input type="text" class="" name="group_num" id="group_num" placeholder="团号" value="<?php echo ($post["group_num"]); ?>">
                         </div>

                         <div class="form-group">
                             <label for="lineName">出发地</label>
                             <input type="text" class="" name="area_name" id="area_name" placeholder="出发地" value="<?php echo ($post["area_name"]); ?>">
                         </div>
                         <div class="form-group">
                             <label for="lineName">运营性质</label>
                             <select name="source_type" id="source_type">
                                 <option value="">全部</option>
                                 <option <?php if($post['source_type'] == 1): ?>selected<?php endif; ?> value="1">自营</option>
                                 <option  <?php if($post['source_type'] == 2): ?>selected<?php endif; ?>  value="2">专线</option>
                             </select>
                         </div>


                         <!--
                           <div class="form-group">
                             <label for="pushDepartment">发布部门</label>
                             <input type="text" class="" name="department" id="department" placeholder="发布部门" value="<?php echo ($post["department"]); ?>">
                         </div>
                         <div class="form-group">
                             <label for="startDate">发布时间</label>
                             <input type="text" class="" name="start_time" id="start_time" placeholder="开始时间" value="<?php echo ($post["start_time"]); ?>" readonly>
                         </div>
                         <div class="form-group">
                             <label for="endDate">&nbsp;</label>
                             <input type="text" class="" name="end_time" id="end_time" placeholder="结束时间" value="<?php echo ($post["end_time"]); ?>" readonly>
                         </div>-->
                         <button type="submit" class="btn btn-primary">查询</button>
                     </form>
                 </div>

                 <table class="table table-striped table-bordered table-hover table-condensed text-center">
                     <tbody>
                         <tr>
                             <th>全选 <input type="checkbox" class="checkAll"></th>
                             <th>线路编号</th>
                             <th>线路名称</th>
                             <th>团号</th>
                             <th>行程天数</th>
                             <th>出发地</th>
                             <th>发布时间</th>
                             <th>供应商</th>
                            <!-- <th>结算价格</th>-->
                             <th>状态</th>
                             <th>审核人</th>
                             <th>操作</th>
                         </tr>
                         <?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
                             <td><input type="checkbox" class="chk" value=""></td>
                             <td><?php echo ($vo["line_sn"]); ?></td>
                             <td><?php echo ($vo["line_name"]); ?></td>
                             <td><?php echo ($vo["group_num"]); ?></td>
                             <td><?php echo ($vo["travel_days"]); ?></td>
                             <td><?php echo ($vo["areaName"]); ?></td>
                             <td> <?php echo (date("Y-m-d",$vo["create_time"])); ?></td>
                             <td> <?php echo ((isset($vo["supplier_name"]) && ($vo["supplier_name"] !== ""))?($vo["supplier_name"]):"无"); ?></td>
                            <!-- <td>
                                 <?php if($vo['adult_money'] > 0): ?>成<?php echo ($vo["adult_money"]); ?>;成<?php echo ($vo["child_money"]); ?>;成<?php echo ($vo["oldman_money"]); endif; ?>
                             </td>-->
                             <td>
                                 <?php switch($vo["line_status"]): case "1": ?>已上架<?php break;?>
                                     <?php case "0": ?>待审核<?php break;?>
                                     <?php case "-1": ?>已下架<?php break;?>
                                     <?php case "-2": ?>已拒绝<?php break; endswitch;?>
                             </td>
                             <td><?php echo ($vo["operator_name"]); ?></td>
                             <td>

                                 <?php if($vo['source_type'] == 1): if(checkAuth('Operator/Product/lineDetail')): ?><a href="<?php echo U('lineDetail',array('id'=>$vo['line_id']));?>" class="btn btn-xs btn-primary">详情</a><?php endif; ?>
                                        <?php else: ?>
                                     <?php if(checkAuth('Operator/Product/specialLineDetail')): ?><a href="<?php echo U('specialLineDetail',array('id'=>$vo['line_id']));?>" class="btn btn-xs btn-primary">详情</a><?php endif; endif; ?>


                                 <?php switch($vo["line_status"]): case "1": if(checkAuth('Operator/Product/soldOut')): ?><a href="<?php echo U('soldOut',array('id'=>$vo['line_id']));?>" class="btn btn-xs btn-primary">下架</a><?php endif; break;?>
                                     <?php case "0": if($vo['source_type'] == 1): if(checkAuth('Operator/Product/auditLine')): ?><a href="<?php echo U('auditLine',array('id'=>$vo['line_id']));?>" class="btn btn-xs btn-primary">审核</a><?php endif; ?>
                                                 <?php else: ?>
                                                 <?php if(checkAuth('Operator/Product/auditSpecialLine')): ?><a href="<?php echo U('auditSpecialLine',array('id'=>$vo['line_id']));?>" class="btn btn-xs btn-primary">审核</a><?php endif; endif; break;?>
                                     <?php case "-2": if($vo['source_type'] == 1): if(checkAuth('Operator/EditLine/form_step1')): ?><a href="<?php echo U('EditLine/form_step1',array('id'=>$vo['line_id']));?>" class="btn btn-xs btn-primary">编辑</a><?php endif; ?>
                                                 <?php else: ?>
                                                 <!--供应商提供的专线路-->
                                                 <?php if(checkAuth('Operator/SpecialLine/form_step1')): ?><a href="<?php echo U('SpecialLine/form_step1',array('id'=>$vo['line_id']));?>" class="btn btn-xs btn-primary">编辑</a><?php endif; endif; break;?>
                                     <?php case "-1": if(checkAuth('Operator/Product/putaway')): ?><a href="<?php echo U('putaway',array('id'=>$vo['line_id']));?>" class="btn btn-xs btn-primary">上架</a><?php endif; break; endswitch;?>
                             </td>
                         </tr><?php endforeach; endif; else: echo "" ;endif; ?>
                            <tr>
                                <td colspan="12">
                                    <ul class="pagination">
                                        <?php echo ($show); ?>
                                    </ul>
                                </td>
                            </tr>
                     </tbody>
                 </table>
             </div>
         </div>
     </div>

<script src="/Public/statics/js/jquery-1.10.2.min.js"></script><!-- <![endif]--><!--[if IE]>
<script src="/Public/statics/js/jquery-1.10.2.min.js"></script><![endif]--><!--[if !IE]> -->
<script type="text/javascript">
    window.jQuery || document.write("<script src='/Public/statics/aceadmin/js/jquery-2.0.3.min.js'>" + "<" + "script>");
</script><!-- <![endif]--><!--[if IE]>
<script type="text/javascript">
    window.jQuery || document.write("<script src='/Public/statics/aceadmin/js/jquery-1.10.2.min.js'>" + "<" + "script>");
</script><![endif]-->
<script type="text/javascript">
    if ("ontouchend" in document) document.write("<script src='/Public/statics/aceadmin/js/jquery.mobile.custom.min.js'>" + "<" + "script>");
</script>
<script src="/Public/statics/aceadmin/js/bootstrap.min.js"></script>
<script src="/Public/statics/aceadmin/js/typeahead-bs2.min.js"></script>
<!--[if lte IE 8]>
<script src="/Public/statics/aceadmin/js/excanvas.min.js"></script><![endif]-->
<script src="/Public/statics/aceadmin/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.ui.touch-punch.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.slimscroll.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.easy-pie-chart.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.sparkline.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.pie.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.resize.min.js"></script>
<script src="/Public/statics/aceadmin/js/ace-elements.min.js"></script>
<script src="/Public/statics/aceadmin/js/ace.min.js"></script>
<script src="/Public/statics/Operator/js/base.js"></script>
<script src="/Public/statics/layer/layer.js"></script>
<!-- <script src="/Public/js/base.js"></script> -->

    <script src="/Public/statics/layui/layui.all.js"></script>
    <script src="/Public/statics/layui/layui.js"></script>

    <script>
        function checkStatus(status) {
            $('input[name="status"]').val(status);
            $('form').submit();
        }

        layui.use('laydate', function() {
            var laydate = layui.laydate;
            //常规用法
            laydate.render({
                elem: '#start_time'
            });
            laydate.render({
                elem: '#end_time'
            });
        });


    </script>

<script>
    var publicurl="/Public";
    var domainURL="";

    $(function () {
        var bodyH=$(document).height();
        try{
            //parent.resetFrameHeight(bodyH);
        }catch (err){

        }

    })
</script>
</body>
</html>