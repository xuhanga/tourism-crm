$(document).ready(function(){

	$(".level1").on('change',function(){
		$selval = $("select[name='level1']").val();
		$("select[name='level2']").remove();
		$.ajax({
                url:$dependency,
                dataType:'json',
                type:'post',
                cache:false,
                data:{id:$selval},
                success:function(arr){
                	console.info(arr);
                	//组装select增加到后面
                	var op = '<select class="level2" name="level2">';
                		op += '<option value="-1">请选择省份</option>';
                		for(var i in arr['data']){

                			op += '<option value="'+arr['data'][i]['areaId']+'">'+arr['data'][i]['areaName']+'</option>';
                		}
                		op += '</select>'; 
                	$("#ssd").append(op);
                	$(".level2").on('change',function(){
						$selval = $("select[name='level2']").val();
						//清空他的下级
						$("select[name='level3']").remove();
						$("select[name='level4']").remove();
						$.ajax({
				                url:$dependency,
				                dataType:'json',
				                type:'post',
				                cache:false,
				                data:{id:$selval},
				                success:function(arr){
				                	console.info(arr);
				                	//组装select增加到后面
				                	var op = '<select class="level3" name="level3">';
				                	op += '<option value="-1">请选择城市</option>';
				                		for(var i in arr['data']){

				                			op += '<option value="'+arr['data'][i]['areaId']+'">'+arr['data'][i]['areaName']+'</option>';
				                		}
				                		op += '</select>'; 
				                	$("#ssd").append(op);
				                	$(".level3").on('change',function(){
										$selval = $("select[name='level3']").val();
										//城市级别，显示选择按钮
										$(".selht").show();
										//移除下级
										$("select[name='level4']").remove();

										$.ajax({
								                url:$dependency,
								                dataType:'json',
								                type:'post',
								                cache:false,
								                data:{id:$selval},
								                success:function(arr){
								                	console.info(arr);
								                	//组装select增加到后面
								                	var op = '<select class="level4" name="level4">';
								                		op += '<option value="-1">请选择地区</option>';
								                		for(var i in arr['data']){

								                			op += '<option value="'+arr['data'][i]['areaId']+'">'+arr['data'][i]['areaName']+'</option>';
								                		}
								                		op += '</select>'; 
								                	$("#ssd").append(op);
								                }
								        });        
									});

				                }
				        });        
					});

                }
        });        
	});

	$("#selht").on('click',function(){

		//获取当前录入的国-省-市-区
		var guo = $("select[name='level1']").val()?$("select[name='level1']").val():0;
		var sheng = $("select[name='level2']").val()?$("select[name='level2']").val():0;
		var shi  = $("select[name='level3']").val()?$("select[name='level3']").val():0;
		var  dq  = $("select[name='level4']").val()?$("select[name='level4']").val():0;
		$.ajax({
                url:$showmsg,
                dataType:'json',
                type:'post',
                cache:false,
                data:{guo:guo,sheng:sheng,shi:shi,dq:dq,type:'hotel'},
                success:function(data){

                	//组装数据
                	$("#tr_hotel").empty();
                		var tr = '';
                		for(var i in data['hotels']){
                			tr += '<tr>';
                			tr += '<td style="text-align: center;"><span id="hotel_name">'+data['hotels'][i]['supplier_name']+'</th>';
                			tr += '<td style="text-align: center;">';
                			for(var j in data['hotels'][i]['rooms']){
                				if(j < 1){
                					tr += '<input name="room_id" checked type="radio" value="'+data['hotels'][i]['rooms'][j]['room_id']+'"/><span id="room-'+data['hotels'][i]['rooms'][j]['room_id']+'">'+data['hotels'][i]['rooms'][j]['room_name']+'</span>';

                				}else{
                					tr += '<input name="room_id" type="radio" value="'+data['hotels'][i]['rooms'][j]['room_id']+'"/><span id="room-'+data['hotels'][i]['rooms'][j]['room_id']+'">'+data['hotels'][i]['rooms'][j]['room_name']+'</span>';
                				}	
                			}
                			
                			tr += '</td> ';
                			tr += '</tr>';
                		}
                	$("#tr_hotel").append(tr);		
                	$('#bjy-add').modal('show');
                	


                }
        });


	});

});










$("body").on('click','.add_pro',function(){
                		console.info('添加');
						//酒店资料
						$room_id = $("input[name='room_id']").val();
						$hotel_name = $("#hotel_name").text();
						$room_name = $("#room-"+$room_id).text();

						var tr = '<tr>';
							tr += '<input name="room_id[]" type="hidden" value="'+$room_id+'" />';
							tr += '<td>'+$hotel_name+'</td>';
							tr += '<td>'+$room_name+'</td>';
							tr += '</tr>';
						//组装数据生成li展示到页面上
						$(".hotels_list").append(tr);

						$('#bjy-add').modal('hide');
	});
