$(document).ready(function(){

		//动态加载行程

		var days = $("input[name='travel_days']").val();
		// console.info(days);
		for(i=0;i<days;i++){
			parm = i+1;	

				//动态生成菜单
			var li = '';
				if(i==0){
					li += ' <li class="head-on travel_days">第1天</li>';
				}else{
					li += '<li class="travel_days">第'+parm+'天</li>';
				}	
				$("#ul1").append(li);

			//动态生成行程表单
			var html = '';
				if(i==0){
					html += '<div class="travel_detail-1 day-item day-item-on" style="display: block;">';
				}else{
					html += '<div class="travel_detail-'+parm+' day-item day-item-on"  style="display: none;">';
				}
				html += '<div class="row row-div"><div class="col-md-2" ><p class="text-right"><font color="red">*</font>第'+parm+'天:行程标题：</p></div><div class="col-md-5"><input required="required" class="trip-input" type="text" name="arr['+parm+"][trip_title]"+'" value="" /></div></div>';
				html += '<div class="row row-div"><div class="col-md-2" ><p class="text-right"><font color="red">*</font>线路特色：</p></div><div class="col-md-5"><textarea required="required" class="trip-input textarea-height" type="text" name="arr['+parm+"][features]"+'" value=""></textarea></div></div>';
				html += '<div class="row row-div"><div class="col-md-2" ><p class="text-right"><font color="red">*</font>线路简介：</p></div><div class="col-md-5"><textarea required="required" class="trip-input textarea-height" type="text" name="arr['+parm+"][introduce]"+'" value=""></textarea></div></div>';
				html += '<div class="row row-div"><div class="col-md-2" ><p class="text-right">用餐(默认不勾选不包吃饭)：</p></div><div class="col-md-10"><input class="form-radio" type="checkbox" width="30"  name="arr['+parm+"][dining][]"+'" value="2"/><span>早餐</span><input class="form-radio" type="checkbox" width="30"  name="arr['+parm+"][dining][]"+'" value="3"/><span>中餐</span><input class="form-radio" type="checkbox" width="30"  name="arr['+parm+"][dining][]"+'" value="4"/><span>晚餐</span></div></div>';
				html += '<div class="row row-div"><div class="col-md-2" ><p class="text-right">住宿：</p></div><div class="col-md-5"><input class="trip-input" type="text" name="arr['+parm+"][lodging]"+'" value=""></div></div>';
				html += '<div class="row row-div"><div class="col-md-2" ><p class="text-right">游玩景区：</p></div><div class="col-md-5"><input class="trip-input" type="text" name="arr['+parm+"][scenic_spot]"+'" value=""></div></div>';
				html += '<div class="row row-div"><div class="col-md-2" ><p class="text-right"><font color="red">*</font>活动安排：</p></div><div class="col-md-5"><textarea required="required" class="trip-input textarea-height" id="editor_id" type="text" style="width:700px;height:300px;" name="arr['+parm+"][trip_details]"+'" value=""></textarea></div></div>';
				html += '<div class="row row-div low-top-2"><div class="col-md-2" ><p class="text-right">游玩项目：</p></div><div class="col-md-9 play-table-width"><i class=" icon-plus-sign-alt"></i><input type="button" class="'+parm+'-play-add"  item="13" value="添加" ></input><div class="play_table low-top-2"><table id="sample-table-1" class="table table-striped table-bordered table-hover"><thead><tr><th class="table-th-width-3 ">名称</th><th class="table-th-width-3">参考价格</th><th class="">详情/备注</th><th class="table-th-width-1 center">操作</th></tr></thead><tbody class="'+parm+'-play-add"><tr class="'+parm+'-play"><td><input  name="arr['+parm+"][play][name][]"+'" type="text"></td><td><input  name="arr['+parm+"][play][price][]"+'" type="tel"></td><td><input name="arr['+parm+"][play][note][]"+'" type="text"></td><td><div class="p"><input type="button" class="'+parm+'-play-delete" item="13" value="删除"> </input></div></td></tr></tbody></table></div></div></div>';
				html += '<div class="row row-div low-top-2"><div class="col-md-2" ><p class="text-right">购物项目：</p></div><div class="col-md-9 play-table-width"><i class=" icon-plus-sign-alt"></i><input type="button" class="'+parm+'-shopping-add" item="13" value="添加"/><div class="play_table low-top-2"><table id="sample-table-1" class="table table-striped table-bordered table-hover"><thead><tr><th class="table-th-width-2">名称</th><th class="table-th-width-2">营业产品</th><th class="table-th-width-2">停留时间</th><th class="">说明</th><th class="table-th-width-1 center">操作</th></tr></thead><tbody class="'+parm+'-shopping-add"><tr class="'+parm+'-shopping"><td><input  name="arr['+parm+"][shopping][shopping_name][]"+'" type="text"></td><td><input  name="arr['+parm+"][shopping][product][]"+'" type="text"></td><td><input  name="arr['+parm+"][shopping][stay_time][]"+'" type="text"></td><td><input  name="arr['+parm+"][shopping][instructions][]"+'" type="text"></td><td><div class="p"><input type="button" class="'+parm+'-shopping-delete" item="13" value="删除"></input></div></td></tr></tbody></table></div></div></div></div>';
				html += '</div>';
				// console.info(html);
				$("#div1").append(html);
			
		}



        $("input[class$='-add']").click(function(data){
        	// console.info($(this));
        	var cls = $(this)[0].className;
       		//1-play-add
        	//获取该tobody标签等于该class属性的最后一个tr标签的class值是多少,然后用-炸开，拼接新的tr新的class值+1
           	var trcls = $("tbody[class='"+cls+"']").find("tr:last")[0].className;
           	// console.log(trcls);

           arr = trcls.split('-');
           //最后一个下标 
           // var next_index = parseInt(arr[2])+1;
           //arr[0]是第几天，arr[1]什么项目next_index是下个项目的下标
           //arr[2]就是最后一个tr是第几个
           console.log(arr);
            $someday = $("#someday").val();
         	if(cls.indexOf('play')!=-1){
    			console.info('play');
           	   var tr = '<tr class="'+$someday+'-play">';
	           	   tr += '<td><input  name= "arr['+$someday+"][play][name][]"+'" type="text" /></td>';
	           	   tr += '<td><input  name= "arr['+$someday+"][play][price][]"+'" type="text" /></td>';
	           	   tr += '<td><input  name= "arr['+$someday+"][play][note][]"+'" type="text" /></td>';                                       
	           	   tr += '<td><div class="p"><input type="button" class="'+$someday+'-play-'+'delete" item="13" value="删除"> </input></div></td>';
	           	   tr += '</tr>';
         	}else{
         		console.log('shopping');
         	    var tr = '<tr class="'+$someday+'-shopping">';
	           	   tr += '<td><input  name= "arr['+$someday+"][shopping][shopping_name][]"+'" type="text" /></td>';
	           	   tr += '<td><input  name= "arr['+$someday+"][shopping][product][]"+'" type="text" /></td>';
	           	   tr += '<td><input  name= "arr['+$someday+"][shopping][stay_time][]"+'" type="text" /></td>'; 
	           	   tr += '<td><input  name= "arr['+$someday+"][shopping][instructions][]"+'" type="text" /></td>';                                   
	           	   tr += '<td><div class="p"><input type="button" class="'+$someday+'-shopping-'+'delete" item="13" value="删除"> </input></div></td>';
	           	   tr += '</tr>';

         	}
            //新增append到最后一个tr的后面去
            $("tbody[class='"+cls+"']").append(tr); 

            //删除对应的tr标签
	        $("input[class$='-delete']").click(function(data){
	        	// console.log($(this));return false;
	        	var cls = $(this)[0].className;
	        	arr = cls.split('-');
	        	var del = arr[0]+"-"+arr[1]+'-'+arr[2];
	        	// console.log(arr);
	        	tbdcls = arr[0]+'-'+arr[1]+"-add";
	        	var len = $("tbody[class='"+tbdcls+"']").find("tr").length;
	        	//console.info($("tbody[class='"+tbdcls+"']"));
	    
	        	if(len < 2){

	        		layer.msg('最少要添加一个项目');return false;

	        	}else{

	        		$(this).closest('tr').remove();
	        	}
	            

	             
	        });

        });






});